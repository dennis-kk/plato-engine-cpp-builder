#include "../../builder/plato_builder.hh"
#include "../core/plato_node.hh"
#include "../framework/flow_checker.hh"
#include "../framework/unittest.hh"

FIXTURE_BEGIN(Test5)

class Logic : public plato::PlatoNodeLogic {
public:
  Logic() {}
  virtual ~Logic() {}
  // ͨ�� PlatoNodeLogic �̳�
  virtual auto do_logic(plato::PlatoNode * /*node*/, std::time_t /*tick*/,
                        plato::PlatoFlowStatus & /*status*/)
      -> plato::ExecResult override {
    plato::ExecResult result;
    result.output_pin_index = 0;
    return result;
  }
  virtual auto do_event(plato::PlatoNode * /*node*/) -> void override {}
};

class LogicManager : public plato::PlatoLogicManager {
  std::shared_ptr<Logic> logic_;

public:
  LogicManager() { logic_ = std::make_shared<Logic>(); }
  virtual ~LogicManager() {}
  // ͨ�� PlatoLogicManager �̳�
  virtual auto get_logic_instance(plato::PlatoNodeID /*static_id*/)
      -> plato::PlatoNodeLogicPtr override {
    return logic_;
  }
};

CASE(Test5) {
  plato::PlatoStageBuilderImpl builder;
  auto logic_manager_ptr = std::make_shared<LogicManager>();
  auto stage = plato::new_stage(1, &builder, "../generated/Test5/Test5.plato");
  ASSERT_TRUE(stage->get_flow_count() == 2);
  plato::set_logic_manager(logic_manager_ptr);

  FlowChecker checker({"OnStart", "Getter", "Print", "Done", "OnStart",
                       "Getter", "Print", "Done"});
  stage->set_hook(&FlowChecker::hook);

  stage->update(0);
  auto flow_ptr = stage->get_flow(0);
  ASSERT_TRUE(flow_ptr->status() == plato::PlatoFlowStatus::DONE);
  auto flow_ptr1 = stage->get_flow(1);
  ASSERT_TRUE(flow_ptr1->status() == plato::PlatoFlowStatus::DONE);

  ASSERT_TRUE(checker.is_valid());
}

FIXTURE_END(Test5)
