#include "flow_checker.hh"

static SeqVector real_seq_;
static SeqVector expect_seq_;
FlowChecker *global_check_{nullptr};

FlowChecker::FlowChecker(const SeqVector &expect_seq) {
  expect_seq_ = expect_seq;
  global_check_ = this;
}

FlowChecker::~FlowChecker() {
  real_seq_.clear();
  expect_seq_.clear();
  global_check_ = nullptr;
}

auto FlowChecker::reset(const SeqVector &expect_seq) -> void {
  expect_seq_ = expect_seq;
}

auto FlowChecker::add_node(const std::string &node_name) -> void {
  real_seq_.emplace_back(node_name);
}

auto FlowChecker::is_valid() -> bool {
  auto result = (real_seq_ == expect_seq_);
  real_seq_.clear();
  expect_seq_.clear();
  return result;
}

auto FlowChecker::hook(plato::PlatoNode *node_ptr, plato::HookEvent e,
                       plato::PlatoFlowStatus) -> plato::HookAction {
  if (e == plato::HookEvent::BEFORE_RUN) {
    global_check_->add_node(node_ptr->name());
  }
  return plato::HookAction::OK;
}
