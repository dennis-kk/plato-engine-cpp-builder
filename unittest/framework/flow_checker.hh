#pragma once

#include "../core/plato_node.hh"
#include "../core/plato_stage.hh"
#include <vector>

using SeqVector = std::vector<std::string>;

class FlowChecker {
public:
  FlowChecker(const SeqVector &expect_seq);
  ~FlowChecker();
  auto reset(const SeqVector &expect_seq) -> void;
  auto add_node(const std::string &node_name) -> void;
  auto is_valid() -> bool;
  static auto hook(plato::PlatoNode *node_ptr, plato::HookEvent e,
                   plato::PlatoFlowStatus status) -> plato::HookAction;
};
