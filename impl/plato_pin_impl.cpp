#include "plato_pin_impl.hh"

namespace plato {

PlatoPinImpl::PlatoPinImpl(PlatoPinType type, VariablePtr var_ptr) {
  type_ = type;
  var_ptr_ = var_ptr;
}

PlatoPinImpl::~PlatoPinImpl() {}

auto PlatoPinImpl::type() -> PlatoPinType { return type_; }

auto PlatoPinImpl::var() -> VariablePtr { return var_ptr_; }

auto PlatoPinImpl::backtrack_node() -> PlatoNodePtr {
  return backtrack_node_ptr_;
}

auto PlatoPinImpl::set_backtrack_node(PlatoNodePtr node_ptr) -> void {
  backtrack_node_ptr_ = node_ptr;
}

auto PlatoPinImpl::object_size() -> std::size_t { return sizeof(PlatoPinImpl); }

auto PlatoPinImpl::reset() -> void {
  if (var_ptr_) {
    var_ptr_->copy_default();
  }
}

auto PlatoPinImpl::set_var(VariablePtr var_ptr) -> void { var_ptr_ = var_ptr; }

} // namespace plato
