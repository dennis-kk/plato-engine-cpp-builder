#pragma once

#include "../core/plato_stage.hh"
#include "plato_stream_impl.hh"

namespace plato {

using NodeFlowMap = std::unordered_map<PlatoNodeID, PlatoFlowID>;

/**
 * @brief 舞台实现类
 */
class PlatoStageImpl : public PlatoStage {
  using FlowVector = std::vector<PlatoFlowPtr>;
  FlowVector flow_array_;                     ///< 多个流
  DomainPtr domain_ptr_;                      ///< 变量域
  PlatoStageID id_{INVALID_STAGE_ID};         ///< 舞台ID
  std::unique_ptr<MemBlock> mb_;              ///< 内存块
  std::size_t mem_size_{0};                   ///< 内存块长度
  PlatoStreamImpl ostream_;                   ///< 输出数据流
  PlatoStreamImpl istream_;                   ///< 输入数据流
  PlatoHook hook_;                            ///< 钩子函数
  FlowRunMode run_mode_{FlowRunMode::ACTIVE}; ///< 流运行模式
  PlatoLogger *logger_{nullptr};              ///< 日志
  NodeFlowMap node_flow_map_;                 ///< {节点ID, 流ID}

public:
  PlatoStageImpl(PlatoStageID id, std::size_t mem_size,
                 FlowRunMode run_mode = FlowRunMode::ACTIVE);
  virtual ~PlatoStageImpl();
  virtual auto reset() -> void override;
  virtual auto id() -> PlatoStageID override;
  virtual auto domain() -> DomainPtr override;
  virtual auto add_flow() -> PlatoFlowPtr override;
  virtual auto update(std::time_t tick) -> void override;
  virtual auto get_flow_ostream() -> PlatoStream & override;
  virtual auto get_flow_istream() -> PlatoStream & override;
  virtual auto get_lifetime_state() -> PlatoStageLifetimeState;
  virtual auto get_flow_count() -> std::size_t override;
  virtual auto get_flow(std::size_t index) -> PlatoFlowPtr override;
  virtual auto set_hook(PlatoHook hook) -> PlatoHook override;
  virtual auto set_logger(PlatoLogger *logger) -> void override;
  virtual auto logger() -> PlatoLogger * override;
  virtual auto dispatch_event(PlatoNodeID node_id) -> void override;

public:
  /**
   * @brief 添加节点与流的对应关系
   * @param node_id 节点ID
   * @param flow_id 流ID
   * @return true或false
   */
  auto add_node_flow(PlatoNodeID node_id, PlatoFlowID flow_id) -> bool;
};

}
