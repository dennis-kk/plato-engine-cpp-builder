#include "../include/plato.hh"
#include "../builder/plato_builder.hh"
#include "../core/plato_node.hh"
#include "../core/plato_stage.hh"
#include "../impl/plato_flow_impl.hh"
#include "../impl/plato_logic_manager_impl.hh"
#include "../impl/plato_node_impl.hh"
#include "../impl/plato_pin_impl.hh"
#include "../impl/plato_stage_impl.hh"
#include "../impl/plato_stream_impl.hh"
#include "../logic/plato_node_meta_list.h"
#include <stack>
#include <unordered_map>
#include <vector>

auto plato::new_stage(PlatoStageID stage_id, PlatoStageBuilder *builder,
                      const std::string &config_file, FlowRunMode run_mode)
    -> PlatoStagePtr {
  if (!builder->pre_build(config_file)) {
    return nullptr;
  }
  return builder->build(stage_id, run_mode);
}

auto plato::pre_build_stage(PlatoStageBuilder *builder,
                            const std::string &config_file) -> bool {
  return builder->pre_build(config_file);
}

auto plato::new_stage(plato::PlatoStageID id, std::size_t mem_size,
                      FlowRunMode run_mode) -> plato::PlatoStagePtr {
  return std::make_shared<plato::PlatoStageImpl>(id, mem_size, run_mode);
}

auto plato::new_pin(DomainPtr domain_ptr, PlatoPinType pin_type,
                    VariablePtr var_ptr) -> PlatoPinPtr {
  return plato::make_shared<PlatoPinImpl>(domain_ptr->mem_block(), pin_type,
                                          var_ptr);
}

auto plato::new_node(DomainPtr domain_ptr, PlatoNodeID static_id,
                     PlatoNodeID id, PlatoNodeSyncType type) -> PlatoNodePtr {
  return plato::make_shared<PlatoNodeImpl>(domain_ptr->mem_block(), static_id,
                                           id, type);
}

auto plato::new_flow(PlatoFlowID flow_id, DomainPtr domain_ptr,
                     PlatoStage *stage) -> PlatoFlowPtr {
  return plato::make_shared<PlatoFlowImpl>(domain_ptr->mem_block(), flow_id,
                                           domain_ptr, stage);
}

auto plato::get_flow_size() -> std::size_t {
  return sizeof(plato::PlatoFlowImpl);
}

auto plato::get_pin_size() -> std::size_t {
  return sizeof(plato::PlatoPinImpl);
}

auto plato::get_node_size() -> std::size_t {
  return sizeof(plato::PlatoNodeImpl);
}

static plato::PlatoLogicManagerPtr global_logic_manager_ptr_;

auto plato::set_logic_manager(PlatoLogicManagerPtr manager_ptr) -> void {
  global_logic_manager_ptr_ = manager_ptr;
}

auto plato::get_logic_manager() -> PlatoLogicManagerPtr {
  return global_logic_manager_ptr_;
}

using PlatoBuilderMap =
    std::unordered_map<std::string, plato::PlatoStageBuilder *>;
PlatoBuilderMap builder_map_;

auto plato::new_plato_stage(std::uint64_t stage_id,
                            const std::string &stage_conf_file_path,
                            FlowRunMode run_mode, plato::PlatoLogger *logger)
    -> std::shared_ptr<plato::PlatoStage> {
  plato::PlatoStageBuilder *builder_pointer = nullptr;
  auto it = builder_map_.find(stage_conf_file_path);
  if (it == builder_map_.end()) {
    builder_pointer = new plato::PlatoStageBuilderImpl();
    builder_map_[stage_conf_file_path] = builder_pointer;
    if (!builder_pointer->pre_build(stage_conf_file_path)) {
      return nullptr;
    }
  } else {
    builder_pointer = it->second;
  }
  return builder_pointer->build(stage_id, run_mode);
}

auto plato::pre_build_plato_stage(const std::string &stage_conf_file_path,
                                  std::string &error) -> bool {
  plato::PlatoStageBuilder *builder_pointer = nullptr;
  auto it = builder_map_.find(stage_conf_file_path);
  if (it == builder_map_.end()) {
    builder_pointer = new plato::PlatoStageBuilderImpl();
    builder_map_[stage_conf_file_path] = builder_pointer;
    if (!builder_pointer->pre_build(stage_conf_file_path)) {
      error = builder_pointer->get_error();
      return false;
    }
  }
  return true;
}

#if !defined(__linux__)
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN // avoid including unnecessary WIN32 header files
#endif
#include <windows.h>
#define ModuleHandle HMODULE // DLL handle
#define INVALID_MODULE_HANDLE 0
#define MODULE_SUFFIX ".dll"
#undef max
#else
#include <dlfcn.h>
#define ModuleHandle void * // LINUX SO handle
#define INVALID_MODULE_HANDLE nullptr
#define MODULE_SUFFIX ".so"
#undef max
#endif // !defined(__linux__)

using BundleFunc = NodeLogicMeta *(*)(uint32_t *);
static const char *const REGISTER_NAME = "get_node_logic_meta_list";
static std::shared_ptr<plato::NodeLogicManagerImpl> node_logic_manager_ptr_;
ModuleHandle load_handle{INVALID_MODULE_HANDLE};

auto plato::load_logic_bundle(const std::string &logic_bundle_path,
                              std::string &error) -> bool {
  if (!node_logic_manager_ptr_) {
    node_logic_manager_ptr_.reset(new plato::NodeLogicManagerImpl());
  }
#ifdef WIN32
  load_handle = ::LoadLibraryA(logic_bundle_path.c_str());
#else
  load_handle = dlopen(logic_bundle_path.c_str(), RTLD_LAZY);
#endif // WIN32
  if (load_handle == INVALID_MODULE_HANDLE) {
    error = "Load bundle failed [" + logic_bundle_path + "]";
    return false;
  }
#ifdef WIN32
  auto reg_func = (BundleFunc)::GetProcAddress(load_handle, REGISTER_NAME);
#else
  auto reg_func = (BundleFunc)dlsym(load_handle, REGISTER_NAME);
#endif
  if (reg_func == nullptr) {
    error = "Load bundle failed [" + logic_bundle_path + "]," +
            "Cannot find register entry function [" + REGISTER_NAME + "]";
    unload_logic_bundle();
    return false;
  }
  std::uint32_t count = 0;
  auto *logic_meta_list = reg_func(&count);
  if (!logic_meta_list) {
    error = "Load bundle failed [" + logic_bundle_path + "]," +
            "Call node login meta function failed";
    return false;
  }
  for (std::uint32_t i = 0; i < count; i++) {
    auto *meta = logic_meta_list + count;
    node_logic_manager_ptr_->add_node_logic_instance(
        meta->node_static_id, std::shared_ptr<plato::PlatoNodeLogic>(
                                  reinterpret_cast<plato::PlatoNodeLogic *>(
                                      meta->node_logic_pointer)));
  }
  plato::set_logic_manager(node_logic_manager_ptr_);
  return true;
}

auto plato::reload_logic_bundle(const std::string &logic_bundle_path,
                                std::string &error) -> bool {
  unload_logic_bundle();
  return load_logic_bundle(logic_bundle_path, error);
}

auto plato::unload_logic_bundle() -> void {
  node_logic_manager_ptr_->reset();
  if (load_handle == INVALID_MODULE_HANDLE) {
    return;
  }
#ifdef WIN32
  if (FALSE == ::FreeLibrary(load_handle)) {
    ::UnmapViewOfFile(load_handle);
  }
#else
  dlclose(load_handle);
#endif // WIN32
  load_handle = INVALID_MODULE_HANDLE;
}
