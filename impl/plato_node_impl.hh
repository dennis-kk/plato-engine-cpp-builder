#pragma once

#include "../core/plato_node.hh"

namespace plato {

/**
 * @brief 节点实现类
 */
class PlatoNodeImpl : public PlatoNode {
  PlatoNodeID static_id_{0}; ///< 静态ID，对应节点逻辑的类型ID
  PlatoNodeID id_{0};        ///< 实例ID
  using PinArray = std::vector<PlatoPinPtr>;
  PinArray input_pins_;                                  ///< 输入引脚
  PinArray output_pins_;                                 ///< 输出引脚
  PlatoNodeSyncType sync_type_{PlatoNodeSyncType::NONE}; ///< 节点同步类型
  PlatoFlow *flow_{nullptr};                             ///< 节点所属流
  std::string name_;                                     ///< 节点名称

public:
  /**
   * @brief 构造
   * @param static_id 静态ID
   * @param id 实例ID
   * @param sync_type 同步类型
   */
  PlatoNodeImpl(PlatoNodeID static_id, PlatoNodeID id,
                PlatoNodeSyncType sync_type);
  virtual ~PlatoNodeImpl();
  virtual auto static_id() -> PlatoNodeID override;
  virtual auto id() -> PlatoNodeID override;
  virtual auto sync_type() -> PlatoNodeSyncType override;
  virtual auto get_input_pin(PlatoPinIndex index) -> PlatoPin * override;
  virtual auto get_output_pin(PlatoPinIndex index) -> PlatoPin * override;
  virtual auto get_input_size() -> std::size_t override;
  virtual auto get_output_size() -> std::size_t override;
  virtual auto add_input(PlatoPinPtr pin_ptr) -> void override;
  virtual auto add_output(PlatoPinPtr pin_ptr) -> void override;
  virtual auto flow() -> PlatoFlow * override;
  virtual auto object_size() -> std::size_t override;
  virtual auto reset() -> void override;
  virtual auto name() -> const char * override;
  virtual auto set_name(const char *name) -> void override;

public:
  /**
   * @brief 设置节点所属流
   * @param flow 流指针
   * @return
   */
  auto set_flow(PlatoFlow *flow) -> void { flow_ = flow; }
};

} // namespace plato
