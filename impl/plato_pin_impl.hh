#pragma once

#include "../core/plato_node.hh"
#include "../core/plato_stage.hh"

namespace plato {

/**
 * @brief 引脚实现类
 */
class PlatoPinImpl : public PlatoPin {
  PlatoPinType type_{PlatoPinType::NONE}; ///< 引脚类型
  VariablePtr var_ptr_;                   ///< 变量
  PlatoNodePtr backtrack_node_ptr_;       ///< 回溯节点实例

public:
  /**
   * @brief 构造
   * @param type 引脚类型
   * @param var_ptr 变量
   */
  PlatoPinImpl(PlatoPinType type, VariablePtr var_ptr);
  virtual ~PlatoPinImpl();
  virtual auto type() -> PlatoPinType override;
  virtual auto var() -> VariablePtr override;
  virtual auto backtrack_node() -> PlatoNodePtr override;
  virtual auto set_backtrack_node(PlatoNodePtr node_ptr) -> void override;
  virtual auto object_size() -> std::size_t override;
  virtual auto reset() -> void override;
  auto set_var(VariablePtr var_ptr) -> void;
};

} // namespace plato
