#include "plato_logic_manager_impl.hh"

plato::NodeLogicManagerImpl::NodeLogicManagerImpl() {}

plato::NodeLogicManagerImpl::~NodeLogicManagerImpl() {}

auto plato::NodeLogicManagerImpl::get_logic_instance(
    plato::PlatoNodeID static_id) -> plato::PlatoNodeLogicPtr {
  auto it = node_logic_inst_map_.find(static_id);
  if (it == node_logic_inst_map_.end()) {
    return nullptr;
  }
  return it->second;
}

auto plato::NodeLogicManagerImpl::add_node_logic_instance(
    PlatoNodeID static_id, PlatoNodeLogicPtr node_logic_ptr) -> bool {
  if (node_logic_inst_map_.find(static_id) !=
      node_logic_inst_map_.find(static_id)) {
    return false;
  }
  node_logic_inst_map_[static_id] = node_logic_ptr;
  return true;
}

auto plato::NodeLogicManagerImpl::reset() -> void {
  node_logic_inst_map_.clear();
}
