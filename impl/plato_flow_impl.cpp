#include "plato_flow_impl.hh"
#include "plato_node_impl.hh"
#include "plato_stage_impl.hh"

namespace plato {

PlatoFlowImpl::PlatoFlowImpl(PlatoFlowID flow_id, DomainPtr domain_ptr,
                             PlatoStage *stage_ptr) {
  flow_id_ = flow_id;
  domain_ptr_ = domain_ptr;
  stage_ptr_ = stage_ptr;
}

PlatoFlowImpl::~PlatoFlowImpl() {}

auto PlatoFlowImpl::set_start_node(PlatoNodePtr node_ptr) -> void {
  cur_node_ptr_ = node_ptr;
}

auto PlatoFlowImpl::add_node(PlatoNodePtr node_ptr) -> bool {
  if (node_map_.find(node_ptr->id()) != node_map_.end()) {
    return false;
  }
  node_map_.emplace(node_ptr->id(), node_ptr);
  // 设置流指针
  std::dynamic_pointer_cast<PlatoNodeImpl>(node_ptr)->set_flow(this);
  if (stage_ptr_) {
    // 添加{节点ID, 流ID}对应关系, 用于事件分发
    dynamic_cast<PlatoStageImpl *>(stage_ptr_)
        ->add_node_flow(node_ptr->id(), flow_id_);
  }
  return true;
}

auto PlatoFlowImpl::complete() -> bool {
  // 设置每个输入变量引脚的回溯节点
  for (auto &[_, node] : node_map_) {
    node->reset();
    for (PlatoPinIndex i = 0; i < (PlatoPinIndex)node->get_input_size(); i++) {
      auto link_search_it = link_search_map_.find({node->id(), i});
      if (link_search_it == link_search_map_.end()) {
        continue;
      }
      auto *pin_ptr = node->get_input_pin(i);
      if (pin_ptr->type() != PlatoPinType::VAR) {
        continue;
      }
      auto node_it = node_map_.find(link_search_it->second.node_id);
      if (node_it == node_map_.end()) {
        return false;
      }
      auto prev_node_ptr = node_it->second;
      auto output_exec_pin_index = get_exec_output_pin_index(prev_node_ptr);
      if (output_exec_pin_index != INVALID_PIN_INDEX) {
        auto link_it =
            link_map_.find({prev_node_ptr->id(), output_exec_pin_index});
        if (link_it != link_map_.end()) {
          if (link_it->second.node_id == node->id()) {
            // 相同的执行节点已经有exec连线了
            continue;
          }
        }
      }
      pin_ptr->set_backtrack_node(prev_node_ptr);
    }
  }
  return true;
}

auto PlatoFlowImpl::get_node(PlatoNodeID node_id) -> PlatoNodePtr {
  auto it = node_map_.find(node_id);
  if (it == node_map_.end()) {
    return nullptr;
  }
  return it->second;
}

auto PlatoFlowImpl::add_link(const LinkNode &start, const LinkNode &end)
    -> bool {
  if (link_map_.find(start) != link_map_.end()) {
    return false;
  }
  link_map_.emplace(start, end);
  link_search_map_.emplace(end, start);
  return true;
}

auto PlatoFlowImpl::get_domain() -> DomainPtr { return domain_ptr_; }

auto PlatoFlowImpl::get_current() -> PlatoNodePtr { return cur_node_ptr_; }

auto PlatoFlowImpl::get_ostream() -> PlatoStream & {
  if (!stage_ptr_) {
    return ostream_;
  }
  return stage_ptr_->get_flow_ostream();
}

auto PlatoFlowImpl::get_istream() -> PlatoStream & {
  if (!stage_ptr_) {
    return istream_;
  }
  return stage_ptr_->get_flow_istream();
}

auto PlatoFlowImpl::id() -> PlatoFlowID { return flow_id_; }

auto PlatoFlowImpl::reset() -> void {
  halt_ = false;
  cur_node_ptr_ = start_node_ptr_;
  for (auto &[_, v] : node_map_) {
    v->reset();
  }
}

auto PlatoFlowImpl::status() -> PlatoFlowStatus { return status_; }

auto PlatoFlowImpl::halt() -> void {
  if (halt_) {
    return;
  }
  cleanup();
  halt_ = true;
}

auto PlatoFlowImpl::dispatch_event(PlatoNodeID node_id) -> void {
  event_node_id_list_.push_back(node_id);
}

auto PlatoFlowImpl::stage() -> PlatoStage * { return stage_ptr_; }

/**
 * @brief 重置
 * @return
 */

auto PlatoFlowImpl::cleanup() -> void {
  BacktrackStack temp;
  backtrack_stack_.swap(temp);
  istream_.clear();
  ostream_.clear();
  event_node_id_list_.clear();
}

/**
 * @brief 获取某个节点的输出引脚类型为PlatoPinType::EXEC的引脚索引
 * @param node_ptr 节点实例
 * @return 引脚索引
 */

auto PlatoFlowImpl::get_exec_output_pin_index(PlatoNodePtr node_ptr)
    -> PlatoPinIndex {
  for (PlatoPinIndex i = 0; i < (PlatoPinIndex)node_ptr->get_output_size();
       i++) {
    if (node_ptr->get_output_pin(i)->type() == PlatoPinType::EXEC) {
      return i;
    }
  }
  return INVALID_PIN_INDEX;
}

/**
 * @brief 查找节点某个输出引脚对应的输入引脚
 * @param node_id 节点ID
 * @param output_pin_index 输出引脚索引
 * @return 输入引脚指针
 */

auto PlatoFlowImpl::get_next_pin(PlatoNodeID node_id,
                                 PlatoPinIndex output_pin_index) -> PlatoPin * {
  LinkNode ln{node_id, output_pin_index};
  // 查找连接的终点
  auto link_it = link_map_.find(ln);
  if (link_it == link_map_.end()) {
    return nullptr;
  }
  auto input_pin_index = link_it->second.pin_index;
  // 找到节点及对应的输入引脚
  auto node_it = node_map_.find(link_it->second.node_id);
  if (node_it == node_map_.end()) {
    return nullptr;
  }
  return node_it->second->get_input_pin(input_pin_index);
}

auto PlatoFlowImpl::update(PlatoHook hook, std::time_t tick,
                           FlowRunMode run_mode) -> PlatoFlowStatus {
  if (halt_) {
    return status_;
  }
  // 先同步变量
  domain_ptr_->do_sync();
  // 处理数据流的输入
  if (!do_command(hook, tick)) {
    return status_;
  }
  if (run_mode == FlowRunMode::PASSIVE) {
    // 被动模式主动返回
    return status_;
  }
  // 运行流
  return run(hook, tick);
}

/**
 * @brief 处理数据流内的输入数据
 * @param hook 钩子函数
 * @param tick 当前时间戳(毫秒)
 * @return true表示继续运行节点，false表示本次执行结束
 */

auto PlatoFlowImpl::do_command(PlatoHook hook, std::time_t tick) -> bool {
  if (get_istream().available()) {
    // 跳过流ID
    get_istream().skip(sizeof(PlatoFlowID));
    // 获取命令
    PlatoSyncCommandID command_id{INVALID_SYNC_COMMAND_ID};
    get_istream() >> command_id;
    switch (PlatoSyncCommand(command_id)) {
    case PlatoSyncCommand::CALL: // 远程调用
      call(hook, tick);
      return false;
    case PlatoSyncCommand::ACK: // 调用返回
      ack();
      break;
    default:
      break;
    }
  }
  return true;
}

/**
 * @brief 调用本地节点
 * @return
 */

auto PlatoFlowImpl::call(PlatoHook hook, std::time_t tick) -> void {
  // 获取节点ID
  PlatoNodeID node_id{INVALID_NODE_ID};
  get_istream() >> node_id;
  // 查找节点
  auto it = node_map_.find(node_id);
  if (it == node_map_.end()) {
    // 节点未找到

    // 写入流ID
    get_ostream() << flow_id_;
    // PlatoSyncCommand
    get_ostream() << (PlatoSyncCommandID)PlatoSyncCommand::ACK;
    // 写入节点ID
    get_ostream() << node_id;
    // PlatoFlowStatus
    get_ostream() << (PlatoFlowStatusID)PlatoFlowStatus::NOT_FOUND;
  } else {
    // 调用节点并立即返回，不继续运行流后续节点
    // 设置当前节点
    cur_node_ptr_ = it->second;
    // 运行当前节点
    auto output_index = run_once(hook, tick, true, false);
    // 写入流ID
    get_ostream() << flow_id_;
    // PlatoSyncCommand
    get_ostream() << (PlatoSyncCommandID)PlatoSyncCommand::ACK;
    // 写入节点ID
    get_ostream() << node_id;
    // PlatoFlowStatus
    get_ostream() << (PlatoFlowStatusID)status_;
    // 后续输出引脚
    get_ostream() << output_index;
  }
}

/**
 * @brief 远程调用的返回
 * @return
 */

auto PlatoFlowImpl::ack() -> void {
  get_istream().skip(sizeof(PlatoNodeID));
  PlatoFlowStatusID status_id;
  get_istream() >> status_id;
  if (PlatoFlowStatus(status_id) == PlatoFlowStatus::NOT_FOUND) {
    status_ = PlatoFlowStatus::NOT_FOUND;
    return;
  }
  PlatoPinIndex output_index = INVALID_PIN_INDEX;
  get_istream() >> output_index;
  // 根据返回值进行下一步运行
  cur_node_ptr_ = get_next_node(output_index);
  status_ = PlatoFlowStatus::OK;
}

/**
 * @brief 运行流
 * @return 流状态
 */

auto PlatoFlowImpl::run(PlatoHook hook, std::time_t tick) -> PlatoFlowStatus {
  switch (status_) {
  case PlatoFlowStatus::DONE:
  case PlatoFlowStatus::ERROR:
  case PlatoFlowStatus::NONE:
  case PlatoFlowStatus::NOT_FOUND:
  case PlatoFlowStatus::INVOKING:
    return status_;
  case PlatoFlowStatus::RUNNING:
    // 让节点逻辑再次检测并改变运行状态
    break;
  default:
    break;
  }
  for (;;) {
    run_once(hook, tick, false, true);
    if (status_ != PlatoFlowStatus::OK) {
      break;
    }
  }
  if (status_ == PlatoFlowStatus::DONE) {
    cleanup();
  }
  return status_;
}

/**
 * @brief 获取当前节点的某个输出引脚对应的下一个节点
 * @param output_index 输出引脚索引
 * @return 节点
 */

auto PlatoFlowImpl::get_next_node(PlatoPinIndex output_index) -> PlatoNodePtr {
  LinkNode ln{cur_node_ptr_->id(), output_index};
  auto link_it = link_map_.find(ln);
  if (link_it == link_map_.end()) {
    return nullptr;
  }
  auto node_it = node_map_.find(link_it->second.node_id);
  if (node_it == node_map_.end()) {
    return nullptr;
  }
  return node_it->second;
}

/**
 * @brief 检查当前节点是否为远程节点
 * @return true或false
 */

auto PlatoFlowImpl::is_remote_node() -> bool {
  return (cur_node_ptr_->sync_type() == PlatoNodeSyncType::CLIENT_SIDE);
}

/**
 * @brief 调用远程节点
 * @return
 */

auto PlatoFlowImpl::do_remote_call() -> void {
  get_ostream() << flow_id_;
  get_ostream() << (PlatoSyncCommandID)PlatoSyncCommand::CALL;
  get_ostream() << cur_node_ptr_->id();
  status_ = PlatoFlowStatus::INVOKING;
}

/**
 * @brief 获取回溯执行完成后下一个需要检测的输入引脚索引
 * @return 输入引脚索引
 */

auto PlatoFlowImpl::get_backtrack_pin_index() -> PlatoPinIndex {
  PlatoPinIndex backtrack_pin_index = INVALID_PIN_INDEX;
  if (!backtrack_stack_.empty()) {
    if (cur_node_ptr_ == backtrack_stack_.top().node_ptr) {
      backtrack_pin_index = backtrack_stack_.top().cur_input_index + 1;
      backtrack_stack_.pop();
    }
  }
  return backtrack_pin_index;
}

/**
 * @brief 尝试获取回溯的节点
 * @param backtrack_pin_index 当前的输入引脚索引
 * @return true成功找到，false未找到
 */

auto PlatoFlowImpl::try_get_backtrack_node(PlatoPinIndex backtrack_pin_index)
    -> bool {
  for (PlatoPinIndex i = backtrack_pin_index;
       i < (PlatoPinIndex)cur_node_ptr_->get_input_size(); i++) {
    auto *input_pin = cur_node_ptr_->get_input_pin(i);
    if ((input_pin->type() == PlatoPinType::VAR) &&
        input_pin->backtrack_node()) {
      backtrack_stack_.push({cur_node_ptr_, i});
      cur_node_ptr_ = input_pin->backtrack_node();
      status_ = PlatoFlowStatus::OK;
      return true;
    }
  }
  return false;
}

/**
 * @brief 检测是否有当前节点的事件触发
 * @return true或false
 */

auto PlatoFlowImpl::check_event() -> bool {
  if (status_ == PlatoFlowStatus::RUNNING) {
    if (!event_node_id_list_.empty()) {
      const auto &event_node_id = event_node_id_list_.front();
      if (event_node_id == cur_node_ptr_->id()) {
        event_node_id_list_.pop_front();
        return true;
      }
    }
  }
  return false;
}

/**
 * @brief 运行当前节点
 * @param hook 钩子函数
 * @param tick 当前时间戳(毫秒)
 * @param stop
 * true表示不推进流，false表示运行完成后将当前节点设置为下一个节点
 * @return 输出引脚索引
 */

auto PlatoFlowImpl::run_once(plato::PlatoHook hook, std::time_t tick, bool stop,
                             bool backtrack) -> PlatoPinIndex {
  if (!cur_node_ptr_) {
    status_ = PlatoFlowStatus::DONE;
    return INVALID_PIN_INDEX;
  }
  // 是否要跳过钩子函数
  bool skip_hook = false;
  if (status_ == PlatoFlowStatus::DEBUG_SUSPEND_BEFORE_RUN) {
    if (hook) {
      auto status = hook(cur_node_ptr_.get(), HookEvent::BEFORE_RUN,
                         PlatoFlowStatus::DEBUG_SUSPEND_BEFORE_RUN);
      if (status != HookAction::OK) {
        // 钩子函数要求挂起这个流
        status_ = PlatoFlowStatus::DEBUG_SUSPEND_BEFORE_RUN;
        return INVALID_PIN_INDEX;
      } else {
        // 钩子函数运行完成后之前的挂起被取消
        status_ = PlatoFlowStatus::OK;
        skip_hook = true;
      }
    }
  }
  PlatoPinIndex backtrack_pin_index = get_backtrack_pin_index();
  if (backtrack && !stop) {
    // 正常执行，stop为true时表示被远端调用，不进行回溯
    if (backtrack_pin_index == INVALID_PIN_INDEX) {
      backtrack_pin_index = 0;
    }
    // 输入引脚回溯遍历并执行
    if (try_get_backtrack_node(backtrack_pin_index)) {
      return INVALID_PIN_INDEX;
    }
  }
  if (is_remote_node()) {
    // 远程执行
    do_remote_call();
    return INVALID_PIN_INDEX;
  }
  // 获取节点逻辑的静态ID
  auto static_id = cur_node_ptr_->static_id();
  if (!get_logic_manager()) {
    // 节点逻辑未找到
    status_ = PlatoFlowStatus::ERROR;
    if (stage_ptr_->logger()) {
      stage_ptr_->logger()->write("Plato node logic manager not found");
    }
    return INVALID_PIN_INDEX;
  }
  if (hook && !skip_hook) {
    auto status =
        hook(cur_node_ptr_.get(), HookEvent::BEFORE_RUN, PlatoFlowStatus::NONE);
    if (status != HookAction::OK) {
      status_ = PlatoFlowStatus::DEBUG_SUSPEND_BEFORE_RUN;
      return INVALID_PIN_INDEX;
    }
  }
  return run_once_internal(tick, static_id, stop, check_event());
}

auto PlatoFlowImpl::run_once_internal(std::time_t tick, PlatoNodeID static_id,
                                      bool stop, bool is_event)
    -> PlatoPinIndex {
  // 获取节点逻辑
  auto logic_ptr = get_logic_manager()->get_logic_instance(static_id);
  if (!logic_ptr) {
    // TODO 语法糖节点，没有逻辑
    // 节点逻辑未找到
    if (stage_ptr_->logger()) {
      stage_ptr_->logger()->write(
          std::string("Plato node logic not found, node name[") +
          cur_node_ptr_->name() + "]");
    }
    status_ = PlatoFlowStatus::ERROR;
    return INVALID_PIN_INDEX;
  }
  if (is_event) {
    // 节点由事件触发
    logic_ptr->do_event(cur_node_ptr_.get());
  }
  // 运行节点逻辑
  auto result = logic_ptr->do_logic(cur_node_ptr_.get(), tick, status_);
  if (status_ != PlatoFlowStatus::OK) {
    // 暂停或错误
    return INVALID_PIN_INDEX;
  }
  if (result.output_pin_index == INVALID_PIN_INDEX) {
    // 没有后续节点
    status_ = PlatoFlowStatus::DONE;
    return INVALID_PIN_INDEX;
  }
  // 获取输出引脚数量
  auto output_size = cur_node_ptr_->get_output_size();
  // 针对所有输出引脚，将变量内容拷贝到对应输出引脚的变量内
  for (PlatoPinIndex i = 0; i < (PlatoPinIndex)output_size; i++) {
    auto *pin_ptr = cur_node_ptr_->get_output_pin(i);
    if (pin_ptr->type() == PlatoPinType::VAR) {
      // 获取下一个节点对应的输入引脚
      auto *next_pin = get_next_pin(cur_node_ptr_->id(), i);
      if (!next_pin) {
        continue;
      }
      // 输出引脚到输入引脚的变量内容拷贝
      next_pin->var()->copy(pin_ptr->var().get());
    }
  }
  if (!stop) {
    // 查找并设置流内的下一个节点
    cur_node_ptr_ = get_next_node(result.output_pin_index);
    if (!cur_node_ptr_) {
      status_ = PlatoFlowStatus::DONE;
    }
  }
  // 返回输出引脚索引
  return result.output_pin_index;
}

} // namespace plato
