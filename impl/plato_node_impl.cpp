#include "plato_node_impl.hh"

/**
 * @brief 构造
 * @param static_id 静态ID
 * @param id 实例ID
 * @param sync_type 同步类型
 */

plato::PlatoNodeImpl::PlatoNodeImpl(PlatoNodeID static_id, PlatoNodeID id,
                                    PlatoNodeSyncType sync_type) {
  static_id_ = static_id;
  id_ = id;
  sync_type_ = sync_type;
}

plato::PlatoNodeImpl::~PlatoNodeImpl() {}

auto plato::PlatoNodeImpl::static_id() -> PlatoNodeID { return static_id_; }

auto plato::PlatoNodeImpl::id() -> PlatoNodeID { return id_; }

auto plato::PlatoNodeImpl::sync_type() -> PlatoNodeSyncType {
  return sync_type_;
}

auto plato::PlatoNodeImpl::get_input_pin(PlatoPinIndex index) -> PlatoPin * {
  if (index >= (PlatoPinIndex)input_pins_.size()) {
    return nullptr;
  }
  return input_pins_[index].get();
}

auto plato::PlatoNodeImpl::get_output_pin(PlatoPinIndex index) -> PlatoPin * {
  if (index >= (PlatoPinIndex)output_pins_.size()) {
    return nullptr;
  }
  return output_pins_[index].get();
}

auto plato::PlatoNodeImpl::get_input_size() -> std::size_t {
  return input_pins_.size();
}

auto plato::PlatoNodeImpl::get_output_size() -> std::size_t {
  return output_pins_.size();
}

auto plato::PlatoNodeImpl::add_input(PlatoPinPtr pin_ptr) -> void {
  input_pins_.emplace_back(pin_ptr);
}

auto plato::PlatoNodeImpl::add_output(PlatoPinPtr pin_ptr) -> void {
  output_pins_.emplace_back(pin_ptr);
}

auto plato::PlatoNodeImpl::flow() -> PlatoFlow * { return flow_; }

auto plato::PlatoNodeImpl::object_size() -> std::size_t {
  return sizeof(PlatoNodeImpl);
}

auto plato::PlatoNodeImpl::reset() -> void {
  for (auto &pin : input_pins_) {
    pin->reset();
  }
  for (auto &pin : output_pins_) {
    pin->reset();
  }
}

auto plato::PlatoNodeImpl::name() -> const char * { return name_.c_str(); }

auto plato::PlatoNodeImpl::set_name(const char *name) -> void { name_ = name; }
