#pragma once

#include "../core/plato_stage.hh"
#include "plato_stream_impl.hh"
#include <stack>

namespace plato {

struct KeyHash {
  std::size_t operator()(const LinkNode &k) const {
    return std::hash<std::uint32_t>()(k.node_id) ^
           (std::hash<std::uint32_t>()(k.pin_index) << 1);
  }
};

struct KeyEqual {
  bool operator()(const LinkNode &lhs, const LinkNode &rhs) const {
    return lhs.node_id == rhs.node_id && lhs.pin_index == rhs.pin_index;
  }
};
/**
 * @brief 连线对
 */
struct LinkPair {
  LinkNode prev; ///< 上一个接口
  LinkNode next; ///< 下一个接口
};

/**
 * @brief 流实现类
 */
class PlatoFlowImpl : public PlatoFlow {
  using LinkMap = std::unordered_map<LinkNode, LinkNode, KeyHash, KeyEqual>;
  using LinkSearchMap =
      std::unordered_map<LinkNode, LinkPair, KeyHash, KeyEqual>;
  using NodeMap = std::unordered_map<PlatoNodeID, PlatoNodePtr>;
  using EventNodeIDList = std::list<PlatoNodeID>;
  LinkMap link_search_map_; ///< {终点，起点}
  LinkMap link_map_;        ///< {起点，终点}
  NodeMap node_map_;        ///< 节点表
  DomainPtr domain_ptr_; ///< 变量域，流内所有变量都属于这个域
  PlatoNodePtr cur_node_ptr_;                   ///< 当前正在执行的节点
  PlatoNodePtr start_node_ptr_;                 ///< 起始节点
  PlatoFlowID flow_id_{0};                      ///< 流ID
  PlatoFlowStatus status_{PlatoFlowStatus::OK}; ///< 流状态
  PlatoStreamImpl ostream_;                     ///< 输出数据流
  PlatoStreamImpl istream_;                     ///< 输入数据流
  PlatoStage *stage_ptr_{nullptr};              ///< 舞台
  EventNodeIDList event_node_id_list_; ///< 事件需要触发的节点ID队列
  /**
   * @brief 回溯堆栈元素
   */
  struct Backtrack {
    PlatoNodePtr node_ptr; ///< 当前栈节点
    PlatoPinIndex cur_input_index{
        INVALID_PIN_INDEX}; ///< 当前被回溯的输入引脚索引
  };
  using BacktrackStack = std::stack<Backtrack>;
  BacktrackStack backtrack_stack_; ///< 执行过程中使用的回溯栈
  bool halt_{false};               ///< 停止标志

public:
  PlatoFlowImpl(PlatoFlowID flow_id, DomainPtr domain_ptr,
                PlatoStage *stage_ptr);
  virtual ~PlatoFlowImpl();
  virtual auto set_start_node(PlatoNodePtr node_ptr) -> void override;
  virtual auto add_node(PlatoNodePtr node_ptr) -> bool override;
  virtual auto complete() -> bool override;
  virtual auto get_node(PlatoNodeID node_id) -> PlatoNodePtr override;
  virtual auto add_link(const LinkNode &start, const LinkNode &end)
      -> bool override;
  virtual auto get_domain() -> DomainPtr override;
  virtual auto get_current() -> PlatoNodePtr override;
  virtual auto get_ostream() -> PlatoStream & override;
  virtual auto get_istream() -> PlatoStream & override;
  virtual auto id() -> PlatoFlowID override;
  virtual auto reset() -> void override;
  virtual auto status() -> PlatoFlowStatus override;
  virtual auto halt() -> void override;
  virtual auto dispatch_event(PlatoNodeID node_id) -> void override;
  virtual auto stage() -> PlatoStage * override;
  virtual auto update(PlatoHook hook, std::time_t tick, FlowRunMode run_mode)
      -> PlatoFlowStatus override;

private:
  /**
   * @brief 重置
   * @return
   */
  auto cleanup() -> void;
  /**
   * @brief 获取某个节点的输出引脚类型为PlatoPinType::EXEC的引脚索引
   * @param node_ptr 节点实例
   * @return 引脚索引
   */
  auto get_exec_output_pin_index(PlatoNodePtr node_ptr) -> PlatoPinIndex;
  /**
   * @brief 查找节点某个输出引脚对应的输入引脚
   * @param node_id 节点ID
   * @param output_pin_index 输出引脚索引
   * @return 输入引脚指针
   */
  auto get_next_pin(PlatoNodeID node_id, PlatoPinIndex output_pin_index)
      -> PlatoPin *;
  /**
   * @brief 处理数据流内的输入数据
   * @param hook 钩子函数
   * @param tick 当前时间戳(毫秒)
   * @return true表示继续运行节点，false表示本次执行结束
   */
  auto do_command(PlatoHook hook, std::time_t tick) -> bool;
  /**
   * @brief 调用本地节点
   * @return
   */
  auto call(PlatoHook hook, std::time_t tick) -> void;
  /**
   * @brief 远程调用的返回
   * @return
   */
  auto ack() -> void;
  /**
   * @brief 运行流
   * @return 流状态
   */
  auto run(PlatoHook hook, std::time_t tick) -> PlatoFlowStatus;
  /**
   * @brief 获取当前节点的某个输出引脚对应的下一个节点
   * @param output_index 输出引脚索引
   * @return 节点
   */
  auto get_next_node(PlatoPinIndex output_index) -> PlatoNodePtr;
  /**
   * @brief 检查当前节点是否为远程节点
   * @return true或false
   */
  auto is_remote_node() -> bool;
  /**
   * @brief 调用远程节点
   * @return
   */
  auto do_remote_call() -> void;
  /**
   * @brief 获取回溯执行完成后下一个需要检测的输入引脚索引
   * @return 输入引脚索引
   */
  auto get_backtrack_pin_index() -> PlatoPinIndex;
  /**
   * @brief 尝试获取回溯的节点
   * @param backtrack_pin_index 当前的输入引脚索引
   * @return true成功找到，false未找到
   */
  auto try_get_backtrack_node(PlatoPinIndex backtrack_pin_index) -> bool;
  /**
   * @brief 检测是否有当前节点的事件触发
   * @return true或false
   */
  auto check_event() -> bool;
  /**
   * @brief 运行当前节点
   * @param hook 钩子函数
   * @param tick 当前时间戳(毫秒)
   * @param stop
   * true表示不推进流，false表示运行完成后将当前节点设置为下一个节点
   * @return 输出引脚索引
   */
  auto run_once(plato::PlatoHook hook, std::time_t tick, bool stop,
                bool backtrack) -> PlatoPinIndex;
  /**
   * @brief 运行当前节点，不进行回溯处理，不运行钩子函数
   * @param tick 当前时间戳(毫秒)
   * @param static_id 节点静态ID
   * @param stop true表示不推进流，false表示运行完成后将当前节点设置为下一个节点
   * @param is_event 当前节点事件是否已经触发
   * @return 输出引脚索引
  */
  auto run_once_internal(std::time_t tick, PlatoNodeID static_id, bool stop, bool is_event) -> PlatoPinIndex;
};

} // namespace plato
