#include "plato_stage_impl.hh"

namespace plato {

/**
 * @brief 添加节点与流的对应关系
 * @param node_id 节点ID
 * @param flow_id 流ID
 * @return true或false
 */

PlatoStageImpl::PlatoStageImpl(PlatoStageID id, std::size_t mem_size,
                               FlowRunMode run_mode) {
  id_ = id;
  mem_size_ = mem_size;
  mb_ = new_mem_block();
  mb_->reset(mem_size);
  domain_ptr_ = new_domain((DomainID)id, mb_.get());
  run_mode = run_mode_;
}

PlatoStageImpl::~PlatoStageImpl() {
  // 这里必须手动释放，防止MemBlock先释放
  flow_array_.clear();
  domain_ptr_.reset();
}

auto PlatoStageImpl::reset() -> void {
  // 清理变量黑板
  domain_ptr_->reset_named_var();
  // 清理流
  for (auto &flow : flow_array_) {
    flow->reset();
  }
  // 清理数据流
  domain_ptr_->get_stream().clear();
  ostream_.clear();
  istream_.clear();
}

auto PlatoStageImpl::id() -> PlatoStageID { return id_; }

auto PlatoStageImpl::domain() -> DomainPtr { return domain_ptr_; }

auto PlatoStageImpl::add_flow() -> PlatoFlowPtr {
  auto flow_ptr =
      new_flow((PlatoFlowID)(flow_array_.size() + 1), domain_ptr_, this);
  flow_array_.emplace_back(flow_ptr);
  return flow_ptr;
}

auto PlatoStageImpl::update(std::time_t tick) -> void {
  for (auto &flow : flow_array_) {
    flow->update(hook_, tick, run_mode_);
    if (flow->status() == PlatoFlowStatus::ERROR ||
        flow->status() == PlatoFlowStatus::NOT_FOUND) {
      flow->halt();
    }
  }
}

auto PlatoStageImpl::get_flow_ostream() -> PlatoStream & { return ostream_; }

auto PlatoStageImpl::get_flow_istream() -> PlatoStream & { return istream_; }

auto PlatoStageImpl::get_lifetime_state() -> PlatoStageLifetimeState {
  return PlatoStageLifetimeState::RUNNING;
}

auto PlatoStageImpl::get_flow_count() -> std::size_t {
  return flow_array_.size();
}

auto PlatoStageImpl::get_flow(std::size_t index) -> PlatoFlowPtr {
  if (index >= flow_array_.size()) {
    return nullptr;
  }
  return flow_array_[index];
}

auto PlatoStageImpl::set_hook(PlatoHook hook) -> PlatoHook {
  auto old_hook = hook_;
  hook_ = hook;
  return old_hook;
}

auto PlatoStageImpl::set_logger(PlatoLogger *logger) -> void {
  logger_ = logger;
}

auto PlatoStageImpl::logger() -> PlatoLogger * { return logger_; }

auto PlatoStageImpl::dispatch_event(PlatoNodeID node_id) -> void {
  auto it = node_flow_map_.find(node_id);
  if (it == node_flow_map_.end()) {
    return;
  }
  flow_array_[it->second]->dispatch_event(node_id);
}

auto PlatoStageImpl::add_node_flow(PlatoNodeID node_id, PlatoFlowID flow_id)
    -> bool {
  if (node_flow_map_.find(node_id) != node_flow_map_.end()) {
    return false;
  }
  node_flow_map_[node_id] = flow_id;
  return true;
}

} // namespace plato
