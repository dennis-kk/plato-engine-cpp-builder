#pragma once

#include "../core/plato_node.hh"
#include <unordered_map>

namespace plato {

/**
 * @brief 节点逻辑管理器
 */
class NodeLogicManagerImpl : public plato::PlatoLogicManager {
  using NodeLogicInstMap = std::unordered_map<PlatoNodeID, PlatoNodeLogicPtr>;
  NodeLogicInstMap node_logic_inst_map_; ///< 节点逻辑表

public:
  NodeLogicManagerImpl();
  virtual ~NodeLogicManagerImpl();
  virtual auto get_logic_instance(plato::PlatoNodeID static_id)
      -> plato::PlatoNodeLogicPtr override;

public:
  /**
   * @brief 添加节点逻辑实例
   * @param static_id 节点静态ID
   * @param node_logic_ptr 节点逻辑实例
   * @return true成功,false失败
   */
  auto add_node_logic_instance(PlatoNodeID static_id,
                               PlatoNodeLogicPtr node_logic_ptr) -> bool;
  /**
   * @brief 清理并重置
   * @return
   */
  auto reset() -> void;
};

} // namespace plato
