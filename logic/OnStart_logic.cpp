#include "../core/plato_node.hh"

namespace plato {

class LogicOnStart : public PlatoNodeLogic {
public:
  LogicOnStart() {}
  virtual ~LogicOnStart() {}
  virtual auto do_logic(plato::PlatoNode * /*node*/, std::time_t /*tick*/, plato::PlatoFlowStatus & /*status*/) -> ExecResult override {
    return ExecResult{INVALID_PIN_INDEX};
  }
  virtual auto do_event(plato::PlatoNode * /*node*/) -> void override {}
};
}

extern auto add_node_logic_meta(plato::PlatoNodeID static_id,  plato::PlatoNodeLogic *node_logic_pointer) -> bool;
auto result_logic_LogicOnStart = add_node_logic_meta(1, new plato::LogicOnStart());
