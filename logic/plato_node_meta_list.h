#ifndef PLATO_NODE_META_LIST_HH
#define PLATO_NODE_META_LIST_HH

#if !defined(__linux__)
#define FuncExport extern "C" __declspec(dllexport) /* WIN32 DLL exporter */
#else
#define FuncExport extern "C" /* LINUX shared object exporter */
#endif                        /* !defined(__linux__) */

#include <stdint.h>

typedef struct {
  uint32_t node_static_id;
  void *node_logic_pointer;
} NodeLogicMeta;

FuncExport NodeLogicMeta *get_node_logic_meta_list(uint32_t *count);

#endif /* PLATO_NODE_META_LIST_HH */
