#include "plato_node_meta_list.h"
#include "../core/plato_node.hh"

static constexpr std::uint32_t DEFAULT_MAX_COUNT = 8196;
static char node_logic_meta_list[sizeof(NodeLogicMeta) * DEFAULT_MAX_COUNT];
static std::uint32_t max_count = DEFAULT_MAX_COUNT;
static std::uint32_t cur_count = 0;

FuncExport NodeLogicMeta *get_node_logic_meta_list(uint32_t *count) {
  *count = cur_count;
  return reinterpret_cast<NodeLogicMeta *>(node_logic_meta_list);
}

auto add_node_logic_meta(plato::PlatoNodeID static_id,
                         plato::PlatoNodeLogic *node_logic_pointer) -> bool {
  if (cur_count >= DEFAULT_MAX_COUNT) {
    return false;
  }
  auto &meta = *reinterpret_cast<NodeLogicMeta *>(
      node_logic_meta_list + sizeof(NodeLogicMeta) * cur_count);
  meta.node_static_id = static_id;
  meta.node_logic_pointer = node_logic_pointer;
  cur_count += 1;
  return true;
}
