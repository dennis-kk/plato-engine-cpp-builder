import json
import os
import sys
import getopt

global cwd
cwd = os.getcwd()
global abs_path
abs_path = os.path.abspath(os.path.dirname(__file__)) 
if cwd != abs_path:
    os.chdir(abs_path)

class NodeGenerator:
    def __init__(self, file_path, out_dir):
        self.file_path = file_path
        self.file_name = self.get_filename_without_extension(file_path)
        self.out_dir = out_dir
        self.gen_plato_base_cpp()
        self.gen_logic_bundle_cmake()

    def gen_logic_cpp(self, cont):
        for category in cont['categories']:
            for node in category['nodes']:
                self.gen_node_logic_cpp(node)

    def gen_logic_bundle_cmake(self):
        file_name = os.path.join(self.out_dir, 'CMakeLists.txt')
        file = open(file_name, "w")
        file.write('cmake_minimum_required(VERSION 3.5)\n')
        file.write('set(CMAKE_CXX_STANDARD 17)\n')
        file.write('project (plato_logic)\n')
        file.write('set(SRC_LIST .)\n')
        file.write('aux_source_directory(. SRC_LIST)\n')
        file.write('set(LIBRARY_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/../bin)\n')
        file.write('SET(CMAKE_CURRENT_BINARY_DIR ${CMAKE_SOURCE_DIR}/temp)\n')
        file.write('add_library(plato_logic SHARED ${SRC_LIST})\n')
        file.write('IF (MSVC)\n')
        file.write('  set_target_properties(plato_logic  PROPERTIES COMPILE_FLAGS "/EHa /bigobj /wd4267 /wd4065 /wd4250")\n')
        file.write('  foreach(var\n')
        file.write('    CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE\n')
        file.write('    CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO\n')
        file.write('    CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE\n')
        file.write('    CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO\n')
        file.write('    )\n')
        file.write('    if(${var} MATCHES "/MD")\n')
        file.write('      string(REGEX REPLACE "/MD" "/MT" ${var} "${${var}}")\n')
        file.write('    endif()\n')
        file.write('  endforeach()\n')
        file.write('ELSE()\n')
        file.write('  if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")\n')
        file.write('    SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")\n')
        file.write('    SET(CMAKE_CXX_COMPILER "clang++")\n')
        file.write('  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")\n')
        file.write('    SET(CMAKE_CXX_FLAGS "-m64 -g -O2 -std=c++17 -Wall -Wno-deprecated-declarations -fnon-call-exceptions -fPIC")\n')
        file.write('    SET(CMAKE_CXX_COMPILER "g++")\n')
        file.write('  endif()\n')
        file.write('ENDIF ()\n')
        file.write('IF (MSVC)\n')
        file.write('  set_target_properties(plato_logic PROPERTIES COMPILE_FLAGS "/EHa")\n')
        file.write('ENDIF ()\n')
        file.write('SET_TARGET_PROPERTIES(plato_logic PROPERTIES PREFIX "lib")\n')
        file.write('SET_TARGET_PROPERTIES(plato_logic PROPERTIES SUFFIX ".so")\n')

    def gen_node_logic_cpp(self, node):
        cpp_file_path = os.path.join(self.out_dir, node['title'])+'_logic.cpp'
        if os.path.exists(cpp_file_path):
            return
        file = open(cpp_file_path, "w")
        node_name = node['title']
        file.write('#include "../core/plato_node.hh"\n\n')
        file.write('namespace plato {\n\n')
        file.write('class Logic'+node_name+' : public PlatoNodeLogic {\n')
        file.write('public:\n')
        file.write('  Logic'+node_name+'() {}\n')
        file.write('  virtual ~Logic'+node_name+'() {}\n')
        file.write('  virtual auto do_logic(plato::PlatoNode * /*node*/, std::time_t /*tick*/, plato::PlatoFlowStatus & /*status*/) -> ExecResult override {\n')
        file.write('    return ExecResult{INVALID_PIN_INDEX};\n')
        file.write('  }\n')
        file.write('  virtual auto do_event(plato::PlatoNode * /*node*/) -> void override {}\n')
        file.write('};\n')
        file.write('}\n\n')
        file.write('extern auto add_node_logic_meta(plato::PlatoNodeID static_id,  plato::PlatoNodeLogic *node_logic_pointer) -> bool;\n')
        file.write('auto result_logic_Logic'+node_name+' = add_node_logic_meta(1, new plato::Logic'+node_name+'());\n')

    def gen_plato_base_cpp(self):
        file = open(self.file_path).read()
        cont = json.loads(file)
        self.gen_logic_cpp(cont)

    def get_filename_without_extension(self, file_path):
        file_basename = os.path.basename(file_path)
        return file_basename.split('.')[0]

if __name__ == "__main__":
    opts = None
    args = None
    opts,args = getopt.getopt(sys.argv[1:],'-f:-o:',['file=','out='])
    file_path = "."
    out_dir = "."
    for opt, value in opts:
        if opt in ("-o", "--out"):
            out_dir = value
        elif opt in ("-f", "--file"):
            file_path = value
    if os.path.exists(file_path):
        NodeGenerator(file_path, out_dir)
    # try:
    #     opts,args = getopt.getopt(sys.argv[1:],'-f:-o',['file=','out='])
    #     file_path = "."
    #     out_dir = "."
    #     for opt, value in opts:
    #         if opt in ("-o", "--out"):
    #             out_dir = value
    #         elif opt in ("-f", "--file"):
    #             file_path = value
    #     if os.path.exists(file_path):
    #         NodeGenerator(file_path, out_dir)
    # except getopt.GetoptError as e:
    #     print(str(e))
