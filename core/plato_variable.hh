#pragma once

#include <cmath>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "plato_memory.hh"
#include "plato_stream.hh"

namespace std {
/**
 * @brief 检查是否是std::string类型
 * @tparam T
 */
template <typename T>
struct is_string
    : public std::disjunction<
          std::is_same<std::string, typename std::decay<T>::type>> {};
} // namespace std

namespace plato {

using TypeID = std::uint8_t;
using SyncType = std::uint8_t;
using VarID = std::uint32_t;
using ArrayOpType = std::uint8_t;
using MapOpType = std::uint8_t;
using SetOpType = std::uint8_t;
using DomainID = std::uint64_t;
constexpr static VarID INVALID_VAR_ID = 0;
constexpr static DomainID INVALID_DOMAIN_ID = 0;

/**
 * @brief Plato变量类型
 */
enum class PlatoType : TypeID {
  NONE = 0,
  BOOL = 1,
  INT8,
  UINT8,
  INT16,
  UINT16,
  INT32,
  UINT32,
  INT64,
  UINT64,
  STRING,
  FLOAT,
  DOUBLE,
  STRUCT,
  ARRAY,
  SET,
  MAP,
};

/**
 * @brief 同步类型
 */
enum class PlatoVariableSyncType : SyncType {
  NONE = 0,
  RECEIVER, ///< 被同步方，只能被SENDER同步修改
  SENDER,   ///< 同步发起方，任何变化会被同步到RECEIVER
};
/**
 * @brief Plato数组同步操作类型
 */
enum class PlatoArrayOp : ArrayOpType {
  ADD = 1, ///< 添加
  REMOVE,  ///< 移除
  ALL,     ///< 全量同步
};
/**
 * @brief Plato表同步操作类型
 */
enum class PlatoMapOp : MapOpType {
  ADD = 1, ///< 添加
  REMOVE,  ///< 删除
  ALL,     ///< 全量同步
};
/**
 * @brief Plato集合操作类型
 */
enum class PlatoSetOp : SetOpType {
  ADD = 1, ///< 添加
  REMOVE,  ///< 删除
  ALL,     ///< 全量同步
};

class Domain;
class DomainImpl;
class PlatoStream;
class Variable;
class VariableImpl;
class MemBlock;

using VariablePtr = std::shared_ptr<Variable>;
using DomainPtr = std::shared_ptr<Domain>;

/**
 * @brief 变量域，负责变量的声明周期管理，同步
 */
class Domain {
public:
  virtual ~Domain() {}
  /**
   * @brief 将变量加入到域，并自动产生变量ID
   * @param ptr 变量指针
   * @return true成功, false失败
   */
  virtual auto identify(VariablePtr ptr) -> bool = 0;
  /**
   * @brief 从域内移除变量
   * @param id 变量ID
   * @return
   */
  virtual auto remove(VarID id) -> void = 0;
  /**
   * @brief 从域内获取变量
   * @param id 变量ID
   * @return 变量指针
   */
  virtual auto get(VarID id) -> VariablePtr = 0;
  /**
   * @brief 从域内获取变量
   * @param name 变量名
   * @return 变量指针
   */
  virtual auto get(const std::string &name) -> VariablePtr = 0;
  /**
   * @brief 获取数据流
   * @return 数据流
   */
  virtual auto get_stream() -> PlatoStream & = 0;
  /**
   * @brief 将数据流内数据同步到域内
   * @return
   */
  virtual auto do_sync() -> void = 0;
  /**
   * @brief 将域内所有变量进行一次全量同步并写入域的数据流
   * @return
   */
  virtual auto serialize_all() -> void = 0;
  /**
   * @brief 域ID，用于同步，同步双方域ID必须相同
   * @return 域ID
   */
  virtual auto id() -> DomainID = 0;
  /**
   * @brief 获取内存块
   * @return 内存块指针
   */
  virtual auto mem_block() -> MemBlock * = 0;
  /**
   * @brief 重置命名变量
   * @return
   */
  virtual auto reset_named_var() -> void = 0;
  /**
   * @brief 设置保留的变量ID最大值
   * @param var_id 保留的变量ID最大值
   * @return 
  */
  virtual auto set_reserved_max_id(VarID var_id) -> void = 0;
  /**
   * @brief 建立一个domain内的顶层变量
   * @tparam T 变量类型
   * @tparam ...ARGS 参数类型
   * @param sync_type 同步类型
   * @param ...args 参数
   * @return 变量智能指针
   */
  template <typename T, typename... ARGS>
  auto New(PlatoVariableSyncType sync_type = PlatoVariableSyncType::NONE,
           ARGS... args) -> std::shared_ptr<T> {
    auto ptr = plato::make_shared<T>(mem_block(), this, 0, sync_type,
                                     std::forward<ARGS>(args)...);
    identify(ptr);
    return ptr;
  }
  /**
   * @brief 建立一个domain内的顶层变量
   * @tparam T 变量类型
   * @tparam ...ARGS 参数类型
   * @param sync_type 同步类型
   * @param ...args 参数
   * @return 变量智能指针
   */
  template <typename T, typename... ARGS>
  auto
  NewNoIdentified(PlatoVariableSyncType sync_type = PlatoVariableSyncType::NONE,
                  ARGS... args) -> std::shared_ptr<T> {
    return plato::make_shared<T>(mem_block(), this, 0, sync_type,
                                 std::forward<ARGS>(args)...);
  }
  /**
   * @brief 建立一个domain顶层变量
   * @param type 类型
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  auto NewNoIdentifiedBasic(PlatoType type, PlatoVariableSyncType sync_type)
      -> VariablePtr;
  /**
   * @brief 建立一个domain顶层Set变量
   * @param type 类型
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  auto NewNoIdentifiedSet(PlatoType key_type, PlatoVariableSyncType sync_type)
      -> VariablePtr;
  /**
   * @brief 建立一个domain顶层数组变量
   * @param type 类型
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  auto NewNoIdentifiedArray(PlatoType key_type, PlatoVariableSyncType sync_type)
      -> VariablePtr;
  /**
   * @brief 建立一个domain顶层Array变量，元素类型为struct
   * @param struct_name struct名字
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  auto NewNoIdentifiedArrayStruct(const char *struct_name,
                                  PlatoVariableSyncType sync_type)
      -> VariablePtr;
  /**
   * @brief 建立一个domain顶层Map变量
   * @param key_type key类型
   * @param value_type value类型
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  auto NewNoIdentifiedMap(PlatoType key_type, PlatoType value_type,
                          PlatoVariableSyncType sync_type) -> VariablePtr;
  /**
   * @brief 建立一个domain顶层Map变量, value类型为struct
   * @param key_type key类型
   * @param struct_name struct名字
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  auto NewNoIdentifiedMapStruct(PlatoType key_type, const char *struct_name,
                                PlatoVariableSyncType sync_type) -> VariablePtr;
};
/**
 * @brief 变量
 */
class Variable {
public:
  virtual ~Variable() {}
  /**
   * @brief 获取变量类型
   * @return 变量类型
   */
  virtual auto type() const -> PlatoType = 0;
  /**
   * @brief 获取变量名
   * @return 变量名
   */
  virtual auto name() -> const std::string & = 0;
  /**
   * @brief 获取父变量
   * @return 父变量
   */
  virtual auto parent() const -> VariablePtr = 0;
  /**
   * @brief 获取域指针
   * @return 域指针
   */
  virtual auto domain() const -> Domain * = 0;
  /**
   * @brief 获取同步类型
   * @return 同步类型
   */
  virtual auto sync_type() const -> PlatoVariableSyncType = 0;
  /**
   * @brief 获取变量ID
   * @return 变量ID
   */
  virtual auto id() const -> VarID = 0;
  /**
   * @brief 进行一次全量序列化
   * @param stream 数据流
   * @return true成功, false失败
   */
  virtual auto serialize(PlatoStream &stream) -> bool = 0;
  /**
   * @brief 反序列化
   * @param stream 数据流
   * @return true成功, false失败
   */
  virtual auto deserialize(PlatoStream &stream) -> bool = 0;
  /**
   * @brief 深拷贝变量内容
   * @param other 另一个同类型变量
   * @return
   */
  virtual auto copy(Variable *other) -> void = 0;
  /**
   * @brief 变量class占用内存量
   * @return class占用内存量
   */
  virtual auto object_size() -> std::size_t = 0;
  /**
   * @brief 拷贝默认值
   * @return
   */
  virtual auto copy_default() -> void = 0;
  /**
   * @brief 完成默认值设置后调用
   * @return
   */
  virtual auto complete_prototype() -> void = 0;
};
/**
 * @brief 变量实现类
 */
class VariableImpl : public Variable {
private:
  Domain *domain_{nullptr};         ///< 域指针
  VarID parent_{INVALID_VAR_ID};    ///< 父变量ID
  PlatoType type_{PlatoType::NONE}; ///< 变量类型
  PlatoVariableSyncType sync_type_{
      PlatoVariableSyncType::NONE}; ///< 变量同步类型
  VarID var_id_{INVALID_VAR_ID};    ///< 变量ID
  std::string name_;                ///< 变量名

public:
  /**
   * @brief 构造
   * @param domain 域指针
   * @param type 变量类型
   * @param parent 父变量ID
   * @param sync_type 变量同步类型
   */
  VariableImpl(Domain *domain, PlatoType type, VarID parent,
               PlatoVariableSyncType sync_type);
  /**
   * @brief 构造
   * @param name 变量名
   * @param domain 域指针
   * @param type 变量类型
   * @param parent 父变量ID
   * @param sync_type 变量同步类型
   */
  VariableImpl(const std::string &name, Domain *domain, PlatoType type,
               VarID parent, PlatoVariableSyncType sync_type);
  virtual ~VariableImpl();
  virtual auto type() const -> PlatoType override;
  virtual auto name() -> const std::string & override;
  virtual auto parent() const -> VariablePtr override;
  virtual auto domain() const -> Domain * override;
  virtual auto sync_type() const -> PlatoVariableSyncType override;
  virtual auto id() const -> VarID override;
  /**
   * @brief 设置变量ID
   * @param id 变量ID
   * @return
   */
  auto set_id(VarID id) -> void;

  friend class DomainImpl;
};

template <typename T> struct DefaultValue { constexpr static T value{0}; };
template <> struct DefaultValue<bool> { constexpr static bool value = false; };
template <> struct DefaultValue<float> { constexpr static float value = .0f; };
template <> struct DefaultValue<double> { constexpr static bool value = .0; };
template <> struct DefaultValue<std::string> {
  constexpr static const char *value = "";
};

template <typename T> struct TypeValue {
  constexpr static PlatoType value = PlatoType::NONE;
};

template <> struct TypeValue<bool> {
  constexpr static PlatoType value = PlatoType::BOOL;
};

template <> struct TypeValue<std::int8_t> {
  constexpr static PlatoType value = PlatoType::INT8;
};

template <> struct TypeValue<std::uint8_t> {
  constexpr static PlatoType value = PlatoType::UINT8;
};

template <> struct TypeValue<std::int16_t> {
  constexpr static PlatoType value = PlatoType::INT16;
};

template <> struct TypeValue<std::uint16_t> {
  constexpr static PlatoType value = PlatoType::UINT16;
};

template <> struct TypeValue<std::int32_t> {
  constexpr static PlatoType value = PlatoType::INT32;
};

template <> struct TypeValue<std::uint32_t> {
  constexpr static PlatoType value = PlatoType::UINT32;
};

template <> struct TypeValue<std::int64_t> {
  constexpr static PlatoType value = PlatoType::INT64;
};

template <> struct TypeValue<std::uint64_t> {
  constexpr static PlatoType value = PlatoType::UINT64;
};

template <> struct TypeValue<float> {
  constexpr static PlatoType value = PlatoType::FLOAT;
};

template <> struct TypeValue<double> {
  constexpr static PlatoType value = PlatoType::DOUBLE;
};

template <> struct TypeValue<std::string> {
  constexpr static PlatoType value = PlatoType::STRING;
};
/**
 * @brief 整数变量
 * @tparam T 整数类型
 */
template <typename T> class NumericVariable : public VariableImpl {
  NumericVariable() = delete;
  NumericVariable(const NumericVariable &) = delete;
  NumericVariable(NumericVariable &&) = delete;
  auto operator=(const NumericVariable &) = delete;

public:
  /**
   * @brief 构造
   * @param domain 域
   * @param parent 父变量ID
   * @param sync_type 同步类型
   */
  NumericVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
      : VariableImpl(domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_integral<T>::value, "Not a integral");
  }
  /**
   * @brief 构造
   * @param name 变量名
   * @param domain 域
   * @param parent 父变量ID
   * @param sync_type 同步类型
   */
  NumericVariable(const std::string &name, Domain *domain, VarID parent,
                  PlatoVariableSyncType sync_type)
      : VariableImpl(name, domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_integral<T>::value, "Not a integral");
  }
  /**
   * @brief 构造
   * @param domain 域
   * @param parent 父变量ID
   * @param sync_type 同步类型
   * @param t 变量默认值
   */
  NumericVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type,
                  const T t)
      : VariableImpl(domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_integral<T>::value, "Not a integral");
    value_ = t;
    value_default_ = t;
  }
  /**
   * @brief 构造
   * @param name 变量名
   * @param domain 域
   * @param parent 父变量ID
   * @param sync_type 同步类型
   * @param t 变量默认值
   */
  NumericVariable(const std::string &name, Domain *domain, VarID parent,
                  PlatoVariableSyncType sync_type, const T t)
      : VariableImpl(name, domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_integral<T>::value, "Not a integral");
    value_ = t;
    value_default_ = t;
  }

protected:
  T value_{DefaultValue<T>::value};         ///< 整数值
  T value_default_{DefaultValue<T>::value}; ///< 整数值默认值

public:
  virtual ~NumericVariable() {}
  /**
   * @brief 建立一个NumericVariable<T>实例
   * @param domain 域
   * @param parent 父变量ID
   * @param sync_type 同步类型
   * @return NumericVariable<T>实例
   */
  static auto New(Domain *domain, VarID parent,
                  PlatoVariableSyncType sync_type) {
    return plato::make_shared<NumericVariable<T>>(domain->mem_block(), domain,
                                                  parent, sync_type);
  }
  virtual auto serialize(PlatoStream &stream) -> bool override {
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      stream << id();
      stream << value_;
      return true;
    }
    return false;
  }
  virtual auto deserialize(PlatoStream &stream) -> bool override {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      VarID var_id;
      stream >> var_id;
      set_id(var_id);
      stream >> value_;
      return true;
    }
    return false;
  }
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<NumericVariable<T> *>(other);
    if (!ptr) {
      return;
    }
    value_ = ptr->value_;
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto object_size() -> std::size_t override {
    return sizeof(NumericVariable<T>);
  }
  virtual auto copy_default() -> void override { value_ = value_default_; }
  virtual auto complete_prototype() -> void override {}
  auto set_default(const T t) { value_default_ = t; }
  operator T() { return value_; }
  operator T() const { return value_; }
  auto operator=(const T t) -> NumericVariable<T> & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ = t;
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator==(const T t) const -> bool { return (value_ == t); }
  auto operator+(const T t) const -> T { return (value_ + t); }
  auto operator-(const T t) const -> T { return (value_ - t); }
  auto operator*(const T t) const -> T { return (value_ * t); }
  auto operator/(const T t) const -> T {
    if (!t) {
      throw std::runtime_error("Divide zero");
    }
    return (value_ / t);
  }
  auto operator%(const T t) const -> T {
    if (!t) {
      throw std::runtime_error("Mod zero");
    }
    return (value_ % t);
  }
  auto operator&(const T t) const -> T { return (value_ & t); }
  auto operator|(const T t) const -> T { return (value_ | t); }
  auto operator^(const T t) const -> T { return (value_ ^ t); }
  auto operator~() -> T { return ~value_; }
  auto operator>(const T t) const -> bool { return (value_ > t); }
  auto operator>=(const T t) const -> bool { return (value_ >= t); }
  auto operator<(const T t) const -> bool { return (value_ < t); }
  auto operator<=(const T t) const -> bool { return (value_ <= t); }
  auto operator!=(const T t) const -> bool { return (value_ != t); }
  auto operator++(int) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    auto old_value = value_;
    value_ += T(1);
    serialize(domain()->get_stream());
    return old_value;
  }
  auto operator++() -> NumericVariable<T> & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ += T(1);
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator--(int) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    auto old_value = value_;
    value_ -= T(1);
    serialize(domain()->get_stream());
    return old_value;
  }
  auto operator--() -> NumericVariable<T> & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ -= T(1);
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator>>(int bits) -> T { return (value_ >> bits); }
  auto operator<<(int bits) -> T { return (value_ << bits); }

  auto operator+=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ += t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator-=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ -= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator*=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ *= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator/=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    if (!t) {
      throw std::runtime_error("Divide zero");
    }
    value_ /= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator%=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    if (!t) {
      throw std::runtime_error("Divide zero");
    }
    value_ %= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator>>=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ >>= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator<<=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ <<= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator&=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ &= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator^=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ ^= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator|=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ |= t;
    serialize(domain()->get_stream());
    return value_;
  }
};

template <typename T>
auto inline operator+(const NumericVariable<T> &lht,
                      const NumericVariable<T> &rht)
    -> std::shared_ptr<NumericVariable<T>> {
  T res = (T)lht + (T)rht;
  return lht.domain()->New<NumericVariable<T>>(PlatoVariableSyncType::NONE,
                                               res);
}

template <typename T>
auto inline operator-(const NumericVariable<T> &lht,
                      const NumericVariable<T> &rht)
    -> std::shared_ptr<NumericVariable<T>> {
  T res = (T)lht - (T)rht;
  return lht.domain()->New<NumericVariable<T>>(PlatoVariableSyncType::NONE,
                                               res);
}

template <typename T>
auto inline operator*(const NumericVariable<T> &lht,
                      const NumericVariable<T> &rht)
    -> std::shared_ptr<NumericVariable<T>> {
  T res = (T)lht * (T)rht;
  return lht.domain()->New<NumericVariable<T>>(PlatoVariableSyncType::NONE,
                                               res);
}

template <typename T>
auto inline operator/(const NumericVariable<T> &lht,
                      const NumericVariable<T> &rht)
    -> std::shared_ptr<NumericVariable<T>> {
  T res = (T)lht / (T)rht;
  return lht.domain()->New<NumericVariable<T>>(PlatoVariableSyncType::NONE,
                                               res);
}

template <typename T>
auto inline operator%(const NumericVariable<T> &lht,
                      const NumericVariable<T> &rht)
    -> std::shared_ptr<NumericVariable<T>> {
  T res = (T)lht % (T)rht;
  return lht.domain()->New<NumericVariable<T>>(PlatoVariableSyncType::NONE,
                                               res);
}
/**
 * @brief bool类型变量
 */
class BoolVariable : public VariableImpl {
  BoolVariable() = delete;
  BoolVariable(const BoolVariable &) = delete;
  BoolVariable(BoolVariable &&) = delete;

protected:
  bool value_{false};
  bool value_default_{false};

public:
  BoolVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type);
  BoolVariable(const std::string &name, Domain *domain, VarID parent,
               PlatoVariableSyncType sync_type);
  BoolVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type,
               bool t);
  BoolVariable(const std::string &name, Domain *domain, VarID parent,
               PlatoVariableSyncType sync_type, bool t);
  virtual ~BoolVariable();
  static auto New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
      -> std::shared_ptr<BoolVariable>;
  virtual auto serialize(PlatoStream &stream) -> bool override;
  virtual auto deserialize(PlatoStream &stream) -> bool override;
  virtual auto complete_prototype() -> void override {}
  auto set_default(const bool t) { value_default_ = t; }
  operator bool() { return value_; }
  operator bool() const { return value_; }
  auto operator=(const bool t) -> BoolVariable & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ = t;
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator==(const bool t) const -> bool { return (t == value_); }
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<BoolVariable *>(other);
    if (!ptr) {
      return;
    }
    value_ = ptr->value_;
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto object_size() -> std::size_t override {
    return sizeof(BoolVariable);
  }
  virtual auto copy_default() -> void override { value_ = value_default_; }
};
/**
 * @brief 浮点数类型
 * @tparam T float或double
 */
template <typename T> class FloatingVariable : public VariableImpl {
  FloatingVariable() = delete;
  FloatingVariable(const FloatingVariable &) = delete;
  FloatingVariable(FloatingVariable &&) = delete;

protected:
  T value_{DefaultValue<T>::value};
  T value_default_{DefaultValue<T>::value};

public:
  FloatingVariable(Domain *domain, VarID parent,
                   PlatoVariableSyncType sync_type)
      : VariableImpl(domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_floating_point<T>::value, "Not a floating type");
  }
  FloatingVariable(const std::string &name, Domain *domain, VarID parent,
                   PlatoVariableSyncType sync_type)
      : VariableImpl(name, domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_floating_point<T>::value, "Not a floating type");
  }
  FloatingVariable(Domain *domain, VarID parent,
                   PlatoVariableSyncType sync_type, T t)
      : VariableImpl(domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_floating_point<T>::value, "Not a floating type");
    value_ = t;
    value_default_ = t;
  }
  FloatingVariable(const std::string &name, Domain *domain, VarID parent,
                   PlatoVariableSyncType sync_type, T t)
      : VariableImpl(name, domain, TypeValue<T>::value, parent, sync_type) {
    static_assert(std::is_floating_point<T>::value, "Not a floating type");
    value_ = t;
    value_default_ = t;
  }
  virtual ~FloatingVariable() {}
  static auto New(Domain *domain, VarID parent,
                  PlatoVariableSyncType sync_type) {
    return plato::make_shared<FloatingVariable<T>>(domain->mem_block(), domain,
                                                   parent, sync_type);
  }
  virtual auto serialize(PlatoStream &stream) -> bool override {
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      stream << id();
      stream << value_;
      return true;
    }
    return false;
  }
  virtual auto deserialize(PlatoStream &stream) -> bool override {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      VarID var_id;
      stream >> var_id;
      set_id(var_id);
      stream >> value_;
      return true;
    }
    return false;
  }
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<FloatingVariable<T> *>(other);
    if (!ptr) {
      return;
    }
    value_ = ptr->value_;
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto object_size() -> std::size_t override {
    return sizeof(FloatingVariable<T>);
  }
  virtual auto copy_default() -> void override { value_ = value_default_; }
  virtual auto complete_prototype() -> void override {}
  auto set_default(const T t) { value_default_ = t; }
  operator T() { return value_; }
  operator T() const { return value_; }
  auto operator=(const T t) -> FloatingVariable<T> & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ = t;
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator==(const T t) const -> bool {
    constexpr T EPSILON = T(0.000001f);
    auto gap = value_ - t;
    if (gap > -EPSILON && gap < EPSILON) {
      return true;
    }
    return false;
  }
  auto operator>(const T t) const -> bool { return (value_ > t); }
  auto operator>=(const T t) const -> bool { return (value_ >= t); }
  auto operator<(const T t) const -> bool { return (value_ < t); }
  auto operator<=(const T t) const -> bool { return (value_ <= t); }
  auto operator!=(const T t) const -> bool { return !(*this == t); }
  auto operator++() -> FloatingVariable<T> & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ += T(1.0f);
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator++(int) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    auto temp = value_;
    value_ += T(1.0f);
    serialize(domain()->get_stream());
    return temp;
  }
  auto operator--() -> FloatingVariable<T> & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    value_ -= T(1.0f);
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator--(int) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    auto temp = value_;
    value_ -= T(1.0f);
    serialize(domain()->get_stream());
    return temp;
  }
  auto operator+(const T t) const -> T { return (value_ + t); }
  auto operator-(const T t) const -> T { return (value_ - t); }
  auto operator*(const T t) const -> T { return (value_ * t); }
  auto operator/(const T t) const -> T {
    if (!t) {
      throw std::runtime_error("Divide zero");
    }
    return (value_ / t);
  }
  auto operator%(const T t) const -> T {
    if (!t) {
      throw std::runtime_error("Mod zero");
    }
    return std::fmod(value_, t);
  }
  auto operator%=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    if (!t) {
      throw std::runtime_error("Divide zero");
    }
    value_ = std::fmod(value_, t);
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator+=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ += t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator-=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ -= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator*=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    value_ *= t;
    serialize(domain()->get_stream());
    return value_;
  }
  auto operator/=(const T t) -> T {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_;
    }
    if (!t) {
      throw std::runtime_error("Divide zero");
    }
    value_ /= t;
    serialize(domain()->get_stream());
    return value_;
  }
};

template <typename T>
auto inline operator+(const FloatingVariable<T> &lht,
                      const FloatingVariable<T> &rht)
    -> std::shared_ptr<FloatingVariable<T>> {
  T res = (T)lht + (T)rht;
  return lht.domain()->New<FloatingVariable<T>>(PlatoVariableSyncType::NONE,
                                                res);
}

template <typename T>
auto inline operator-(const FloatingVariable<T> &lht,
                      const FloatingVariable<T> &rht)
    -> std::shared_ptr<FloatingVariable<T>> {
  T res = (T)lht - (T)rht;
  return lht.domain()->New<FloatingVariable<T>>(PlatoVariableSyncType::NONE,
                                                res);
}

template <typename T>
auto inline operator*(const FloatingVariable<T> &lht,
                      const FloatingVariable<T> &rht)
    -> std::shared_ptr<FloatingVariable<T>> {
  T res = (T)lht * (T)rht;
  return lht.domain()->New<FloatingVariable<T>>(PlatoVariableSyncType::NONE,
                                                res);
}

template <typename T>
auto inline operator/(const FloatingVariable<T> &lht,
                      const FloatingVariable<T> &rht)
    -> std::shared_ptr<FloatingVariable<T>> {
  T res = (T)lht / (T)rht;
  return lht.domain()->New<FloatingVariable<T>>(PlatoVariableSyncType::NONE,
                                                res);
}

template <typename T>
auto inline operator%(const FloatingVariable<T> &lht,
                      const FloatingVariable<T> &rht)
    -> std::shared_ptr<FloatingVariable<T>> {
  T res = (T)(lht % (T)rht);
  return lht.domain()->New<FloatingVariable<T>>(PlatoVariableSyncType::NONE,
                                                res);
}

extern char *get_buffer(std::size_t size);
extern char *get_buffer();

/**
 * @brief 字符串类型
 */
class StringVariable : public VariableImpl {
  StringVariable() = delete;
  StringVariable(const StringVariable &) = delete;
  StringVariable(StringVariable &&) = delete;

  std::string s_;
  std::string s_default_;

public:
  StringVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type);
  StringVariable(const std::string &name, Domain *domain, VarID parent,
                 PlatoVariableSyncType sync_type);
  StringVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type,
                 const std::string &t);
  StringVariable(const std::string &name, Domain *domain, VarID parent,
                 PlatoVariableSyncType sync_type, const std::string &t);
  virtual ~StringVariable();
  static auto New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
      -> std::shared_ptr<StringVariable>;
  virtual auto serialize(PlatoStream &stream) -> bool override;
  virtual auto deserialize(PlatoStream &stream) -> bool override;
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<StringVariable *>(other);
    if (!ptr) {
      return;
    }
    s_ = ptr->s_;
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto object_size() -> std::size_t override {
    return sizeof(StringVariable);
  }
  virtual auto copy_default() -> void override { s_ = s_default_; }
  virtual auto complete_prototype() -> void override {}
  auto set_default(const std::string &s) { s_default_ = s; }
  operator std::string();
  auto operator=(const std::string &s) -> StringVariable & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    s_ = s;
    serialize(domain()->get_stream());
    return *this;
  }
  auto operator==(const std::string &s) -> bool { return (s_ == s); }
  auto operator+=(const std::string &s) -> StringVariable & {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return *this;
    }
    s_ += s;
    serialize(domain()->get_stream());
    return *this;
  }
};
/**
 * @brief 数组类型
 * @tparam T 必须继承于VariableImpl
 */
template <typename T> class ArrayVariable : public VariableImpl {
  ArrayVariable() = delete;
  ArrayVariable(const ArrayVariable &) = delete;
  ArrayVariable(ArrayVariable &&) = delete;
  using TPtr = std::shared_ptr<T>;

  using ValueArray = std::vector<TPtr>;
  ValueArray value_array_;
  ValueArray value_array_default_;

public:
  using iterator = typename ValueArray::iterator;
  using const_iterator = typename ValueArray::const_iterator;
  using reverse_iterator = typename ValueArray::reverse_iterator;
  using const_reverse_iterator = typename ValueArray::const_reverse_iterator;

public:
  ArrayVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
      : VariableImpl(domain, PlatoType::ARRAY, parent, sync_type) {
    static_assert(std::is_base_of<VariableImpl, T>::value,
                  "Not a plato variable type");
  }
  ArrayVariable(const std::string &name, Domain *domain, VarID parent,
                PlatoVariableSyncType sync_type)
      : VariableImpl(name, domain, PlatoType::ARRAY, parent, sync_type) {
    static_assert(std::is_base_of<VariableImpl, T>::value,
                  "Not a plato variable type");
  }
  virtual ~ArrayVariable() {}
  virtual auto serialize(PlatoStream &stream) -> bool override {
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return false;
    }
    stream << id();
    stream << (ArrayOpType)PlatoArrayOp::ALL;
    stream << (std::uint32_t)value_array_.size();
    std::size_t index = 0;
    for (auto &value : value_array_) {
      stream << index;
      value->serialize(stream);
      index += 1;
    }
    return true;
  }
  virtual auto deserialize(PlatoStream &stream) -> bool override {
    if (sync_type() != PlatoVariableSyncType::RECEIVER) {
      return false;
    }
    stream.skip(sizeof(VarID));
    ArrayOpType op;
    stream >> op;
    switch (PlatoArrayOp(op)) {
    case PlatoArrayOp::ALL:
      deserialize_all(stream);
      break;
    case PlatoArrayOp::ADD:
      deserialize_add(stream);
      break;
    case PlatoArrayOp::REMOVE:
      deserialize_remove(stream);
      break;
    default:
      return false;
    }
    return true;
  }
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<ArrayVariable<T> *>(other);
    if (!ptr) {
      return;
    }
    for (auto &v : ptr->value_array_) {
      auto sptr = T::New(domain(), id(), sync_type());
      domain()->identify(sptr);
      sptr->copy(v.get());
      value_array_.push_back(sptr);
    }
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto object_size() -> std::size_t override {
    return sizeof(ArrayVariable<T>);
  }
  virtual auto copy_default() -> void override {
    value_array_.clear();
    for (auto &var : value_array_default_) {
      var->copy_default();
    }
    value_array_ = value_array_default_;
  }
  virtual auto complete_prototype() -> void override { copy_default(); }
  auto static New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
      -> std::shared_ptr<ArrayVariable<T>> {
    return plato::make_shared<ArrayVariable<T>>(domain->mem_block(), domain,
                                                parent, sync_type);
  }

public:
  auto add_default() -> TPtr {
    auto sptr = T::New(domain(), id(), PlatoVariableSyncType::NONE);
    domain()->identify(sptr);
    value_array_default_.push_back(sptr);
    return sptr;
  }
  auto add() -> TPtr {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return nullptr;
    }
    auto sptr = T::New(domain(), id(), sync_type());
    domain()->identify(sptr);
    value_array_.push_back(sptr);
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return sptr;
    }
    auto &stream = domain()->get_stream();
    stream << id();
    stream << (ArrayOpType)PlatoArrayOp::ADD;
    sptr->serialize(stream);
    return sptr;
  }

  auto remove(std::size_t index) -> void {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return;
    }
    if (index >= value_array_.size()) {
      return;
    }
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      auto &stream = domain()->get_stream();
      stream << id();
      stream << (ArrayOpType)PlatoArrayOp::REMOVE;
      stream << index;
    }
    value_array_.erase(value_array_.begin() + index);
  }

  auto operator[](std::size_t index) -> TPtr {
    if (index >= value_array_.size()) {
      return nullptr;
    }
    return value_array_[index];
  }

  auto at(std::size_t index) -> TPtr {
    if (index >= value_array_.size()) {
      throw std::runtime_error("Array out of range");
    }
    return value_array_[index];
  }

  auto size() -> std::size_t { return value_array_.size(); }

  iterator erase(const_iterator it) {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return value_array_.end();
    }
    if (it == value_array_.end()) {
      return value_array_.end();
    }
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      auto &stream = domain()->get_stream();
      stream << id();
      stream << (ArrayOpType)PlatoArrayOp::REMOVE;
      stream << (std::size_t)(it - value_array_.begin());
      (*it)->serialize(stream);
    }
    return value_array_.erase(it);
  }

  iterator begin() noexcept { return value_array_.begin(); }

  const_iterator begin() const noexcept {
    return ValueArray::const_iterator(value_array_.begin());
  }

  iterator end() noexcept { return value_array_.end(); }

  const_iterator end() const noexcept {
    return ValueArray::const_iterator(value_array_.end());
  }

  reverse_iterator rbegin() noexcept { return value_array_.rbegin(); }

  const_reverse_iterator rbegin() const noexcept {
    return value_array_.rbegin();
  }

  reverse_iterator rend() noexcept { return value_array_.rend(); }

  const_reverse_iterator rend() const noexcept {
    return const_reverse_iterator(begin());
  }

  const_iterator cbegin() const noexcept { return value_array_.cbegin(); }

  const_iterator cend() const noexcept { return value_array_.cend(); }

  const_reverse_iterator crbegin() const noexcept {
    return value_array_.crbegin();
  }

  const_reverse_iterator crend() const noexcept { return value_array_.crend(); }

private:
  auto deserialize_remove(PlatoStream &stream) -> void {
    std::size_t index;
    stream >> index;
    value_array_.erase(value_array_.begin() + index);
  }
  auto deserialize_add(PlatoStream &stream) -> void {
    auto sptr = T::New(domain(), id(), sync_type());
    value_array_.emplace_back(sptr);
    sptr->deserialize(stream);
    domain()->identify(sptr);
  }
  auto deserialize_all(PlatoStream &stream) -> void {
    std::uint32_t count = 0;
    stream >> count;
    value_array_.clear();
    for (std::uint32_t i = 0; i < count; i++) {
      auto sptr = T::New(domain(), id(), sync_type());
      sptr->deserialize(stream);
      value_array_.emplace_back(sptr);
      domain()->identify(sptr);
    }
  }
};
/**
 * @brief 结构体
 */
class StructVariable : public VariableImpl {
  StructVariable() = delete;
  StructVariable(const StructVariable &) = delete;
  StructVariable(StructVariable &&) = delete;

public:
  StructVariable(Domain *domain, VarID parent, PlatoVariableSyncType sync_type);
  StructVariable(const std::string &name, Domain *domain, VarID parent,
                 PlatoVariableSyncType sync_type);
  virtual ~StructVariable();
};
/**
 * @brief 字典类型
 * @tparam KeyT 字典key类型
 * @tparam ValueT 必须继承于VariableImpl
 */
template <typename KeyT, typename ValueT>
class MapVariable : public VariableImpl {
  MapVariable() = delete;
  MapVariable(const MapVariable &) = delete;
  MapVariable(MapVariable &&) = delete;
  using VarPtr = std::shared_ptr<ValueT>;
  using VarMap = std::unordered_map<KeyT, VarPtr>;

  VarMap var_map_;
  VarMap var_map_default_;

public:
  using iterator = typename VarMap::iterator;
  using const_iterator = typename VarMap::const_iterator;

public:
  MapVariable(Domain *domain, VarID /*parent*/, PlatoVariableSyncType sync_type)
      : VariableImpl(domain, PlatoType::MAP, id(), sync_type) {
    static_assert(std::is_base_of<VariableImpl, ValueT>::value,
                  "Value type is not a plato variable type");
  }
  MapVariable(const std::string &name, Domain *domain, VarID /*parent*/,
              PlatoVariableSyncType sync_type)
      : VariableImpl(name, domain, PlatoType::MAP, id(), sync_type) {
    static_assert(std::is_base_of<VariableImpl, ValueT>::value,
                  "Value type is not a plato variable type");
  }
  virtual ~MapVariable() {}
  virtual auto serialize(PlatoStream &stream) -> bool override {
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return false;
    }
    stream << id();
    stream << (MapOpType)PlatoMapOp::ALL;
    stream << (std::uint32_t)var_map_.size();
    for (auto &[k, v] : var_map_) {
      serialize_by_type(stream, k);
      v->serialize(stream);
    }
    return true;
  }
  virtual auto deserialize(PlatoStream &stream) -> bool override {
    if (sync_type() != PlatoVariableSyncType::RECEIVER) {
      return false;
    }
    VarID var_id = INVALID_VAR_ID;
    stream >> var_id;
    set_id(var_id);
    MapOpType op_type;
    stream >> op_type;
    switch (PlatoMapOp(op_type)) {
    case PlatoMapOp::ALL:
      deserialize_all(stream);
      break;
    case PlatoMapOp::ADD:
      deserialize_add(stream);
      break;
    case PlatoMapOp::REMOVE:
      deserialize_remove(stream);
      break;
    default:
      return false;
    }
    return true;
  }
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<MapVariable<KeyT, ValueT> *>(other);
    if (!ptr) {
      return;
    }
    for (auto &[k, v] : ptr->var_map_) {
      auto sptr = ValueT::New(domain(), id(), sync_type());
      domain()->identify(sptr);
      var_map_.emplace(k, sptr);
      sptr->copy(v.get());
    }
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto copy_default() -> void override {
    var_map_.clear();
    for (auto &[_, v] : var_map_default_) {
      v->copy_default();
    }
    var_map_ = var_map_default_;
  }
  virtual auto complete_prototype() -> void override { copy_default(); }
  virtual auto object_size() -> std::size_t override {
    return sizeof(MapVariable<KeyT, ValueT>);
  }
  auto static New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
      -> std::shared_ptr<MapVariable<KeyT, ValueT>> {
    return plato::make_shared<MapVariable<KeyT, ValueT>>(
        domain->mem_block(), domain, parent, sync_type);
  }

public:
  auto add_default(KeyT key) -> VarPtr {
    auto ptr = ValueT::New(domain(), id(), PlatoVariableSyncType::NONE);
    domain()->identify(ptr);
    var_map_default_.emplace(key, ptr);
    return ptr;
  }
  auto add(KeyT key) -> VarPtr {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return nullptr;
    }
    auto ptr = ValueT::New(domain(), id(), sync_type());
    domain()->identify(ptr);
    var_map_.emplace(key, ptr);
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return ptr;
    }
    auto &stream = domain()->get_stream();
    stream << id();
    stream << (MapOpType)PlatoMapOp::ADD;
    serialize_by_type(stream, key);
    ptr->serialize(stream);
    return ptr;
  }

  auto remove(KeyT key) -> void {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return;
    }
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      auto &stream = domain()->get_stream();
      stream << id();
      stream << (MapOpType)PlatoMapOp::REMOVE;
      serialize_by_type(stream, key);
    }
    var_map_.erase(key);
  }

  auto get(KeyT key) -> VarPtr {
    auto it = var_map_.find(key);
    if (it == var_map_.end()) {
      return nullptr;
    }
    return it->second;
  }

  auto size() -> std::size_t { return var_map_.size(); }

  iterator erase(const_iterator it) {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return it;
    }
    if (it == var_map_.end()) {
      return it;
    }
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      auto &stream = domain()->get_stream();
      stream << id();
      stream << (MapOpType)PlatoMapOp::REMOVE;
      serialize_by_type(stream, it->key);
    }
    return var_map_.erase(it);
  }

  iterator begin() noexcept { return var_map_.begin(); }

  const_iterator begin() const noexcept {
    return VarMap::const_iterator(var_map_.begin());
  }

  iterator end() noexcept { return var_map_.end(); }

  const_iterator end() const noexcept {
    return VarMap::const_iterator(var_map_.end());
  }

private:
  auto deserialize_remove(PlatoStream &stream) -> void {
    KeyT key = DefaultValue<KeyT>::value;
    deserialize_by_type(stream, key);
    var_map_.erase(key);
  }

  auto deserialize_add(PlatoStream &stream) -> void {
    KeyT key;
    deserialize_by_type(stream, key);
    auto ptr = ValueT::New(domain(), id(), sync_type());
    ptr->deserialize(stream);
    var_map_.emplace(key, ptr);
    domain()->identify(ptr);
  }

  auto deserialize_all(PlatoStream &stream) -> void {
    std::uint32_t count = 0;
    stream >> count;
    var_map_.clear();
    for (std::uint32_t i = 0; i < count; i++) {
      KeyT key = DefaultValue<KeyT>::value;
      deserialize_by_type(stream, key);
      auto value_ptr = ValueT::New(domain(), id(), sync_type());
      value_ptr->deserialize(stream);
      domain()->identify(value_ptr);
      var_map_.emplace(key, value_ptr);
    }
  }

private:
  template <typename U>
  auto deserialize_by_type(PlatoStream &stream, U &u) -> void {
    stream >> u;
  }
  template <>
  auto deserialize_by_type<std::string>(PlatoStream &stream, std::string &u)
      -> void {
    std::uint32_t size = 0;
    stream >> size;
    stream.read(get_buffer(size), size);
    u.assign(get_buffer(), size);
  }
  template <typename U>
  auto serialize_by_type(PlatoStream &stream, const U &u) -> void {
    stream << u;
  }
  template <>
  auto serialize_by_type<std::string>(PlatoStream &stream, const std::string &u)
      -> void {
    stream << (std::uint32_t)u.size();
    stream.write(u.data(), u.size());
  }
};
/**
 * @brief 集合类型
 * @tparam KeyT 集合key
 */
template <typename KeyT> class SetVariable : public VariableImpl {
  SetVariable() = delete;
  SetVariable(const SetVariable &) = delete;
  SetVariable(SetVariable &&) = delete;
  using VarSet = std::unordered_set<KeyT>;

  VarSet var_set_;
  VarSet var_set_default_;

public:
  using iterator = typename VarSet::iterator;
  using const_iterator = typename VarSet::const_iterator;

public:
  SetVariable(Domain *domain, VarID /*parent*/, PlatoVariableSyncType sync_type)
      : VariableImpl(domain, PlatoType::MAP, id(), sync_type) {
    static_assert(std::is_integral<KeyT>::value ||
                      std::is_floating_point<KeyT>::value ||
                      std::is_string<KeyT>::value,
                  "Not a key type");
  }
  SetVariable(const std::string &name, Domain *domain, VarID /*parent*/,
              PlatoVariableSyncType sync_type)
      : VariableImpl(name, domain, PlatoType::MAP, id(), sync_type) {
    static_assert(std::is_integral<KeyT>::value ||
                      std::is_floating_point<KeyT>::value ||
                      std::is_string<KeyT>::value,
                  "Not a key type");
  }
  virtual ~SetVariable() {}
  virtual auto serialize(PlatoStream &stream) -> bool override {
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return false;
    }
    stream << id();
    stream << (SetOpType)PlatoSetOp::ALL;
    stream << (std::uint32_t)var_set_.size();
    for (auto &k : var_set_) {
      serialize_by_type(stream, k);
    }
    return true;
  }
  virtual auto deserialize(PlatoStream &stream) -> bool override {
    if (sync_type() != PlatoVariableSyncType::RECEIVER) {
      return false;
    }
    VarID var_id = INVALID_VAR_ID;
    stream >> var_id;
    set_id(var_id);
    SetOpType op_type;
    stream >> op_type;
    switch (PlatoSetOp(op_type)) {
    case PlatoSetOp::ALL:
      deserialize_all(stream);
      break;
    case PlatoSetOp::ADD:
      deserialize_add(stream);
      break;
    case PlatoSetOp::REMOVE:
      deserialize_remove(stream);
      break;
    default:
      return false;
    }
    return true;
  }
  virtual auto copy(Variable *other) -> void override {
    auto *ptr = dynamic_cast<SetVariable<KeyT> *>(other);
    if (!ptr) {
      return;
    }
    for (auto &k : ptr->var_set_) {
      var_set_.emplace(k);
    }
    // 这里不能屏蔽修改
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      serialize(domain()->get_stream());
    }
  }
  virtual auto copy_default() -> void override {
    var_set_.clear();
    var_set_ = var_set_default_;
  }
  virtual auto object_size() -> std::size_t override {
    return sizeof(SetVariable<KeyT>);
  }
  virtual auto complete_prototype() -> void override { copy_default(); }

public:
  auto add_default(KeyT key) -> void { var_set_default_.emplace(key); }
  auto add(KeyT key) -> void {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return;
    }
    var_set_.emplace(key);
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return;
    }
    auto &stream = domain()->get_stream();
    stream << id();
    stream << (SetOpType)PlatoSetOp::ADD;
    serialize_by_type(stream, key);
  }

  auto remove(KeyT key) -> void {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return;
    }
    var_set_.erase(key);
    if (sync_type() != PlatoVariableSyncType::SENDER) {
      return;
    }
    auto &stream = domain()->get_stream();
    stream << id();
    stream << (SetOpType)PlatoSetOp::REMOVE;
    serialize_by_type(stream, key);
  }

  auto has(KeyT key) -> bool {
    auto it = var_set_.find(key);
    if (it == var_set_.end()) {
      return false;
    }
    return true;
  }

  iterator erase(const_iterator it) {
    if (sync_type() == PlatoVariableSyncType::RECEIVER) {
      return it;
    }
    if (it == var_set_.end()) {
      return it;
    }
    if (sync_type() == PlatoVariableSyncType::SENDER) {
      auto &stream = domain()->get_stream();
      stream << id();
      stream << (SetOpType)PlatoSetOp::REMOVE;
      serialize_by_type(stream, *it);
    }
    return var_set_.erase(it);
  }

  auto size() -> std::size_t { return var_set_.size(); }

  iterator begin() noexcept { return var_set_.begin(); }

  const_iterator begin() const noexcept {
    return VarSet::const_iterator(var_set_.begin());
  }

  iterator end() noexcept { return var_set_.end(); }

  const_iterator end() const noexcept {
    return VarSet::const_iterator(var_set_.end());
  }

private:
  auto deserialize_remove(PlatoStream &stream) -> void {
    KeyT key = DefaultValue<KeyT>::value;
    deserialize_by_type(stream, key);
    var_set_.erase(key);
  }

  auto deserialize_add(PlatoStream &stream) -> void {
    KeyT key;
    deserialize_by_type(stream, key);
    var_set_.emplace(key);
  }

  auto deserialize_all(PlatoStream &stream) -> void {
    std::uint32_t count = 0;
    stream >> count;
    var_set_.clear();
    for (std::uint32_t i = 0; i < count; i++) {
      KeyT key = DefaultValue<KeyT>::value;
      deserialize_by_type(stream, key);
      var_set_.emplace(key);
    }
  }

private:
  template <typename U>
  auto deserialize_by_type(PlatoStream &stream, U &u) -> void {
    stream >> u;
  }
  template <>
  auto deserialize_by_type<std::string>(PlatoStream &stream, std::string &u)
      -> void {
    std::uint32_t size = 0;
    stream >> size;
    stream.read(get_buffer(size), size);
    u.assign(get_buffer(), size);
  }
  template <typename U>
  auto serialize_by_type(PlatoStream &stream, const U &u) -> void {
    stream << u;
  }
  template <>
  auto serialize_by_type<std::string>(PlatoStream &stream, const std::string &u)
      -> void {
    stream << (std::uint32_t)u.size();
    stream.write(u.data(), u.size());
  }
};

using Int = NumericVariable<int>;
using Char = NumericVariable<char>;
using Uchar = NumericVariable<unsigned char>;
using Int16 = NumericVariable<std::int16_t>;
using Int32 = NumericVariable<std::int32_t>;
using Int64 = NumericVariable<std::int64_t>;
using Uint16 = NumericVariable<std::uint16_t>;
using Uint32 = NumericVariable<std::uint32_t>;
using Uint64 = NumericVariable<std::uint64_t>;
using Float = FloatingVariable<float>;
using Double = FloatingVariable<double>;
using String = StringVariable;
using Bool = BoolVariable;
template <typename T> using Array = ArrayVariable<T>;
template <typename K, typename V> using Map = MapVariable<K, V>;
template <typename T> using Set = SetVariable<T>;

using IntPtr = std::shared_ptr<Int>;
using CharPtr = std::shared_ptr<Char>;
using UcharPtr = std::shared_ptr<Uchar>;
using Int16Ptr = std::shared_ptr<Int16>;
using Int32Ptr = std::shared_ptr<Int32>;
using Int64Ptr = std::shared_ptr<Int64>;
using Uint16Ptr = std::shared_ptr<Uint16>;
using Uint32Ptr = std::shared_ptr<Uint32>;
using Uint64Ptr = std::shared_ptr<Uint64>;
using FloatPtr = std::shared_ptr<Float>;
using DoublePtr = std::shared_ptr<Double>;
using StringPtr = std::shared_ptr<String>;
using BoolPtr = std::shared_ptr<Bool>;
template <typename T> using ArrayPtr = std::shared_ptr<Array<T>>;
template <typename K, typename V> using MapPtr = std::shared_ptr<Map<K, V>>;
template <typename T> using SetPtr = std::shared_ptr<Set<T>>;

/**
 * @brief 转换变量到类型std::shared_ptr<T>
 * @tparam T 变量类型
 * @param var_ptr 变量指针
 * @return std::shared_ptr<T>
 */
template <typename T> auto cast(VariablePtr var_ptr) -> std::shared_ptr<T> {
  return std::dynamic_pointer_cast<T>(var_ptr);
}

/**
 * @brief 建立一个变量域
 * @param id 域ID
 * @param mb 内存块
 * @return 域指针
 */
extern auto new_domain(DomainID id, MemBlock *mb = nullptr) -> DomainPtr;

} // namespace plato
