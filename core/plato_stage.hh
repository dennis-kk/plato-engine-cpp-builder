#pragma once

#include <cstdint>
#include <ctime>
#include <functional>
#include <memory>

#include "plato_node.hh"
#include "../include/plato.hh"

namespace plato {

class PlatoLogger;
using PlatoStageLifetimeStateID = std::uint32_t;

enum class PlatoStageLifetimeState : PlatoStageLifetimeStateID {
  BUILDING = 1,
  RUNNING,
  DESTROYING,
};

using PlatoFlowPtr = std::shared_ptr<PlatoFlow>;

/**
 * @brief 钩子函数事件
 */
enum class HookEvent {
  BEFORE_RUN = 1, ///< 运行节点逻辑前
  AFTER_RUN,      ///< 运行节点逻辑后
};

enum class HookAction {
  NONE = 0,
  OK = 1,
  PEND_BEFORE_RUN, ///< 调试挂起-节点运行前
};

using PlatoHook =
    std::function<HookAction(PlatoNode *, HookEvent, PlatoFlowStatus)>;

/**
 * @brief 流
 */
class PlatoFlow {
public:
  virtual ~PlatoFlow() {}
  /**
   * @brief 设置流其实节点
   * @param node_ptr 起始节点
   * @return
   */
  virtual auto set_start_node(PlatoNodePtr node_ptr) -> void = 0;
  /**
   * @brief 向流内添加节点
   * @param node_ptr 节点
   * @return true成功，false失败
   */
  virtual auto add_node(PlatoNodePtr node_ptr) -> bool = 0;
  /**
   * @brief 完成流连线并进行回溯节点设置
   * @return true成功，false失败
   */
  virtual auto complete() -> bool = 0;
  /**
   * @brief 获取节点实例
   * @param node_id 节点ID
   * @return 节点实例
   */
  virtual auto get_node(PlatoNodeID node_id) -> PlatoNodePtr = 0;
  /**
   * @brief 向流内添加连线
   * @param start 连线其实节点
   * @param end 连线结束节点
   * @return true成功，false失败
   */
  virtual auto add_link(const LinkNode &start, const LinkNode &end) -> bool = 0;
  /**
   * @brief 获取流的变量域
   * @return
   */
  virtual auto get_domain() -> DomainPtr = 0;
  /**
   * @brief 获取当前正在执行的节点
   * @return 节点
   */
  virtual auto get_current() -> PlatoNodePtr = 0;
  /**
   * @brief 主循环
   * @param hook 钩子函数
   * @param tick 当前时间戳(毫秒)
   * @param run_mode 运行模式
   * @return 流状态
   */
  virtual auto update(PlatoHook hook = nullptr, std::time_t tick = 0,
                      FlowRunMode run_mode = FlowRunMode::ACTIVE)
      -> PlatoFlowStatus = 0;
  /**
   * @brief 获取输出数据流
   * @return 数据流
   */
  virtual auto get_ostream() -> PlatoStream & = 0;
  /**
   * @brief 获取输入数据流
   * @return 数据流
   */
  virtual auto get_istream() -> PlatoStream & = 0;
  /**
   * @brief 获取流ID
   * @return 流ID
   */
  virtual auto id() -> PlatoFlowID = 0;
  /**
   * @brief 重置
   * @return
   */
  virtual auto reset() -> void = 0;
  /**
   * @brief 获取流当前状态
   * @return 流当前状态
   */
  virtual auto status() -> PlatoFlowStatus = 0;
  /**
   * @brief 停止流的执行并清理调用栈
   * @return
   */
  virtual auto halt() -> void = 0;
  /**
   * @brief 向流的当前节点发送一个异步事件
   * @param node_id 节点ID
   * @return
   */
  virtual auto dispatch_event(PlatoNodeID node_id) -> void = 0;
  /**
   * @brief 获取舞台
   * @return 舞台指针
   */
  virtual auto stage() -> PlatoStage * = 0;
};

using PlatoStagePtr = std::shared_ptr<PlatoStage>;

/**
 * @brief 舞台建造器
 */
class PlatoStageBuilder {
public:
  virtual ~PlatoStageBuilder() {}
  /**
   * @brief 预编译
   * @param config_file 配置文件
   * @return true或false
   */
  virtual auto pre_build(const std::string &config_file) -> bool = 0;
  /**
   * @brief 建造一个舞台实例
   * @param stage_id 舞台ID
   * @param config_file 配置文件路径
   * @return 舞台实例
   */
  virtual auto build(PlatoStageID stage_id, FlowRunMode run_mode)
      -> PlatoStagePtr = 0;
  /**
   * @brief 获取错误描述
   * @return 错误描述
   */
  virtual auto get_error() -> std::string = 0;
  /**
   * @brief 获取建造器对应的配置文件名
   * @return 建造器对应的配置文件名
   */
  virtual auto get_config_file() -> const std::string & = 0;
};

class PlatoStage {
public:
  virtual ~PlatoStage() {}
  /**
   * @brief 重置舞台
   * @return
   */
  virtual auto reset() -> void = 0;
  /**
   * @brief 获取舞台ID
   * @return 舞台ID
   */
  virtual auto id() -> PlatoStageID = 0;
  /**
   * @brief 获取变量域
   * @return 变量域
   */
  virtual auto domain() -> DomainPtr = 0;
  /**
   * @brief 添加一个新的流
   * @return 流指针
   */
  virtual auto add_flow() -> PlatoFlowPtr = 0;
  /**
   * @brief 运行一次主循环
   * @param tick 当前时间戳（毫秒）
   * @return
   */
  virtual auto update(std::time_t tick) -> void = 0;
  /**
   * @brief 获取输出数据流
   * @return 输出数据流
   */
  virtual auto get_flow_ostream() -> PlatoStream & = 0;
  /**
   * @brief 获取输入数据流
   * @return 输入数据流
   */
  virtual auto get_flow_istream() -> PlatoStream & = 0;
  /**
   * @brief 获取生命周期状态
   * @return 生命周期状态
   */
  virtual auto get_lifetime_state() -> PlatoStageLifetimeState = 0;
  /**
   * @brief 获取流数量
   * @return 流数量
   */
  virtual auto get_flow_count() -> std::size_t = 0;
  /**
   * @brief 获取流
   * @param index 流索引
   * @return 流实例
   */
  virtual auto get_flow(std::size_t index) -> PlatoFlowPtr = 0;
  /**
   * @brief 设置钩子函数
   * @param hook 钩子函数
   * @return 旧的钩子函数
   */
  virtual auto set_hook(PlatoHook hook) -> PlatoHook = 0;
  /**
   * @brief 设置日志
   * @param logger 日志
   * @return
   */
  virtual auto set_logger(PlatoLogger *logger) -> void = 0;
  /**
   * @brief 获取日志
   * @return 日志
   */
  virtual auto logger() -> PlatoLogger * = 0;
  /**
   * @brief 向流的当前节点发送一个异步事件
   * @param node_id 节点ID
   * @return
   */
  virtual auto dispatch_event(PlatoNodeID node_id) -> void = 0;
};

/**
 * @brief 建立流
 * @param flow_id 流ID
 * @param domain_ptr 变量域
 * @param stage 舞台
 * @return 流实例
 */
extern auto new_flow(PlatoFlowID flow_id, DomainPtr domain_ptr,
                     PlatoStage *stage = nullptr) -> PlatoFlowPtr;
/**
 * @brief 获取流实现类占用字节长度
 * @return 流实现类占用字节长度
 */
extern auto get_flow_size() -> std::size_t;

/**
 * @brief 预创建舞台，后续可以使用builder进行快速创建
 * @param builder 舞台建造器
 * @param config_file 配置文件路径
 * @return true或false
 */
extern auto pre_build_stage(PlatoStageBuilder *builder,
                            const std::string &config_file) -> bool;

/**
 * @brief 建立一个舞台
 * @param stage_id 舞台ID
 * @param builder 舞台建造器
 * @param config_file 配置文件路径
 * @param run_mode 运行模式
 * @return 舞台指针
 */
extern auto new_stage(PlatoStageID stage_id, PlatoStageBuilder *builder,
                      const std::string &config_file,
                      FlowRunMode run_mode = FlowRunMode::ACTIVE)
    -> PlatoStagePtr;

/**
 * @brief 建立一个舞台
 * @param id 舞台ID
 * @param mem_size 内部流占用的内存大小（字节）
 * @param run_mode 运行模式
 * @return 舞台指针
 */
extern auto new_stage(PlatoStageID id, std::size_t mem_size,
                      FlowRunMode run_mode = FlowRunMode::ACTIVE)
    -> PlatoStagePtr;

} // namespace plato
