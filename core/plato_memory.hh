#pragma once

#include <cstdlib>
#include <memory>

namespace plato {

using plato_malloc_func = void *(*)(std::size_t size);
using plato_free_func = void (*)(void *);

/**
 * @brief 设置内存分配函数
 * @param malloc_func malloc
 * @param free_func free
 * @return
 */
extern auto set_mem_allocator(plato_malloc_func malloc_func,
                              plato_free_func free_func) -> void;
/**
 * @brief 获取malloc函数指针
 * @return malloc函数指针
 */
extern auto get_malloc() -> plato_malloc_func;
/**
 * @brief 获取free函数指针
 * @return free函数指针
 */
extern auto get_free() -> plato_free_func;

/**
 * @brief 内存块
 */
class MemBlock {
public:
  virtual ~MemBlock() {}
  /**
   * @brief 重新设置内存块大小
   * @param size 内存块大小
   * @return
   */
  virtual auto reset(std::size_t size) -> void = 0;
  /**
   * @brief 获取一块内存
   * @param size 内存大小
   * @return 内存指针
   */
  virtual auto get(std::size_t size) -> void * = 0;
  /**
   * @brief 内存是否属于此内存块
   * @param address 内存地址
   * @return true或false
   */
  virtual auto is_valid(void *address) -> bool = 0;
};

// Destructor for smart pointer
template <typename T> void shared_ptr_deleter(T * obj) {
  obj->~T();
  // obj内存不需要释放，MemBlock将会回收
}

/**
 * @brief 在mb内建立一个std::shared_ptr实例
 * @tparam T 变量类型
 * @tparam ...ARGS 构造函数参数类型
 * @param mb 内存块指针
 * @param ...args 构造函数参数
 * @return std::shared_ptr实例
 */
template <typename T, typename... ARGS>
inline std::shared_ptr<T> make_shared(MemBlock *mb, ARGS... args) {
  if (!mb) {
    // 从堆上分配
    return std::make_shared<T>(std::forward<ARGS>(args)...);
  }
  auto *address = mb->get(sizeof(T));
  if (address) {
    // 从固定内存池内分配
    auto *optr = new (address) T(std::forward<ARGS>(args)...);
    return std::shared_ptr<T>(optr, shared_ptr_deleter<T>);
  } else {
    // 从堆上分配
    return std::make_shared<T>(std::forward<ARGS>(args)...);
  }
}
/**
 * @brief 建立一块新的内存块
 * @return 新的内存块智能指针
 */
extern auto new_mem_block() -> std::unique_ptr<MemBlock>;
/**
 * @brief 建立一块新的内存块
 * @return 新的内存块指针
 */
extern auto new_mem_block_raw() -> MemBlock *;

} // namespace plato
