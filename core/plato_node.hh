#pragma once

#include "plato_variable.hh"
#include <ctime>
#include <functional>

namespace plato {

class PlatoNodeLogic;
class PlatoPin;
class PlatoNode;
class PlatoFlow;
class PlatoStage;
class PlatoStageBuilder;

using PlatoNodeID = std::uint32_t;
using PlatoPinTypeID = std::uint8_t;
using PlatoFlowStatusID = std::uint32_t;
using PlatoPinPtr = std::shared_ptr<PlatoPin>;
using PlatoNodePtr = std::shared_ptr<PlatoNode>;
using PlatoNodeSyncTypeID = std::uint8_t;
using PlatoSyncCommandID = std::uint8_t;
using PlatoFlowID = std::uint32_t;
using PlatoPinIndex = int;
using PlatoStageID = std::uint64_t;
constexpr PlatoStageID INVALID_STAGE_ID = 0;
constexpr PlatoSyncCommandID INVALID_SYNC_COMMAND_ID = 0;
constexpr PlatoPinIndex INVALID_PIN_INDEX = -1;
constexpr PlatoNodeID INVALID_NODE_ID = 0;
using PlatoNodeLogicPtr = std::shared_ptr<PlatoNodeLogic>;

/**
 * @brief 引脚类型
 */
enum class PlatoPinType : PlatoPinTypeID {
  NONE = 0,
  EXEC = 1, ///< 可执行
  VAR,      ///< 变量
};
/**
 * @brief 节点同步类型
 */
enum class PlatoNodeSyncType : PlatoNodeSyncTypeID {
  NONE = 0,
  SERVER_SIDE = 1, ///< 服务端
  CLIENT_SIDE,     ///< 客户端
};
/**
 * @brief 节点同步命令
 */
enum class PlatoSyncCommand : PlatoSyncCommandID {
  CALL = 1, ///< 调用
  ACK,      ///< 返回
};
/**
 * @brief 引脚
 */
class PlatoPin {
public:
  virtual ~PlatoPin() {}
  /**
   * @brief 获取引脚类型
   * @return 引脚类型
   */
  virtual auto type() -> PlatoPinType = 0;
  /**
   * @brief 获取引脚变量
   * @return 引脚变量
   */
  virtual auto var() -> VariablePtr = 0;
  /**
   * @brief 获取回溯前置头节点
   * @return 回溯前置头节点
   */
  virtual auto backtrack_node() -> PlatoNodePtr = 0;
  /**
   * @brief 设置前置执行节点
   * @param node_ptr 前置执行节点
   * @return
   */
  virtual auto set_backtrack_node(PlatoNodePtr node_ptr) -> void = 0;
  /**
   * @brief 获取对象占用内存字节数
   * @return 对象占用内存字节数
   */
  virtual auto object_size() -> std::size_t = 0;
  /**
   * @brief 重置
   * @return
   */
  virtual auto reset() -> void = 0;
  /**
   * @brief 获取引脚变量并转换为T类型
   * @tparam T 需要转换的类型
   * @return 转换后的类型
   */
  template <typename T> auto dyn_cast() -> std::shared_ptr<T> {
    auto var_ptr = var();
    if (!var_ptr) {
      return nullptr;
    }
    return cast<T>(var());
  }
};

/**
 * @brief 节点
 */
class PlatoNode {
public:
  virtual ~PlatoNode() {}
  /**
   * @brief 获取节点静态ID
   * @return 节点静态ID
   */
  virtual auto static_id() -> PlatoNodeID = 0;
  /**
   * @brief 获取节点动态ID
   * @return 节点动态ID
   */
  virtual auto id() -> PlatoNodeID = 0;
  /**
   * @brief 节点同步类型
   * @return 同步类型
   */
  virtual auto sync_type() -> PlatoNodeSyncType = 0;
  /**
   * @brief 获取输入引脚
   * @param index 引脚索引
   * @return 引脚指针
   */
  virtual auto get_input_pin(PlatoPinIndex index) -> PlatoPin * = 0;
  /**
   * @brief 获取输出引脚
   * @param index 引脚索引
   * @return 引脚指针
   */
  virtual auto get_output_pin(PlatoPinIndex index) -> PlatoPin * = 0;
  /**
   * @brief 获取输入引脚数量
   * @return 输入引脚数量
   */
  virtual auto get_input_size() -> std::size_t = 0;
  /**
   * @brief 获取输出引脚数量
   * @return 输出引脚数量
   */
  virtual auto get_output_size() -> std::size_t = 0;
  /**
   * @brief 添加输入引脚
   * @param pin_ptr 引脚
   * @return
   */
  virtual auto add_input(PlatoPinPtr pin_ptr) -> void = 0;
  /**
   * @brief 添加输出引脚
   * @param pin_ptr 引脚
   * @return
   */
  virtual auto add_output(PlatoPinPtr pin_ptr) -> void = 0;
  /**
   * @brief 获取节点所属流
   * @return 流指针
   */
  virtual auto flow() -> PlatoFlow * = 0;
  /**
   * @brief 获取对象占用内存字节数
   * @return 对象占用内存字节数
   */
  virtual auto object_size() -> std::size_t = 0;
  /**
   * @brief 重置
   * @return
   */
  virtual auto reset() -> void = 0;
  /**
   * @brief 获取节点名称
   * @return 节点名称
   */
  virtual auto name() -> const char * = 0;
  /**
   * @brief 设置节点名称
   * @param name 节点名称
   * @return
   */
  virtual auto set_name(const char *name) -> void = 0;
};

#ifdef ERROR
#undef ERROR
#endif

/**
 * @brief 流状态
 */
enum class PlatoFlowStatus : PlatoFlowStatusID {
  NONE,
  OK = 1,                   ///< 节点执行成功
  RUNNING,                  ///< 节点正在运行中
  DONE,                     ///< 流运行结束
  ERROR,                    ///< 节点错误
  NOT_FOUND,                ///< 远端节点未找到
  INVOKING,                 ///< 等待远端节点返回
  DEBUG_SUSPEND_BEFORE_RUN, ///< 调试挂起-运行节点前
};
/**
 * @brief 节点逻辑执行结果
 */
struct ExecResult {
  PlatoPinIndex output_pin_index{INVALID_PIN_INDEX}; ///< 执行完成后出引脚的索引
};

using PlatoEventID = std::uint32_t;

/**
 * @brief 节点执行逻辑
 */
class PlatoNodeLogic {
public:
  virtual ~PlatoNodeLogic() {}
  /**
   * @brief 执行节点逻辑
   * @param node 逻辑所属节点
   * @param tick 当前时间戳(毫秒)
   * @param status 执行后的状态
   * @return 执行结果
   */
  virtual auto do_logic(PlatoNode *node, std::time_t tick,
                        PlatoFlowStatus &status) -> ExecResult = 0;
  /**
   * @brief 事件触发
   * @param node 逻辑所属节点
   * @return
   */
  virtual auto do_event(PlatoNode *node) -> void = 0;
};

/**
 * @brief 连线节点
 */
struct LinkNode {
  PlatoNodeID node_id{0}; ///< 连线所属节点ID
  int pin_index{0};       ///< 连线相关引脚索引
};
/**
 * @brief 节点逻辑管理器
 */
class PlatoLogicManager {
public:
  virtual ~PlatoLogicManager() {}
  /**
   * @brief 获取节点逻辑实例
   * @param static_id 节点静态ID
   * @return 节点逻辑实例
   */
  virtual auto get_logic_instance(PlatoNodeID static_id)
      -> PlatoNodeLogicPtr = 0;
};

using PlatoLogicManagerPtr = std::shared_ptr<PlatoLogicManager>;

/**
 * @brief 建立一个引脚
 * @param domain_ptr Domain
 * @param pin_type 引脚类型
 * @param var_ptr 引脚变量
 * @return 引脚实例
 */
extern auto new_pin(DomainPtr domain_ptr, PlatoPinType pin_type,
                    VariablePtr var_ptr) -> PlatoPinPtr;
/**
 * @brief 建立一个新流节点
 * @param domain_ptr Domain
 * @param static_id 节点静态ID
 * @param id 实例ID
 * @param sync_type 节点同步类型
 * @return 节点实例
 */
extern auto new_node(DomainPtr domain_ptr, PlatoNodeID static_id,
                     PlatoNodeID id, PlatoNodeSyncType sync_type)
    -> PlatoNodePtr;
/**
 * @brief 获取引脚实现类占用字节长度
 * @return 引脚实现类占用字节长度
 */
extern auto get_pin_size() -> std::size_t;
/**
 * @brief 获取节点实现类占用字节长度
 * @return 节点实现类占用字节长度
 */
extern auto get_node_size() -> std::size_t;
/**
 * @brief 设置节点逻辑管理器
 * @param manager_ptr 节点逻辑管理器实例
 * @return
 */
extern auto set_logic_manager(PlatoLogicManagerPtr manager_ptr) -> void;
/**
 * @brief 获取节点逻辑管理
 * @return 节点逻辑管理器实例
 */
extern auto get_logic_manager() -> PlatoLogicManagerPtr;

} // namespace plato
