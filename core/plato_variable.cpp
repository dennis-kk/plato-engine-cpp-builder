#include "plato_variable.hh"
#include "../impl/plato_stream_impl.hh"

plato::StructVariable::StructVariable(Domain *domain, VarID parent,
                                      PlatoVariableSyncType sync_type)
    : VariableImpl(domain, PlatoType::STRUCT, parent, sync_type) {}

plato::StructVariable::StructVariable(const std::string &name, Domain *domain,
                                      VarID parent,
                                      PlatoVariableSyncType sync_type)
    : VariableImpl(name, domain, PlatoType::STRUCT, parent, sync_type) {}

plato::StructVariable::~StructVariable() {}

auto plato::VariableImpl::set_id(VarID id) -> void { var_id_ = id; }

plato::VariableImpl::VariableImpl(Domain *domain, PlatoType type, VarID parent,
                                  PlatoVariableSyncType sync_type) {
  domain_ = domain;
  type_ = type;
  parent_ = parent;
  sync_type_ = sync_type;
}

plato::VariableImpl::VariableImpl(const std::string &name, Domain *domain,
                                  PlatoType type, VarID parent,
                                  PlatoVariableSyncType sync_type) {
  name_ = name;
  domain_ = domain;
  type_ = type;
  parent_ = parent;
  sync_type_ = sync_type;
}

plato::VariableImpl::~VariableImpl() { domain_->remove(var_id_); }

auto plato::VariableImpl::type() const -> PlatoType { return type_; }

auto plato::VariableImpl::name() -> const std::string & { return name_; }

auto plato::VariableImpl::parent() const -> VariablePtr {
  return domain_->get(parent_);
}

auto plato::VariableImpl::domain() const -> Domain * { return domain_; }

auto plato::VariableImpl::sync_type() const -> PlatoVariableSyncType {
  return sync_type_;
}

auto plato::VariableImpl::id() const -> VarID { return var_id_; }

extern auto create_struct(const std::string &name, plato::Domain *domain,
                          plato::VarID var_id,
                          plato::PlatoVariableSyncType sync_type)
    -> plato::VariablePtr;

namespace plato {

class DomainImpl : public Domain {
  VarID var_id_{1}; ///< 当前最大变量ID
  using VarMap = std::unordered_map<VarID, VariablePtr>;
  using VarNameMap = std::unordered_map<std::string, VariablePtr>;
  VarMap var_map_;                 ///< 变量表
  VarNameMap var_name_map_;        ///< 变量名字表
  PlatoStreamImpl stream_;         ///< 数据流
  DomainID id_{INVALID_DOMAIN_ID}; ///< 变量域ID
  MemBlock *mb_{nullptr};          ///< 内存块

public:
  DomainImpl(DomainID id = INVALID_DOMAIN_ID, MemBlock *mb = nullptr);
  virtual ~DomainImpl();
  virtual auto identify(VariablePtr ptr) -> bool override;
  virtual auto remove(VarID id) -> void override;
  virtual auto get(VarID id) -> VariablePtr override;
  virtual auto get(const std::string &name) -> VariablePtr override;
  virtual auto get_stream() -> PlatoStream & override;
  virtual auto do_sync() -> void override;
  virtual auto serialize_all() -> void override;
  virtual auto id() -> DomainID override;
  virtual auto mem_block() -> MemBlock * override;
  virtual auto reset_named_var() -> void override {
    for (auto &[_, v] : var_name_map_) {
      v->copy_default();
    }
  }
  virtual auto set_reserved_max_id(VarID var_id) -> void {
    var_id_ = var_id + 1;
  }
};

StringVariable::StringVariable(Domain *domain, VarID parent,
                               PlatoVariableSyncType sync_type)
    : VariableImpl(domain, PlatoType::STRING, parent, sync_type) {}

StringVariable::StringVariable(const std::string &name, Domain *domain,
                               VarID parent, PlatoVariableSyncType sync_type)
    : VariableImpl(name, domain, PlatoType::STRING, parent, sync_type) {}

StringVariable::StringVariable(Domain *domain, VarID parent,
                               PlatoVariableSyncType sync_type,
                               const std::string &t)
    : VariableImpl(domain, PlatoType::STRING, parent, sync_type) {
  s_ = t;
}

StringVariable::StringVariable(const std::string &name, Domain *domain,
                               VarID parent, PlatoVariableSyncType sync_type,
                               const std::string &t)
    : VariableImpl(name, domain, PlatoType::STRING, parent, sync_type) {
  s_ = t;
}

StringVariable::~StringVariable() {}

auto StringVariable::New(Domain *domain, VarID parent,
                         PlatoVariableSyncType sync_type)
    -> std::shared_ptr<StringVariable> {
  return std::make_shared<StringVariable>(domain, parent, sync_type);
}

auto StringVariable::serialize(PlatoStream &stream) -> bool {
  if (sync_type() == PlatoVariableSyncType::SENDER) {
    stream << id();
    stream << (std::uint32_t)s_.size();
    stream.write(s_.data(), s_.size());
    return true;
  }
  return false;
}

auto StringVariable::deserialize(PlatoStream &stream) -> bool {
  if (sync_type() == PlatoVariableSyncType::RECEIVER) {
    VarID var_id;
    stream >> var_id;
    set_id(var_id);
    std::uint32_t size = 0;
    stream >> size;
    stream.read(get_buffer(size), size);
    s_.assign(get_buffer(), size);
    return true;
  }
  return false;
}

StringVariable::operator std::string() { return s_; }

BoolVariable::BoolVariable(Domain *domain, VarID parent,
                           PlatoVariableSyncType sync_type)
    : VariableImpl(domain, TypeValue<bool>::value, parent, sync_type) {}

BoolVariable::BoolVariable(const std::string &name, Domain *domain,
                           VarID parent, PlatoVariableSyncType sync_type)
    : VariableImpl(name, domain, TypeValue<bool>::value, parent, sync_type) {}

BoolVariable::BoolVariable(Domain *domain, VarID parent,
                           PlatoVariableSyncType sync_type, bool t)
    : VariableImpl(domain, TypeValue<bool>::value, parent, sync_type) {
  value_ = t;
}
BoolVariable::BoolVariable(const std::string &name, Domain *domain,
                           VarID parent, PlatoVariableSyncType sync_type,
                           bool t)
    : VariableImpl(name, domain, TypeValue<bool>::value, parent, sync_type) {
  value_ = t;
}

BoolVariable::~BoolVariable() {}

auto BoolVariable::New(Domain *domain, VarID parent,
                       PlatoVariableSyncType sync_type)
    -> std::shared_ptr<BoolVariable> {
  return std::make_shared<BoolVariable>(domain, parent, sync_type);
}

auto BoolVariable::serialize(PlatoStream &stream) -> bool {
  if (sync_type() == PlatoVariableSyncType::SENDER) {
    stream << id();
    stream << value_;
    return true;
  }
  return false;
}

auto BoolVariable::deserialize(PlatoStream &stream) -> bool {
  if (sync_type() == PlatoVariableSyncType::RECEIVER) {
    VarID var_id;
    stream >> var_id;
    set_id(var_id);
    stream >> value_;
    return true;
  }
  return false;
}

auto Domain::NewNoIdentifiedBasic(PlatoType type,
                                  PlatoVariableSyncType sync_type)
    -> VariablePtr {
  VariablePtr var_ptr;
  switch (type) {
  case PlatoType::BOOL:
    var_ptr = NewNoIdentified<Bool>(sync_type);
    break;
  case PlatoType::INT32:
    var_ptr = NewNoIdentified<Int32>(sync_type);
    break;
  case PlatoType::INT64:
    var_ptr = NewNoIdentified<Int64>(sync_type);
    break;
  case PlatoType::UINT32:
    var_ptr = NewNoIdentified<Uint32>(sync_type);
    break;
  case PlatoType::UINT64:
    var_ptr = NewNoIdentified<Uint64>(sync_type);
    break;
  case PlatoType::STRING:
    var_ptr = NewNoIdentified<String>(sync_type);
    break;
  case PlatoType::FLOAT:
    var_ptr = NewNoIdentified<Float>(sync_type);
    break;
  default:
    return nullptr;
  }
  return var_ptr;
}

auto Domain::NewNoIdentifiedSet(PlatoType key_type,
                                PlatoVariableSyncType sync_type)
    -> VariablePtr {
  VariablePtr var_ptr;
  switch (key_type) {
  case PlatoType::BOOL:
    var_ptr = NewNoIdentified<Set<bool>>(sync_type);
    break;
  case PlatoType::INT32:
    var_ptr = NewNoIdentified<Set<std::int32_t>>(sync_type);
    break;
  case PlatoType::INT64:
    var_ptr = NewNoIdentified<Set<std::int64_t>>(sync_type);
    break;
  case PlatoType::UINT32:
    var_ptr = NewNoIdentified<Set<std::uint32_t>>(sync_type);
    break;
  case PlatoType::UINT64:
    var_ptr = NewNoIdentified<Set<std::uint64_t>>(sync_type);
    break;
  case PlatoType::STRING:
    var_ptr = NewNoIdentified<Set<std::string>>(sync_type);
    break;
  case PlatoType::FLOAT:
    var_ptr = NewNoIdentified<Set<float>>(sync_type);
    break;
  default:
    return nullptr;
  }
  return var_ptr;
}

auto Domain::NewNoIdentifiedArray(PlatoType key_type,
                                  PlatoVariableSyncType sync_type)
    -> VariablePtr {
  VariablePtr var_ptr;
  switch (key_type) {
  case PlatoType::BOOL:
    var_ptr = NewNoIdentified<Array<Bool>>(sync_type);
    break;
  case PlatoType::INT32:
    var_ptr = NewNoIdentified<Array<Int32>>(sync_type);
    break;
  case PlatoType::INT64:
    var_ptr = NewNoIdentified<Array<Int64>>(sync_type);
    break;
  case PlatoType::UINT32:
    var_ptr = NewNoIdentified<Array<Uint32>>(sync_type);
    break;
  case PlatoType::UINT64:
    var_ptr = NewNoIdentified<Array<Int64>>(sync_type);
    break;
  case PlatoType::STRING:
    var_ptr = NewNoIdentified<Array<String>>(sync_type);
    break;
  case PlatoType::FLOAT:
    var_ptr = NewNoIdentified<Array<Float>>(sync_type);
    break;
  default:
    return nullptr;
  }
  return var_ptr;
}

auto Domain::NewNoIdentifiedArrayStruct(const char *struct_name,
                                        PlatoVariableSyncType sync_type)
    -> VariablePtr {
  auto cont_name = std::string("Array") + struct_name;
  return create_struct(cont_name, this, INVALID_VAR_ID, sync_type);
}

using MapCreatorFunc = VariablePtr (*)(Domain *, VarID, PlatoVariableSyncType);

template <typename KeyT, typename ValueT>
static auto map_creator_func(Domain *domain, VarID var_id,
                             PlatoVariableSyncType sync_type) -> VariablePtr {
  return MapVariable<KeyT, ValueT>::New(domain, var_id, sync_type);
}

using MapCreatorMap = std::unordered_map<TypeID, MapCreatorFunc>;

MapCreatorMap map_bool_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<bool, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<bool, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<bool, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<bool, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<bool, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<bool, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<bool, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<bool, Uint64>},
};

MapCreatorMap map_string_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<std::string, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<std::string, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<std::string, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<std::string, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<std::string, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<std::string, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<std::string, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<std::string, Uint64>},
};

MapCreatorMap map_float_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<float, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<float, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<float, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<float, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<float, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<float, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<float, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<float, Uint64>},
};

MapCreatorMap map_double_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<double, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<double, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<double, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<double, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<double, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<double, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<double, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<double, Uint64>},
};

MapCreatorMap map_int32_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<std::int32_t, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<std::int32_t, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<std::int32_t, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<std::int32_t, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<std::int32_t, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<std::int32_t, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<std::int32_t, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<std::int32_t, Uint64>},
};

MapCreatorMap map_uint32_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<std::uint32_t, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<std::uint32_t, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<std::uint32_t, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<std::uint32_t, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<std::uint32_t, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<std::uint32_t, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<std::uint32_t, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<std::uint32_t, Uint64>},
};

MapCreatorMap map_int64_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<std::int64_t, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<std::int64_t, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<std::int64_t, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<std::int64_t, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<std::int64_t, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<std::int64_t, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<std::int64_t, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<std::int64_t, Uint64>},
};

MapCreatorMap map_uint64_creator_ = {
    {TypeID(PlatoType::BOOL), map_creator_func<std::uint64_t, Bool>},
    {TypeID(PlatoType::STRING), map_creator_func<std::uint64_t, String>},
    {TypeID(PlatoType::FLOAT), map_creator_func<std::uint64_t, Float>},
    {TypeID(PlatoType::DOUBLE), map_creator_func<std::uint64_t, Double>},
    {TypeID(PlatoType::INT32), map_creator_func<std::uint64_t, Int32>},
    {TypeID(PlatoType::INT64), map_creator_func<std::uint64_t, Int64>},
    {TypeID(PlatoType::UINT32), map_creator_func<std::uint64_t, Uint32>},
    {TypeID(PlatoType::UINT64), map_creator_func<std::uint64_t, Uint64>},
};

static std::unordered_map<TypeID, MapCreatorMap> map_creator_ = {
    {TypeID(PlatoType::BOOL), map_bool_creator_},
    {TypeID(PlatoType::STRING), map_string_creator_},
    {TypeID(PlatoType::FLOAT), map_float_creator_},
    {TypeID(PlatoType::DOUBLE), map_double_creator_},
    {TypeID(PlatoType::INT32), map_int32_creator_},
    {TypeID(PlatoType::UINT32), map_uint32_creator_},
    {TypeID(PlatoType::INT64), map_int64_creator_},
    {TypeID(PlatoType::UINT64), map_uint64_creator_},
};

auto Domain::NewNoIdentifiedMap(PlatoType key_type, PlatoType value_type,
                                PlatoVariableSyncType sync_type)
    -> VariablePtr {
  auto key_it = map_creator_.find(TypeID(key_type));
  if (key_it == map_creator_.end()) {
    return nullptr;
  }
  auto value_it = key_it->second.find(TypeID(value_type));
  if (value_it == key_it->second.end()) {
    return nullptr;
  }
  return value_it->second(this, INVALID_VAR_ID, sync_type);
}

auto Domain::NewNoIdentifiedMapStruct(PlatoType key_type,
                                      const char *struct_name,
                                      PlatoVariableSyncType sync_type)
    -> VariablePtr {
  auto cont_name = std::string("Map");
  switch (key_type) {
  case PlatoType::BOOL:
    cont_name += "Bool";
    break;
  case PlatoType::FLOAT:
    cont_name += "Float";
    break;
  case PlatoType::DOUBLE:
    cont_name += "Double";
    break;
  case PlatoType::INT32:
    cont_name += "Int32";
    break;
  case PlatoType::INT64:
    cont_name += "Int64";
    break;
  case PlatoType::UINT32:
    cont_name += "Uint32";
    break;
  case PlatoType::UINT64:
    cont_name += "Uint64";
    break;
  case PlatoType::STRING:
    cont_name += "String";
    break;
  default:
    break;
  }
  cont_name += struct_name;
  return create_struct(cont_name, this, INVALID_VAR_ID, sync_type);
}

} // namespace plato

plato::DomainImpl::DomainImpl(DomainID id, MemBlock *mb) {
  id_ = id;
  if (mb) {
    mb_ = mb;
  }
}

plato::DomainImpl::~DomainImpl() {
  VarMap temp;
  temp.swap(var_map_);
}

auto plato::DomainImpl::identify(VariablePtr ptr) -> bool {
  auto ptr_impl = std::dynamic_pointer_cast<VariableImpl>(ptr);
  if (!ptr_impl) {
    return false;
  }
  if (ptr_impl->id() == INVALID_VAR_ID) {
    ptr_impl->set_id(var_id_);
    var_map_.emplace(var_id_, ptr);
    var_id_ += 1;
  } else {
    var_map_.emplace(ptr->id(), ptr);
  }
  return true;
}

auto plato::DomainImpl::remove(VarID id) -> void { var_map_.erase(id); }

auto plato::DomainImpl::get(VarID id) -> VariablePtr {
  auto it = var_map_.find(id);
  if (it == var_map_.end()) {
    return nullptr;
  }
  return it->second;
}

auto plato::DomainImpl::get(const std::string &name) -> VariablePtr {
  auto it = var_name_map_.find(name);
  if (it == var_name_map_.end()) {
    return nullptr;
  }
  return it->second;
}

auto plato::DomainImpl::get_stream() -> PlatoStream & { return stream_; }

auto plato::DomainImpl::do_sync() -> void {
  while (stream_.available() > sizeof(VarID)) {
    VarID var_id = INVALID_VAR_ID;
    stream_.copy((char *)&var_id, sizeof(VarID));
    auto it = var_map_.find(var_id);
    if (it != var_map_.end()) {
      if (!it->second->deserialize(stream_)) {
        return;
      }
    }
  }
}

auto plato::DomainImpl::serialize_all() -> void {
  for (auto &[_, v] : var_map_) {
    v->serialize(stream_);
  }
}

auto plato::DomainImpl::id() -> DomainID { return id_; }

auto plato::DomainImpl::mem_block() -> MemBlock * { return mb_; }

static std::size_t buffer_size_ = 1024;
static std::shared_ptr<char[]> buffer_(new char[buffer_size_]);

char *plato::get_buffer(std::size_t size) {
  if (buffer_size_ < size) {
    buffer_ = std::shared_ptr<char[]>(new char[size]);
    buffer_size_ = size;
  }
  return buffer_.get();
}

char *plato::get_buffer() { return buffer_.get(); }

auto plato::new_domain(DomainID id, MemBlock *mb) -> DomainPtr {
  return std::make_shared<DomainImpl>(id, mb);
}
