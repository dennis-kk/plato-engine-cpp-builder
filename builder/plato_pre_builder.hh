#pragma once

#include "../core/plato_stage.hh"
#include "plato_node_factory.hh"
#include "thirdparty/jsoncpp/include/json/json.h"

#include <any>
#include <list>
#include <memory>
#include <vector>

namespace Json {
class Value;
}

namespace plato {

struct PlatoNodeCreator;
class PlatoNodeFactory;
class PlatoStageBuilderImpl;
class PlatoStagePreBuilder;
class PlatoVariablePreBuilder;
class PlatoVariablePreBuilderJson;

/**
 * @brief 节点信息
 */
struct NodeInfo {
  std::string node_name;     ///< 节点名称
  PlatoNodeID node_id;       ///< 节点ID
  bool is_start_node{false}; ///< 是否是起始节点
};
using NodeInfoVector = std::vector<NodeInfo>;

/**
 * @brief 连线端点对信息
 */
struct LinkNodePair {
  PlatoNodeID src_node_id;        ///< 起始节点ID
  PlatoPinIndex output_pin_index; ///< 输出引脚索引
  PlatoNodeID dst_node_id;        ///< 结束节点ID
  PlatoPinIndex input_pin_index;  ///< 输入引脚索引
};
using LinkNodePairVector = std::vector<LinkNodePair>;

using LinkList = std::list<Json::Value *>;
using NodeIDSet = std::unordered_set<plato::PlatoNodeID>;
using NodeIDSetVector = std::vector<NodeIDSet>;
using FlowLinkVector = std::vector<LinkList>;
using NodeIDMap = std::unordered_map<plato::PlatoNodeID, Json::Value *>;
using FlowNodeInfoVector = std::vector<NodeInfoVector>;
using FlowLinkNodePairVector = std::vector<LinkNodePairVector>;
using DynamicNodeCreatorVector =
    std::vector<std::shared_ptr<plato::PlatoNodeCreator>>;

class PlatoStagePreBuilder {
public:
  virtual ~PlatoStagePreBuilder() {}
  /**
   * @brief 解析配置文件并建立预处理数据
   * @return true或false
   */
  virtual auto build() -> bool = 0;
  /**
   * @brief 获取舞台名
   * @return 舞台名
   */
  virtual auto get_stage_name() -> const std::string & = 0;
  /**
   * @brief 获取舞台占用内存大小
   * @return 舞台占用内存大小
   */
  virtual auto get_stage_mem_size() -> std::size_t = 0;
  /**
   * @brief 是否已经预处理完成
   * @return
   */
  virtual auto is_built() -> bool = 0;
  /**
   * @brief 获取错误描述
   * @return 错误描述
   */
  virtual auto get_error() -> std::string = 0;
  /**
   * @brief 设置错误信息
   * @param error 错误信息
   * @return
   */
  virtual auto set_error(const std::string &error) -> void = 0;
  /**
   * @brief 获取配置文件路径
   * @return 配置文件路径
   */
  virtual auto get_config_file() -> const std::string & = 0;
  /**
   * @brief 获取节点信息数组
   * @return 节点信息数组
   */
  virtual auto get_node_info_vec() -> const FlowNodeInfoVector & = 0;
  /**
   * @brief 获取节点对信息(连线信息)
   * @return 节点对信息(连线信息)
   */
  virtual auto get_node_pair_vec() -> const FlowLinkNodePairVector & = 0;
  /**
   * @brief 获取舞台节点工厂
   * @return 舞台节点工厂
   */
  virtual auto get_stage_node_factory() -> PlatoNodeFactory * = 0;
  /**
   * @brief 获取变量预建造器
   * @return 变量预建造器
   */
  virtual auto get_pre_var_builder() -> PlatoVariablePreBuilder * = 0;
};

class PlatoStagePreBuilderJson : public PlatoStagePreBuilder {
  NodeIDSetVector node_id_set_vector_; ///< 每个流对应的节点数组
  FlowLinkVector flow_link_vector_;    ///< 每个流对应的节点连线数组
  NodeIDMap node_id_map_;              ///< 当前舞台的所有节点ID表
  std::string stage_name_;             ///< 舞台名
  std::string error_;                  ///< 错误描述
  std::string config_file_;            ///< 当前配置文件
  std::size_t stage_mem_size_{0};      ///< 舞台内存大小(字节)
  std::unique_ptr<Json::Value> root_;  ///< 配置根节点
  bool is_built_{false};               ///< 是否已经预处理完成
  FlowNodeInfoVector flow_node_info_array_;           ///< 流节点信息数组
  FlowLinkNodePairVector flow_link_node_pair_vector_; ///< 多个流的连线数组
  DynamicNodeCreatorVector
      dynamic_node_creator_array_; ///< 动态节点创建器数组，维持引用防止提前销毁
  PlatoNodeFactory *stage_node_factory_{nullptr}; ///< 舞台节点工厂
  std::unique_ptr<PlatoVariablePreBuilderJson>
      pre_var_builder_; ///< 变量预建造器

public:
  /**
   * @brief 构造
   * @param config_file 配置文件路径
   */
  PlatoStagePreBuilderJson(const std::string &config_file);
  /**
   * 析构
   */
  virtual ~PlatoStagePreBuilderJson();
  /**
   * @brief 解析配置文件并建立预处理数据
   * @return true或false
   */
  virtual auto build() -> bool override;
  /**
   * @brief 获取舞台名
   * @return 舞台名
   */
  virtual auto get_stage_name() -> const std::string & override;
  /**
   * @brief 获取舞台占用内存大小
   * @return 舞台占用内存大小
   */
  virtual auto get_stage_mem_size() -> std::size_t override;
  /**
   * @brief 是否已经预处理完成
   * @return
   */
  virtual auto is_built() -> bool override;
  /**
   * @brief 获取错误描述
   * @return 错误描述
   */
  virtual auto get_error() -> std::string override;
  /**
   * @brief 设置错误信息
   * @param error 错误信息
   * @return
   */
  virtual auto set_error(const std::string &error) -> void override;
  /**
   * @brief 获取配置文件路径
   * @return 配置文件路径
   */
  virtual auto get_config_file() -> const std::string & override;
  /**
   * @brief 获取节点信息数组
   * @return 节点信息数组
   */
  virtual auto get_node_info_vec() -> const FlowNodeInfoVector & override;
  /**
   * @brief 获取节点对信息(连线信息)
   * @return 节点对信息(连线信息)
   */
  virtual auto get_node_pair_vec() -> const FlowLinkNodePairVector & override;
  /**
   * @brief 获取舞台节点工厂
   * @return 舞台节点工厂
   */
  virtual auto get_stage_node_factory() -> PlatoNodeFactory * override;
  /**
   * @brief 获取变量预建造器
   * @return 变量预建造器
   */
  virtual auto get_pre_var_builder() -> PlatoVariablePreBuilder * override;

protected:
  /**
   * @brief
   * 从src_link_list内的迭代器source_it删除连线及节点并添加加点ID到dst_node_id_set，从src_node_id_set内删除，连线到dst_link_list
   * @param source_it src_link_list的迭代器
   * @param src_node_id_set 源节点ID结合
   * @param src_link_list 连线表
   * @param [IN OUT] dst_node_id_set 节点结合
   * @param [IN OUT] dst_link_list 目标连线集合
   * @return
   */
  auto insert_and_remove_link(LinkList::iterator source_it,
                              LinkList &src_link_list,
                              NodeIDSet &src_node_id_set,
                              NodeIDSet &dst_node_id_set,
                              LinkList &dst_link_list) -> LinkList::iterator;
  /**
   * @brief 检查节点是否在集合内存在
   * @param id_set 节点集合
   * @param src_id 起始节点
   * @param dst_id 结束节点
   * @return true或false
   */
  auto check_exists(const NodeIDSet &id_set, PlatoNodeID src_id,
                    PlatoNodeID dst_id) -> bool;
  /**
   * @brief 解析并返回舞台名字
   * @param src 文件名
   * @return 舞台名字
   */
  auto build_stage_name(const std::string &src) -> std::string;
  /**
   * @brief 获取JSON文件内容
   * @param file_path 文件路径
   * @param [IN OUT] root JSON根节点
   * @return true成功,false失败
   */
  auto get_json_file_content(const std::string &file_path, Json::Value &root)
      -> bool;
  /**
   * @brief 注册getter,setter节点的创建器
   * @param root JSON根节点
   * @return true成功,false失败
   */
  auto register_getter_setter_creator(Json::Value &root) -> bool;
  /**
   * @brief 计算舞台内存占用量
   * @param root JSON配置根节点
   * @return 舞台内存占用量
   */
  auto query_stage_size(Json::Value &root) -> std::size_t;
  /**
   * @brief 获取舞台节点内存占用量
   * @param root JSON配置根节点
   * @return 舞台节点内存占用量
   */
  auto get_node_size(Json::Value &root) -> std::size_t;
  /**
   * @brief 解析流并返回舞台内流的数量
   * @param root JSON配置根节点
   * @return 舞台内流的数量
   */
  auto parse_flow(Json::Value &root) -> std::size_t;
  /**
   * @brief 预建立一条流
   * @param node_id_set 流节点集合
   * @param link_vector 流连线集合
   * @return true成功,false失败
   */
  auto pre_build_flow(const NodeIDSet &node_id_set, const LinkList &link_list)
      -> bool;
  /**
   * @brief 预处理流内所有节点
   * @param node_id_set 流节点集合
   * @return true成功,false失败
   */
  auto pre_build_flow_nodes(const NodeIDSet &node_id_set) -> bool;
  /**
   * @brief 预创建舞台
   * @param root JSON配置根节点
   * @return true成功,false失败
   */
  auto pre_build_stage(Json::Value &root) -> bool;
  /**
   * @brief 预处理流内所有连线
   * @param link_list 连线表
   * @return true成功,false失败
   */
  auto pre_build_flow_links(const LinkList &link_list) -> bool;
  /**
   * @brief 获取节点创建器
   * @param node 节点
   * @return 节点创建器
   */
  auto get_node_creator(Json::Value &node) -> PlatoNodeCreator *;
  /**
   * @brief 合并解析后的流
   * @return
   */
  auto merge_flow() -> void;
  /**
   * @brief 删除空的节点集合和流链表
   * @return
   */
  auto remove_empty_set() -> void;
  /**
   * @brief 初步筛选节点和连线
   * @param unk_link_list 无归属连线
   * @param unk_node_id_set 无归属节点
   * @return
   */
  auto pick_link_and_node(LinkList &unk_link_list, NodeIDSet &unk_node_id_set)
      -> void;
  /**
   * @brief 搜集无归属的所有连线和节点
   * @param root JSON根节点
   * @param [IN OUT] unk_link_list 无归属连线
   * @param [IN OUT] unk_node_id_set 无归属节点
   * @return
   */
  auto merge_unknow_link_and_node(Json::Value &root, LinkList &unk_link_list,
                                  NodeIDSet &unk_node_id_set) -> void;
  /**
   * @brief 清理建造过程中产生的临时变量
   * @return
   */
  auto cleanup() -> void;
  /**
   * @brief 切分字符串
   * @param src 源字符串
   * @param delim 分隔符
   * @param [IN OUT] result 切分结果
   */
  void split(const std::string &src, const std::string &delim,
             std::vector<std::string> &result);
};

using PlatoStagePreBuilderPtr = std::unique_ptr<PlatoStagePreBuilder>;

extern auto getPlatoStagePreBuilder(const std::string &config_file)
    -> PlatoStagePreBuilderPtr;

} // namespace plato
