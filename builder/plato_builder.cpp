#include "plato_builder.hh"
#include "getter_setter_node.hh"
#include "plato_node_creator.hh"
#include "plato_node_factory.hh"
#include "plato_pre_var_builder.hh"

#include <algorithm>
#include <fstream>
#include <unordered_set>

plato::PlatoStageBuilderImpl::PlatoStageBuilderImpl() {}

plato::PlatoStageBuilderImpl::~PlatoStageBuilderImpl() {}

auto plato::PlatoStageBuilderImpl::pre_build(const std::string &config_file)
    -> bool {
  if (!pre_builder_) {
    pre_builder_ = getPlatoStagePreBuilder(config_file);
  }
  if (pre_builder_->is_built()) {
    return true;
  }
  return pre_builder_->build();
}

auto plato::PlatoStageBuilderImpl::build(PlatoStageID stage_id,
                                         FlowRunMode run_mode)
    -> PlatoStagePtr {
  // 预处理
  if (!pre_builder_->is_built()) {
    return nullptr;
  }
  // 建立舞台实例对象
  auto stage_ptr =
      new_stage(stage_id, pre_builder_->get_stage_mem_size(), run_mode);
  // 建立舞台
  if (!build_stage(stage_ptr)) {
    return nullptr;
  }
  // 返回舞台指针
  return stage_ptr;
}

auto plato::PlatoStageBuilderImpl::get_error() -> std::string {
  return pre_builder_->get_error();
}

auto plato::PlatoStageBuilderImpl::get_config_file() -> const std::string & {
  return pre_builder_->get_config_file();
}

auto plato::PlatoStageBuilderImpl::build_stage(PlatoStagePtr stage_ptr)
    -> bool {
  // 建立变量
  if (!build_variables(stage_ptr)) {
    return false;
  }
  // 建立所有流
  std::size_t index = 0;
  for (const auto &pair_vec : pre_builder_->get_node_pair_vec()) {
    // 从舞台创建一个新的流
    auto flow_ptr = stage_ptr->add_flow();
    // 流的节点集合
    const auto &node_info_vec = pre_builder_->get_node_info_vec()[index++];
    // 建造流的内容
    if (!build_flow(flow_ptr, node_info_vec, pair_vec)) {
      return false;
    }
  }
  return true;
}

auto plato::PlatoStageBuilderImpl::build_variables(PlatoStagePtr stage_ptr)
    -> bool {
  auto *pre_var_builder = pre_builder_->get_pre_var_builder();
  // 设置保留ID, 保证ID不会重复
  stage_ptr->domain()->set_reserved_max_id(
      pre_var_builder->get_max_var_reserved_id());
  // 建立所有变量
  for (const auto &var_id : pre_var_builder->get_stage_var_id_vec()) {
    // 变量默认是双向同步的
    pre_var_builder->get_stage_var_creator_func()(
        var_id, stage_ptr->domain(), PlatoVariableSyncType::SENDER);
  }
  return true;
}

auto plato::PlatoStageBuilderImpl::build_flow(
    PlatoFlowPtr flow_ptr, const NodeInfoVector &node_info_vec,
    const LinkNodePairVector &pair_vec) -> bool {
  // 加入所有节点到流
  if (!build_flow_nodes(flow_ptr, node_info_vec)) {
    return false;
  }
  // 加入所有连线到流
  if (!build_flow_links(flow_ptr, pair_vec)) {
    return false;
  }
  // 完成流建造
  return flow_ptr->complete();
}

auto plato::PlatoStageBuilderImpl::build_flow_nodes(
    PlatoFlowPtr flow_ptr, const NodeInfoVector &node_info_vec) -> bool {
  // 获取全局节点工厂
  auto *node_factory_pointer = get_node_factory();
  // 创建流的节点并加入到流内
  for (const auto &node_info : node_info_vec) {
    // 创建节点实例
    auto node_ptr = node_factory_pointer->create(
        node_info.node_name, flow_ptr->get_domain(), node_info.node_id);
    if (!node_ptr) {
      //
      // 从舞台内节点工厂创建(Getter/Setter)
      //
      // 创建节点实例
      node_ptr = pre_builder_->get_stage_node_factory()->create(
          flow_ptr->get_domain(), node_info.node_id);
    }
    if (!node_ptr) {
      return false;
    }
    // 加入流节点
    if (!flow_ptr->add_node(node_ptr)) {
      return false;
    }
    // 设置起始节点, 事件节点必须是起始节点
    if (node_info.is_start_node) {
      flow_ptr->set_start_node(node_ptr);
    }
  }
  return true;
}

auto plato::PlatoStageBuilderImpl::build_flow_links(
    PlatoFlowPtr flow_ptr, const LinkNodePairVector &node_pair_vec) -> bool {
  for (const auto &pair : node_pair_vec) {
    flow_ptr->add_link({pair.src_node_id, pair.output_pin_index},
                       {pair.dst_node_id, pair.input_pin_index});
  }
  return true;
}

auto get_builder(const std::string &config_file) -> StageBuilderPtr {
  auto stage_builder_ptr = std::make_shared<plato::PlatoStageBuilderImpl>();
  if (!stage_builder_ptr->pre_build(config_file)) {
    return nullptr;
  }
  return stage_builder_ptr;
}
