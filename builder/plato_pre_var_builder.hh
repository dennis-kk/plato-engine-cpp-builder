#pragma once

#include "../core/plato_stage.hh"
#include "plato_node_factory.hh"
#include "thirdparty/jsoncpp/include/json/json.h"

#include <any>
#include <unordered_map>
#include <vector>

namespace plato {

class PlatoStagePreBuilder;

using VarSizeMap = std::unordered_map<VarID, std::size_t>;
using VarNameMap = std::unordered_map<VarID, std::string>;
using VarIDVector = std::vector<VarID>;

/**
 * @brief 变量信息
 */
struct VarInfo {
  VarID var_id{INVALID_VAR_ID}; ///< 变量ID
  std::string name;             ///< 变量名字
  std::string type_name; ///< 变量类型名，针对struct，容器key或者容器value
  PlatoType type{PlatoType::NONE};       ///< 类型枚举
  PlatoType key_type{PlatoType::NONE};   ///< 容器key类型枚举
  PlatoType value_type{PlatoType::NONE}; ///< 容器value类型枚举
  std::any default_value;                ///< 默认值
};
using VarInfoMap = std::unordered_map<VarID, VarInfo>;

class PlatoVariablePreBuilder {
public:
  virtual ~PlatoVariablePreBuilder() {}
  /**
   * @brief 获取舞台变量保留ID最大值
   * @return 舞台变量保留ID最大值
   */
  virtual auto get_max_var_reserved_id() -> VarID = 0;
  /**
   * @brief 获取舞台变量ID数组
   * @return 舞台变量ID数组
   */
  virtual auto get_stage_var_id_vec() -> const VarIDVector & = 0;
  /**
   * @brief 获取舞台变量创建函数
   * @return 舞台变量创建函数
   */
  virtual auto get_stage_var_creator_func() -> VarCreatorFunc = 0;
  /**
   * @brief 设置变量默认值
   * @param var_info 变量信息
   * @param var_ptr 变量指针
   * @return
   */
  virtual auto set_basic_default_value(const VarInfo &var_info,
                                       VariablePtr var_ptr) -> void = 0;
  /**
   * @brief 查询变量内存大小
   * @param var_id 变量ID
   * @return 变量内存大小
   */
  virtual auto query_variable_size(plato::VarID var_id) -> std::size_t = 0;
  /**
   * @brief 通过变量ID查询变量名
   * @param var_id 变量ID
   * @return 变量名
   */
  virtual auto get_variable_name(plato::VarID var_id)
      -> const std::string & = 0;
  /**
   * @brief 通过类型名获取变量类型
   * @param type_name
   * @return 变量类型
   */
  virtual auto get_variable_type(const std::string &type_name) -> PlatoType = 0;
  /**
   * @brief 注册变量相关设施
   * @return
   */
  virtual auto register_variable_infrastructure() -> void = 0;

  /**
   * @brief 建立变量
   * @param var_id 变量ID
   * @param domain_ptr 变量域
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  virtual auto create_variable(plato::VarID var_id, DomainPtr domain_ptr,
                               plato::PlatoVariableSyncType sync_type)
      -> VariablePtr = 0;
};

class PlatoVariablePreBuilderJson : public PlatoVariablePreBuilder {
  VarSizeMap var_size_map_;         ///< 变量长度表
  VarNameMap var_name_map_;         ///< 变量名字表
  VarInfoMap var_info_map_;         ///< 变量信息表
  VarID max_reserved_id_{1};        ///< 需要保留的最大变量ID
  VarIDVector stage_var_id_vector_; ///< 舞台内变量ID数组
  VarCreatorFunc stage_var_create_func_{nullptr}; ///< 舞台变量创建函数
  PlatoStagePreBuilder *pre_builder_{nullptr};    ///< 舞台预创建器

public:
  /**
   * @brief 构造
   */
  PlatoVariablePreBuilderJson(PlatoStagePreBuilder *pre_builder);
  /**
   * 析构
   */
  virtual ~PlatoVariablePreBuilderJson();
  /**
   * @brief 获取舞台变量保留ID最大值
   * @return 舞台变量保留ID最大值
   */
  virtual auto get_max_var_reserved_id() -> VarID override;
  /**
   * @brief 获取舞台变量ID数组
   * @return 舞台变量ID数组
   */
  virtual auto get_stage_var_id_vec() -> const VarIDVector & override;
  /**
   * @brief 获取舞台变量创建函数
   * @return 舞台变量创建函数
   */
  virtual auto get_stage_var_creator_func() -> VarCreatorFunc override;
  /**
   * @brief 设置变量默认值
   * @param var_info 变量信息
   * @param var_ptr 变量指针
   * @return
   */
  virtual auto set_basic_default_value(const VarInfo &var_info,
                                       VariablePtr var_ptr) -> void override;
  /**
   * @brief 查询变量内存大小
   * @param var_id 变量ID
   * @return 变量内存大小
   */
  virtual auto query_variable_size(plato::VarID var_id) -> std::size_t override;
  /**
   * @brief 注册变量相关设施
   * @return
   */
  virtual auto register_variable_infrastructure() -> void override;
  /**
   * @brief 建立变量
   * @param var_id 变量ID
   * @param domain_ptr 变量域
   * @param sync_type 同步类型
   * @return 变量智能指针
   */
  virtual auto create_variable(plato::VarID var_id, DomainPtr domain_ptr,
                               plato::PlatoVariableSyncType sync_type)
      -> VariablePtr override;

  /**
   * @brief 通过变量ID查询变量名
   * @param var_id 变量ID
   * @return 变量名
   */
  virtual auto get_variable_name(plato::VarID var_id)
      -> const std::string & override;
  /**
   * @brief 通过类型名获取变量类型
   * @param type_name
   * @return 变量类型
   */
  virtual auto get_variable_type(const std::string &type_name)
      -> PlatoType override;

public:
  /**
   * @brief 预处理变量
   * @param root JSON根节点
   * @return true或false
   */
  auto build(Json::Value &root) -> bool;
  /**
   * @brief 通过变量配置获取变量内存大小
   * @param var 变量JSON对象
   * @return 变量内存大小
   */
  auto get_variable_size(const Json::Value &var) -> std::size_t;
  /**
   * @brief 获取舞台变量内存占用量
   * @param root JSON配置根节点
   * @return 舞台变量内存占用量
   */
  auto get_variable_size(Json::Value &root) -> std::size_t;
  /**
   * @brief 预建立所有变量
   * @param root JSON配置根节点
   * @return true成功,false失败
   */
  auto pre_build_variables(Json::Value &root) -> bool;
  /**
   * @brief 搜集变量信息
   * @param root 配置根节点
   * @return true成功,false失败
   */
  auto collect_var_infos(Json::Value &root) -> bool;
};

} // namespace plato
