#pragma once

#include "../core/plato_stage.hh"
#include <string>

namespace plato {

struct PlatoNodeCreator;

/**
 * @brief 节点工厂
 */
class PlatoNodeFactory {
public:
  virtual ~PlatoNodeFactory() {}
  /**
   * @brief 创建节点从全局域
   * @param name 节点名
   * @param domain_ptr 变量域
   * @param id 节点ID
   * @return 节点实例
   */
  virtual auto create(const std::string &name, DomainPtr domain_ptr,
                      PlatoNodeID id) -> PlatoNodePtr = 0;
  /**
   * @brief 创建节点，当前舞台域
   * @param domain_ptr 变量域
   * @param id 节点ID
   * @return 节点实例
   */
  virtual auto create(DomainPtr domain_ptr, PlatoNodeID id) -> PlatoNodePtr = 0;
  /**
   * @brief 获取节点创建器
   * @param name 节点名称
   * @return 节点创建器
   */
  virtual auto get_creator(const std::string &name) -> PlatoNodeCreator * = 0;
  /**
   * @brief 获取节点创建器
   * @param id 节点ID
   * @return 节点创建器
   */
  virtual auto get_creator(PlatoNodeID id) -> PlatoNodeCreator * = 0;
};

} // namespace plato

/**
 * @brief 变量创建函数原型
 */
using VarCreatorFunc = std::function<plato::VariablePtr(
    plato::VarID, plato::DomainPtr, plato::PlatoVariableSyncType)>;
/**
 * @brief 变量内存占用字节函数原型
 */
using VarCreatorSizeFunc = std::function<std::size_t(plato::VarID)>;
/**
 * @brief 变量名查询函数
 */
using VarNameQueryFunc =
    std::function<const std::string &(plato::VarID var_id)>;
/**
 * @brief 获取变量名查询函数
 * @param name 舞台名
 * @return 变量名查询函数
 */
extern auto get_var_name_query(const char *name) -> VarNameQueryFunc;
/**
 * @brief 获取全局节点工厂
 * @return 全局节点工厂
 */
extern auto get_node_factory() -> plato::PlatoNodeFactory *;
/**
 * @brief 获取舞台专用节点工厂
 * @param name 舞台名
 * @return 舞台专用节点工厂
 */
extern auto get_node_factory(const char *name) -> plato::PlatoNodeFactory *;
/**
 * @brief 获取舞台变量创建函数
 * @param name 舞台名
 * @return 舞台变量创建函数
 */
extern auto get_var_creator(const char *name) -> VarCreatorFunc;
/**
 * @brief 获取舞台变量内存大小函数
 * @param name 舞台名
 * @return 舞台变量内存大小函数
 */
extern auto get_var_size_query(const char *name) -> VarCreatorSizeFunc;
/**
 * @brief 添加节点创建器
 * @param factory 节点工厂
 * @param node_id 节点ID
 * @param creator 节点创建器
 * @return true或false
 */
extern auto add_node_creator(plato::PlatoNodeFactory *factory,
                             plato::PlatoNodeID node_id,
                             plato::PlatoNodeCreator *creator) -> bool;
/**
 * @brief 添加舞台变量创建相关函数
 * @param name 舞台名
 * @param func 创建函数指针
 * @param size_func 内存占用查询函数指针
 * @return true或false
 */
extern auto add_var_creator(const char *name, VarCreatorFunc func,
                            VarCreatorSizeFunc size_func) -> bool;
/**
 * @brief 添加舞台变量名字查询函数
 * @param name 舞台名
 * @param func 变量名字查询函数指针
 * @return true或false
 */
extern auto add_var_name_query(const char *name, VarNameQueryFunc func) -> bool;
/**
 * @brief 创建舞台变量
 * @param name 舞台名
 * @param domain 变量域
 * @param var_id 变量配置ID
 * @param sync_type 同步类型
 * @return 变量智能指针
*/
extern auto create_struct(const std::string &name, plato::Domain *domain,
                          plato::VarID var_id,
                          plato::PlatoVariableSyncType sync_type)
    -> plato::VariablePtr;
/**
 * @brief 获取struct内存占用大小
 * @param name struct名字
 * @return 内存占用大小
*/
extern auto get_struct_size(const std::string &name) -> std::size_t;
