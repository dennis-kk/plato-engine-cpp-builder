#include "plato_pre_builder.hh"
#include "getter_setter_node.hh"
#include "plato_node_creator.hh"
#include "plato_node_factory.hh"
#include "plato_pre_var_builder.hh"

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <unordered_set>

namespace plato {

PlatoStagePreBuilderJson::PlatoStagePreBuilderJson(
    const std::string &config_file) {
  config_file_ = config_file;
}

PlatoStagePreBuilderJson::~PlatoStagePreBuilderJson() {}

auto PlatoStagePreBuilderJson::build() -> bool {
  try {
    // 获取舞台名，舞台名为文件名去掉扩展名: name.plato -> name
    stage_name_ = build_stage_name(config_file_);
    // 打开并获取JSON文件的根对象
    root_.reset(new Json::Value());
    if (!get_json_file_content(config_file_, *root_)) {
      return false;
    }
    pre_var_builder_.reset(new PlatoVariablePreBuilderJson(this));
    if (!pre_var_builder_->build(*root_)) {
      return false;
    }
    // 获取舞台占用内存(字节)
    stage_mem_size_ = query_stage_size(*root_);
    // 预处理
    if (!pre_build_stage(*root_)) {
      return false;
    }
  } catch (std::exception &e) {
    // 发生异常, 记录异常原因
    error_ = e.what();
    return false;
  }
  return true;
}

auto PlatoStagePreBuilderJson::get_stage_name() -> const std::string & {
  return stage_name_;
}

auto PlatoStagePreBuilderJson::get_stage_mem_size() -> std::size_t {
  return stage_mem_size_;
}

auto PlatoStagePreBuilderJson::is_built() -> bool { return is_built_; }

auto plato::PlatoStagePreBuilderJson::get_json_file_content(
    const std::string &file_path, Json::Value &root) -> bool {
  std::ifstream ifs;
  ifs.open(file_path, std::ios::in);
  if (!ifs) {
    error_ = "Cannot open " + file_path;
    return false;
  }
  // 建立JSON读取器
  Json::CharReaderBuilder builder;
  std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
  // 读取JSON文件内容
  std::string str((std::istreambuf_iterator<char>(ifs)),
                  std::istreambuf_iterator<char>());
  Json::String error;
  if (!reader->parse(str.c_str(), str.c_str() + str.size(), &root, &error)) {
    // 格式错误
    error_ = error;
    return false;
  }
  return true;
}

auto PlatoStagePreBuilderJson::register_getter_setter_creator(Json::Value &root)
    -> bool {
  // 此函数必须在变量建立完成后运行
  for (auto &node : root["nodes"]) {
    if (!node.isMember("title") || !node.isMember("id")) {
      throw std::runtime_error("Invalid JSON format");
    }
    // 节点名称
    auto title = node["title"].asString();
    // Getter/Setter节点创建器指针
    GetterSetterNodeCreator *node_creator_pointer = nullptr;
    if (title == "Getter") {
      // 创建Getter创建器
      auto getter_creator = std::make_shared<GetterCreator>();
      // 获取指针
      node_creator_pointer = getter_creator.get();
      // 添加在动态节点创建器数组
      dynamic_node_creator_array_.emplace_back(getter_creator);
    } else if (title == "Setter") {
      // 创建Setter创建器
      auto setter_creator = std::make_shared<SetterCreator>();
      // 获取指针
      node_creator_pointer = setter_creator.get();
      // 添加在动态节点创建器数组
      dynamic_node_creator_array_.emplace_back(setter_creator);
    }
    if (node_creator_pointer) {
      // 节点ID, Getter/Setter节点ID为舞台内的静态ID
      auto node_static_id = PlatoNodeID(node["id"].asUInt());
      // 设置节点静态ID
      node_creator_pointer->set_static_id(node_static_id);
      auto var_name_query_func = get_var_name_query(stage_name_.c_str());
      if (!var_name_query_func) {
        return false;
      }
      if (!node.isMember("variableId")) {
        throw std::runtime_error("Invalid JSON format");
      }
      // 绑定的变量ID
      auto var_id = VarID(node["variableId"].asUInt());
      // 变量名
      const auto &var_name = var_name_query_func(var_id);
      // 设置绑定的变量名和变量ID
      node_creator_pointer->add_var_pin(var_name, var_id);
      // 舞台内节点创建工厂, 区别于全局节点创建工厂
      auto *stage_node_factory = get_node_factory(stage_name_.c_str());
      if (!stage_node_factory) {
        return false;
      }
      // 添加到舞台内节点创建器
      add_node_creator(stage_node_factory, node_static_id,
                       node_creator_pointer);
    }
  }
  return true;
}

auto PlatoStagePreBuilderJson::query_stage_size(Json::Value &root)
    -> std::size_t {
  // 获取变量内存占用
  std::size_t total_size = pre_var_builder_->get_variable_size(root);
  // 获取舞台所有节点内存占用
  auto node_size = get_node_size(root);
  if (!node_size) {
    return 0;
  }
  total_size += node_size;
  // 获取流数量
  auto flow_count = parse_flow(root);
  if (!flow_count) {
    return 0;
  }
  // 流占用内存
  auto flow_size = flow_count * get_flow_size();
  // 返回总内存占用
  return (total_size + flow_size);
}

auto PlatoStagePreBuilderJson::get_node_size(Json::Value &root) -> std::size_t {
  std::size_t total_size = 0;
  // 获取舞台所有节点内存占用
  for (auto &node : root["nodes"]) {
    if (!node.isMember("title") || !node.isMember("id")) {
      throw std::runtime_error("Invalid JSON format");
    }
    auto title = node["title"].asString();
    if (title == "Getter") {
      total_size += GetterCreator::get_fixed_memory_size();
    } else if (title == "Setter") {
      total_size += SetterCreator::get_fixed_memory_size();
    } else {
      auto *creator = get_node_factory()->get_creator(title);
      if (creator) {
        total_size += creator->get_node_memory_size();
      } else {
        throw std::runtime_error("Unknown node '" + title + "'");
      }
    }
    node_id_map_.emplace(PlatoNodeID(node["id"].asUInt()), &node);
  }
  return total_size;
}

auto PlatoStagePreBuilderJson::parse_flow(Json::Value &root) -> std::size_t {
  LinkList unk_link_list;    // 无归属的连线
  NodeIDSet unk_node_id_set; // 无归属的节点
  // 搜集所有节点和连线
  merge_unknow_link_and_node(root, unk_link_list, unk_node_id_set);
  // 遍历一次挑选节点和连线
  pick_link_and_node(unk_link_list, unk_node_id_set);
  // 合并集合形成多个流所需的节点和连线
  merge_flow();
  // 返回流的数量
  return flow_link_vector_.size();
}

auto PlatoStagePreBuilderJson::pre_build_flow(const NodeIDSet &node_id_set,
                                              const LinkList &link_list)
    -> bool {
  // 加入所有节点到流
  if (!pre_build_flow_nodes(node_id_set)) {
    return false;
  }
  // 加入所有连线到流
  if (!pre_build_flow_links(link_list)) {
    return false;
  }
  return true;
}

auto PlatoStagePreBuilderJson::pre_build_flow_nodes(
    const NodeIDSet &node_id_set) -> bool {
  NodeInfoVector node_info_vector;
  // 创建流的节点并加入到流内
  for (const auto &node_id : node_id_set) {
    // 节点配置引用
    auto &node_ref = *node_id_map_[node_id];
    if (!node_ref.isMember("title") || !node_ref.isMember("category")) {
      throw std::runtime_error("Invalid JSON format");
    }
    // 节点名称
    auto node_name = node_ref["title"].asString();
    // 设置起始节点, 事件节点必须是起始节点
    if (node_ref["category"].asString() == "Event") {
      node_info_vector.emplace_back(NodeInfo{node_name, node_id, true});
    } else {
      node_info_vector.emplace_back(NodeInfo{node_name, node_id, false});
    }
  }
  flow_node_info_array_.emplace_back(node_info_vector);
  return true;
}

auto PlatoStagePreBuilderJson::pre_build_stage(Json::Value &root) -> bool {
  // 注册动态节点创建器
  if (!register_getter_setter_creator(root)) {
    return false;
  }
  // 舞台节点工厂
  stage_node_factory_ = get_node_factory(stage_name_.c_str());
  // 建立所有流
  std::size_t index = 0;
  for (const auto &link_list : flow_link_vector_) {
    // 流的节点集合
    const auto &node_id_set = node_id_set_vector_[index++];
    // 建造流的内容
    if (!pre_build_flow(node_id_set, link_list)) {
      return false;
    }
  }
  // 清理掉预处理数据
  cleanup();
  is_built_ = true;
  return true;
}

auto PlatoStagePreBuilderJson::pre_build_flow_links(const LinkList &link_list)
    -> bool {
  LinkNodePairVector pair_vec;
  for (const auto *link_ptr : link_list) {
    auto &link = *link_ptr;
    if (!link.isMember("srcNodeId") || !link.isMember("dstNodeId") ||
        !link.isMember("srcPinTitle") || !link.isMember("dstPinTitle")) {
      throw std::runtime_error("Invalid JSON format");
    }
    auto src_node_id = PlatoNodeID(link["srcNodeId"].asUInt());
    auto dst_node_id = PlatoNodeID(link["dstNodeId"].asUInt());
    auto &src_node_ref = *node_id_map_[src_node_id];
    auto &dst_node_ref = *node_id_map_[dst_node_id];
    auto *src_node_creator = get_node_creator(src_node_ref);
    auto src_pin_title = link["srcPinTitle"].asString();
    auto src_output_pin_index =
        src_node_creator->get_pin_output_index(src_pin_title);
    auto *dst_node_creator = get_node_creator(dst_node_ref);
    auto dst_pin_title = link["dstPinTitle"].asString();
    auto dst_input_pin_index =
        dst_node_creator->get_pin_input_index(dst_pin_title);
    pair_vec.emplace_back(
        LinkNodePair{PlatoNodeID(src_node_id), src_output_pin_index,
                     PlatoNodeID(dst_node_id), dst_input_pin_index});
  }
  flow_link_node_pair_vector_.emplace_back(pair_vec);
  return true;
}

auto PlatoStagePreBuilderJson::get_node_creator(Json::Value &node)
    -> PlatoNodeCreator * {
  if (!node.isMember("title") || !node.isMember("id")) {
    throw std::runtime_error("Invalid JSON format");
  }
  auto *node_creator =
      get_node_factory()->get_creator(node["title"].asString());
  if (node_creator) {
    return node_creator;
  } else {
    auto *stage_node_factory = get_node_factory(stage_name_.c_str());
    if (stage_node_factory) {
      return stage_node_factory->get_creator(PlatoNodeID(node["id"].asUInt()));
    }
  }
  return nullptr;
}

auto PlatoStagePreBuilderJson::merge_flow() -> void {
  for (std::size_t i = 0; i < node_id_set_vector_.size(); i++) {
    for (std::size_t j = 0; j < node_id_set_vector_.size(); j++) {
      if ((i == j) || node_id_set_vector_[i].empty()) {
        continue;
      }
      auto &cur_node_id_set = node_id_set_vector_[i];
      auto &next_node_id_set = node_id_set_vector_[j];
      auto &cur_link_list = flow_link_vector_[i];
      auto &next_link_list = flow_link_vector_[j];
      for (auto &id : cur_node_id_set) {
        if (next_node_id_set.find(id) != next_node_id_set.end()) {
          //
          // 发现一个节点相同则整个集合合并
          //
          cur_node_id_set.insert(next_node_id_set.begin(),
                                 next_node_id_set.end());
          cur_link_list.insert(cur_link_list.begin(), next_link_list.begin(),
                               next_link_list.end());
          next_node_id_set.clear();
          next_link_list.clear();
          break;
        }
      }
    }
  }
  remove_empty_set();
}

auto PlatoStagePreBuilderJson::remove_empty_set() -> void {
  // 删除所有空集合
  node_id_set_vector_.erase(std::remove_if(node_id_set_vector_.begin(),
                                           node_id_set_vector_.end(),
                                           [](const NodeIDSet &node_id_set) {
                                             return node_id_set.empty();
                                           }),
                            node_id_set_vector_.end());
  // 删除所有空链表
  flow_link_vector_.erase(std::remove_if(flow_link_vector_.begin(),
                                         flow_link_vector_.end(),
                                         [](const LinkList &link_list) {
                                           return link_list.empty();
                                         }),
                          flow_link_vector_.end());
}

auto PlatoStagePreBuilderJson::pick_link_and_node(LinkList &unk_link_list,
                                                  NodeIDSet &unk_node_id_set)
    -> void {
  while (!unk_link_list.empty()) {
    NodeIDSet dst_id_set;
    LinkList dst_link_list;
    // 放入集合一条连线及节点
    insert_and_remove_link(unk_link_list.begin(), unk_link_list,
                           unk_node_id_set, dst_id_set, dst_link_list);
    // 找到一条流
    for (auto link_it = unk_link_list.begin();
         link_it != unk_link_list.end();) {
      auto &link_ref = **link_it;
      if (!link_ref.isMember("srcNodeId") || !link_ref.isMember("dstNodeId")) {
        throw std::runtime_error("Invalid JSON format");
      }
      auto src_node_id = PlatoNodeID(link_ref["srcNodeId"].asUInt());
      auto dst_node_id = PlatoNodeID(link_ref["dstNodeId"].asUInt());
      if (check_exists(dst_id_set, src_node_id, dst_node_id)) {
        link_it = insert_and_remove_link(
            link_it, unk_link_list, unk_node_id_set, dst_id_set, dst_link_list);
      } else {
        link_it++;
      }
    }
    node_id_set_vector_.emplace_back(dst_id_set);
    flow_link_vector_.emplace_back(dst_link_list);
  }
}

auto PlatoStagePreBuilderJson::merge_unknow_link_and_node(
    Json::Value &root, LinkList &unk_link_list, NodeIDSet &unk_node_id_set)
    -> void { // 搜集所有连线和节点
  for (auto &link : root["links"]) {
    if (!link.isMember("srcNodeId") || !link.isMember("dstNodeId")) {
      throw std::runtime_error("Invalid JSON format");
    }
    // 连线
    unk_link_list.push_back(&link);
    // 节点
    unk_node_id_set.emplace(PlatoNodeID(link["srcNodeId"].asUInt()));
    unk_node_id_set.emplace(PlatoNodeID(link["dstNodeId"].asUInt()));
  }
}

auto PlatoStagePreBuilderJson::cleanup() -> void {
  node_id_set_vector_.clear();
  flow_link_vector_.clear();
  node_id_map_.clear();
  root_.reset();
}

auto plato::PlatoStagePreBuilderJson::build_stage_name(const std::string &src)
    -> std::string {
  if (src.empty()) {
    return src;
  }
  std::vector<std::string> result;
  std::string last;
  split(src, "/", result);
  if (result.size() == 1) {
    split(src, "\\", result);
    if (result.size() == 1) {
      last = src;
    } else {
      last = result.back();
    }
  } else {
    last = result.back();
  }
  result.clear();
  split(last, ".", result);
  if (result.size() == 1) {
    return last;
  }
  return result.front();
}

auto plato::PlatoStagePreBuilderJson::get_error() -> std::string {
  return ("Stage '" + stage_name_ + "', " + error_);
}

auto PlatoStagePreBuilderJson::set_error(const std::string &error) -> void {
  error_ = error;
}

auto plato::PlatoStagePreBuilderJson::get_config_file() -> const std::string & {
  return config_file_;
}

auto PlatoStagePreBuilderJson::get_node_info_vec()
    -> const FlowNodeInfoVector & {
  return flow_node_info_array_;
}

auto PlatoStagePreBuilderJson::get_node_pair_vec()
    -> const FlowLinkNodePairVector & {
  return flow_link_node_pair_vector_;
}

auto PlatoStagePreBuilderJson::get_stage_node_factory() -> PlatoNodeFactory * {
  return stage_node_factory_;
}

auto PlatoStagePreBuilderJson::get_pre_var_builder()
    -> PlatoVariablePreBuilder * {
  return pre_var_builder_.get();
}

auto PlatoStagePreBuilderJson::insert_and_remove_link(
    LinkList::iterator source_it, LinkList &src_link_list,
    NodeIDSet &src_node_id_set, NodeIDSet &dst_node_id_set,
    LinkList &dst_link_list) -> LinkList::iterator {
  auto &link_ref = **source_it;
  if (!link_ref.isMember("srcNodeId") || !link_ref.isMember("dstNodeId")) {
    throw std::runtime_error("Invalid JSON format");
  }
  auto src_id = PlatoNodeID(link_ref["srcNodeId"].asUInt());
  auto dst_id = PlatoNodeID(link_ref["dstNodeId"].asUInt());
  dst_node_id_set.emplace(src_id);
  dst_node_id_set.emplace(dst_id);
  dst_link_list.emplace_back(*source_it);
  src_node_id_set.erase(src_id);
  src_node_id_set.erase(dst_id);
  return src_link_list.erase(source_it);
}

auto PlatoStagePreBuilderJson::check_exists(const NodeIDSet &id_set,
                                            PlatoNodeID src_id,
                                            PlatoNodeID dst_id) -> bool {
  return (id_set.find(src_id) != id_set.end() ||
          id_set.find(dst_id) != id_set.end());
}

void PlatoStagePreBuilderJson::split(const std::string &src,
                                     const std::string &delim,
                                     std::vector<std::string> &result) {
  result.clear();
  if (src.empty() || delim.empty()) {
    return;
  }
  size_t end = 0;
  size_t begin = 0;
  std::string substr;
  while (end != std::string::npos) {
    end = src.find(delim, begin);
    if (end != std::string::npos) {
      substr = src.substr(begin, end - begin);
    } else {
      substr = src.substr(begin, std::string::npos);
    }
    if (!substr.empty() && (substr != delim)) {
      result.push_back(substr);
    }
    begin = end + delim.size();
  }
}

auto getPlatoStagePreBuilder(const std::string &config_file)
    -> PlatoStagePreBuilderPtr {
  namespace fs = std::filesystem;
  auto ext = fs::path(config_file).extension();
  if (ext.string() == ".plato") {
    return std::make_unique<PlatoStagePreBuilderJson>(config_file);
  }
  return nullptr;
}

} // namespace plato
