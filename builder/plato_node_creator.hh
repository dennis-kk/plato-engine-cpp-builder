#pragma once

#include "../core/plato_stage.hh"
#include "../core/plato_variable.hh"

namespace plato {

/**
 * @brief 节点创建器接口
 */
struct PlatoNodeCreator {
  /**
   * @brief 析构
   */
  virtual ~PlatoNodeCreator() {}
  /**
   * @brief 创建节点
   * @param domain_ptr 变量域
   * @param id 节点ID
   * @return 节点实例
   */
  virtual auto create(DomainPtr domain_ptr, PlatoNodeID id) -> PlatoNodePtr = 0;
  /**
   * @brief 获取创建器对应节点的静态ID
   * @return 节点的静态ID
   */
  virtual auto static_id() -> PlatoNodeID = 0;
  /**
   * @brief 根据引脚名字获取输入引脚对应的索引
   * @param pin_name 引脚名字
   * @return 引脚对应的索引
   */
  virtual auto get_pin_input_index(const std::string &pin_name)
      -> PlatoPinIndex = 0;
  /**
   * @brief 根据引脚名字获取输出引脚对应的索引
   * @param pin_name 引脚名字
   * @return 引脚对应的索引
   */
  virtual auto get_pin_output_index(const std::string &pin_name)
      -> PlatoPinIndex = 0;
  /**
   * @brief 获取节点实例类占用的内存占用量
   * @return 节点实例类占用的内存占用量
   */
  virtual auto get_node_memory_size() -> std::size_t = 0;
};

} // namespace plato
