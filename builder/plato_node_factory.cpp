#include "plato_node_factory.hh"
#include "plato_node_creator.hh"
#include <string>
#include <unordered_map>

namespace plato {

/**
 * @brief 节点工厂实现类
 */
class PlatoNodeFactoryImpl : public PlatoNodeFactory {
  using NodeProtoTypeMap = std::unordered_map<std::string, PlatoNodeCreator *>;
  using NodeIDProtoTypeMap =
      std::unordered_map<PlatoNodeID, PlatoNodeCreator *>;
  NodeProtoTypeMap node_pt_map_;      ///< {节点名，节点创建器}
  NodeIDProtoTypeMap node_id_pt_map_; ///< {节点ID, 节点创建器}

public:
  PlatoNodeFactoryImpl() {}
  virtual ~PlatoNodeFactoryImpl() {}
  /**
   * @brief 创建节点实例
   * @param name 节点名
   * @param domain_ptr 变量域
   * @param id 变量ID
   * @return 节点实例
   */
  virtual auto create(const std::string &name, DomainPtr domain_ptr,
                      PlatoNodeID id) -> PlatoNodePtr override {
    auto it = node_pt_map_.find(name);
    if (it == node_pt_map_.end()) {
      return nullptr;
    }
    return it->second->create(domain_ptr, id);
  }
  /**
   * @brief 创建节点实例
   * @param domain_ptr  变量域
   * @param id 变量ID
   * @return 节点实例
   */
  virtual auto create(DomainPtr domain_ptr, PlatoNodeID id)
      -> PlatoNodePtr override {
    auto it = node_id_pt_map_.find(id);
    if (it == node_id_pt_map_.end()) {
      return nullptr;
    }
    return it->second->create(domain_ptr, id);
  }
  /**
   * @brief 获取节点创建器
   * @param name 节点名
   * @return 节点创建器
   */
  virtual auto get_creator(const std::string &name)
      -> PlatoNodeCreator * override {
    auto it = node_pt_map_.find(name);
    if (it == node_pt_map_.end()) {
      return nullptr;
    }
    return it->second;
  }
  virtual auto get_creator(PlatoNodeID id) -> PlatoNodeCreator * override {
    auto it = node_id_pt_map_.find(id);
    if (it == node_id_pt_map_.end()) {
      return nullptr;
    }
    return it->second;
  }

public:
  /**
   * @brief 添加节点创建器
   * @param name 节点名字
   * @param creator 节点创建器
   * @return true成功, false失败
   */
  auto add_node_creator(const char *name, PlatoNodeCreator *creator) -> bool {
    if (node_pt_map_.find(name) != node_pt_map_.end()) {
      return false;
    }
    node_pt_map_[name] = creator;
    return true;
  }
  /**
   * @brief 添加节点创建器
   * @param node_id 节点ID
   * @param creator 节点创建器
   * @return true成功, false失败
   */
  auto add_node_creator(PlatoNodeID node_id, PlatoNodeCreator *creator)
      -> bool {
    if (node_id_pt_map_.find(node_id) != node_id_pt_map_.end()) {
      return false;
    }
    node_id_pt_map_[node_id] = creator;
    return true;
  }
};

} // namespace plato

static std::unique_ptr<plato::PlatoNodeFactoryImpl> global_node_factory_;

/**
 * @brief 添加节点创建器到全局管理器
 * @param name 节点名
 * @param creator 创建器
 * @return true成功, false失败
 */
auto add_node_creator(const char *name, plato::PlatoNodeCreator *creator)
    -> bool {
  if (!global_node_factory_) {
    global_node_factory_.reset(new plato::PlatoNodeFactoryImpl());
  }
  return global_node_factory_->add_node_creator(name, creator);
}

/**
 * @brief 添加节点创建器到局部工厂, 每个舞台有一个局部工厂
 * @param factory 节点工厂
 * @param node_id 节点ID
 * @param creator 创建器
 * @return true成功, false失败
 */
auto add_node_creator(plato::PlatoNodeFactory *factory,
                      plato::PlatoNodeID node_id,
                      plato::PlatoNodeCreator *creator) -> bool {
  try {
    auto *factory_imp_pointer =
        dynamic_cast<plato::PlatoNodeFactoryImpl *>(factory);
    if (!factory_imp_pointer) {
      return false;
    }
    return factory_imp_pointer->add_node_creator(node_id, creator);
  } catch (...) {
    return false;
  }
}

using VarCreatorFuncMap = std::unordered_map<std::string, VarCreatorFunc>;
static VarCreatorFuncMap var_creator_func_map_; ///< {舞台名, 函数指针}
using VarCreatorSizeFuncMap =
    std::unordered_map<std::string, VarCreatorSizeFunc>;
static VarCreatorSizeFuncMap var_size_func_map_; ///< {舞台名, 函数指针}
using VarNameQueryFuncMap = std::unordered_map<std::string, VarNameQueryFunc>;
static VarNameQueryFuncMap var_name_query_func_map_; ///< {舞台名, 函数指针}

/**
 * @brief 添加变量名查询函数到管理器
 * @param name 舞台名
 * @param func 查询函数指针
 * @return true成功, false失败
 */
auto add_var_name_query(const char *name, VarNameQueryFunc func) -> bool {
  if (var_name_query_func_map_.find(name) != var_name_query_func_map_.end()) {
    return false;
  }
  var_name_query_func_map_[name] = func;
  return true;
}
/**
 * @brief 添加变量创建/占用查询函数
 * @param name 舞台名
 * @param func 变量创建函数指针
 * @param size_func 占用查询函数指针
 * @return true成功, false失败
 */
auto add_var_creator(const char *name, VarCreatorFunc func,
                     VarCreatorSizeFunc size_func) -> bool {
  if (var_creator_func_map_.find(name) != var_creator_func_map_.end()) {
    return false;
  }
  var_creator_func_map_[name] = func;
  var_size_func_map_[name] = size_func;
  return true;
}
/**
 * @brief 获取变量创建函数
 * @param name 舞台名
 * @return 变量创建函数指针
 */
auto get_var_creator(const char *name) -> VarCreatorFunc {
  auto it = var_creator_func_map_.find(name);
  if (it == var_creator_func_map_.end()) {
    return nullptr;
  }
  return it->second;
}
/**
 * @brief 获取变量占用查询函数
 * @param name 舞台名
 * @return 变量占用查询函数指针
 */
auto get_var_size_query(const char *name) -> VarCreatorSizeFunc {
  auto it = var_size_func_map_.find(name);
  if (it == var_size_func_map_.end()) {
    return nullptr;
  }
  return it->second;
}
/**
 * @brief 获取变量名查询函数
 * @param name 舞台名
 * @return 变量名查询函数指针
 */
auto get_var_name_query(const char *name) -> VarNameQueryFunc {
  auto it = var_name_query_func_map_.find(name);
  if (it == var_name_query_func_map_.end()) {
    return nullptr;
  }
  return it->second;
}
/**
 * @brief 获取全局节点工厂
 * @return 全局节点工厂指针
 */
auto get_node_factory() -> plato::PlatoNodeFactory * {
  if (!global_node_factory_) {
    global_node_factory_.reset(new plato::PlatoNodeFactoryImpl());
  }
  return global_node_factory_.get();
}

using PlatoNodeFactoryMap =
    std::unordered_map<std::string,
                       std::unique_ptr<plato::PlatoNodeFactoryImpl>>;
static PlatoNodeFactoryMap factory_map_; ///< {舞台名，工厂指针}
/**
 * @brief 获取舞台节点工厂
 * @param name 舞台名
 * @return 舞台节点工厂指针
 */
auto get_node_factory(const char *name) -> plato::PlatoNodeFactory * {
  auto it = factory_map_.find(name);
  if (it == factory_map_.end()) {
    auto *factory_pointer = new plato::PlatoNodeFactoryImpl();
    factory_map_.emplace(name, factory_pointer);
    return factory_pointer;
  }
  return it->second.get();
}

using StructCreator = plato::VariablePtr (*)(plato::Domain *domain,
                                             plato::VarID,
                                             plato::PlatoVariableSyncType);

using StructCreatorMap = std::unordered_map<std::string, StructCreator>;
static StructCreatorMap struct_creator_map_; ///< 结构体变量创建函数表

auto add_struct_creator(const std::string &name, StructCreator creator)
    -> bool {
  if (struct_creator_map_.find(name) != struct_creator_map_.end()) {
    return false;
  }
  struct_creator_map_[name] = creator;
  return true;
}

auto create_struct(const std::string &name, plato::Domain *domain,
                   plato::VarID var_id, plato::PlatoVariableSyncType sync_type)
    -> plato::VariablePtr {
  auto it = struct_creator_map_.find(name);
  if (it == struct_creator_map_.end()) {
    return nullptr;
  }
  return it->second(domain, var_id, sync_type);
}

using StructSizeMap = std::unordered_map<std::string, std::size_t>;
static StructSizeMap struct_size_map_; ///< struct内存占用表

auto add_struct_size(const std::string &name, std::size_t size) -> bool {
  if (struct_size_map_.find(name) != struct_size_map_.end()) {
    return false;
  }
  struct_size_map_[name] = size;
  return true;
}

auto get_struct_size(const std::string &name) -> std::size_t {
  if (struct_size_map_.find(name) == struct_size_map_.end()) {
    return 0;
  }
  return struct_size_map_[name];
}
