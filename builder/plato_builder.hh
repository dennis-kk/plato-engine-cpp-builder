#pragma once

#include "../core/plato_stage.hh"
#include "plato_node_factory.hh"
#include "plato_pre_builder.hh"
#include "thirdparty/jsoncpp/include/json/json.h"

#include <any>
#include <list>
#include <memory>
#include <vector>

namespace Json {
class Value;
}

namespace plato {

struct PlatoNodeCreator;
class PlatoNodeFactory;

/**
 * @brief 舞台建造器实现类
 */
class PlatoStageBuilderImpl : public PlatoStageBuilder {
  //
  // 创建舞台内流所用数据
  //
  PlatoStageID stage_id_{PlatoStageID(1)};            ///< 舞台ID
  std::unique_ptr<PlatoStagePreBuilder> pre_builder_; ///< 预创建器

public:
  /**
   * @brief 构造
   */
  PlatoStageBuilderImpl();
  /**
   * @brief 析构
   */
  virtual ~PlatoStageBuilderImpl();
  virtual auto pre_build(const std::string &config_file) -> bool override;
  virtual auto build(PlatoStageID stage_id, FlowRunMode run_mode)
      -> PlatoStagePtr override;
  virtual auto get_error() -> std::string override;
  virtual auto get_config_file() -> const std::string & override;

protected:
  /**
   * @brief 创建舞台,快速模式
   * @param stage_ptr 舞台实例
   * @return true成功,false失败
   */
  auto build_stage(PlatoStagePtr stage_ptr) -> bool;
  /**
   * @brief 建立所有变量实例，快速模式
   * @param stage_ptr 舞台实例
   * @return true成功,false失败
   */
  auto build_variables(PlatoStagePtr stage_ptr) -> bool;
  /**
   * @brief 建立一条流, 快速模式
   * @param flow_ptr 流实例
   * @return true成功,false失败
   */
  auto build_flow(PlatoFlowPtr flow_ptr, const NodeInfoVector &node_info_vec,
                  const LinkNodePairVector &node_pair_vec) -> bool;
  /**
   * @brief 将节点全部加入到流内，快速模式
   * @param flow_ptr 流实例
   * @return true成功,false失败
   */
  auto build_flow_nodes(PlatoFlowPtr flow_ptr,
                        const NodeInfoVector &node_info_vec) -> bool;
  /**
   * @brief 将连线加入流，快速模式
   * @param flow_ptr 流实例
   * @return true成功,false失败
   */
  auto build_flow_links(PlatoFlowPtr flow_ptr,
                        const LinkNodePairVector &node_pair_vec) -> bool;
};

} // namespace plato

using StageBuilderPtr = std::shared_ptr<plato::PlatoStageBuilder>;

/**
 * @brief 获取舞台创建器
 * @param config_file 配置文件
 * @return 舞台创建器
*/
extern auto get_builder(const std::string &config_file) -> StageBuilderPtr;
