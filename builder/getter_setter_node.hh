#pragma once

#include "../core/plato_stage.hh"
#include "plato_node_creator.hh"
#include "plato_node_factory.hh"

namespace plato {

/**
 * @brief Getter,Setter节点创建器接口
 */
class GetterSetterNodeCreator : public PlatoNodeCreator {
  PlatoNodeID static_id_{INVALID_NODE_ID}; ///< 节点静态ID
  VarID var_id_{INVALID_VAR_ID};           ///< 绑定的变量ID

public:
  virtual ~GetterSetterNodeCreator() {}
  /**
   * @brief 添加绑定变量
   * @param name 变量名
   * @param var_id 变量ID
   * @return
   */
  virtual auto add_var_pin(const std::string &name, VarID var_id) -> void = 0;
  /**
   * @brief 设置节点静态ID
   * @param static_id 节点静态ID
   * @return
   */
  auto set_static_id(PlatoNodeID static_id) -> void;
  /**
   * @brief 获取节点静态ID
   * @return 节点静态ID
   */
  auto get_static_id() -> PlatoNodeID;

protected:
  /**
   * @brief 设置变量ID
   * @param var_id 变量ID
   * @return
   */
  auto set_var_id(VarID var_id) -> void;
  /**
   * @brief 获取变量ID
   * @return 变量ID
   */
  auto get_var_id() -> VarID;
};

/**
 * @brief Getter节点创建器
 */
struct GetterCreator : public GetterSetterNodeCreator {
  using PinNameIndexMap = std::unordered_map<std::string, PlatoPinIndex>;
  PinNameIndexMap
      pin_name_output_index_map_; ///< Getter节点输出引脚，{引脚名, 引脚索引},
                                  ///< Getter节点没有输入引脚
  PlatoNodeSyncType sync_type_{PlatoNodeSyncType::NONE}; ///< 节点同步类型

public:
  GetterCreator();
  virtual ~GetterCreator();
  virtual auto static_id() -> PlatoNodeID override;
  virtual auto get_pin_input_index(const std::string &pin_name)
      -> PlatoPinIndex override;
  virtual auto get_pin_output_index(const std::string &pin_name)
      -> PlatoPinIndex override;
  virtual auto get_node_memory_size() -> std::size_t override;
  virtual auto create(DomainPtr domain_ptr, PlatoNodeID id)
      -> PlatoNodePtr override;
  virtual auto add_var_pin(const std::string &name, VarID var_id)
      -> void override;
  /**
   * @brief 获取节点内存占用大小，方便建造器不建立实例也可以获取
   * @return 节点内存占用大小
   */
  static auto get_fixed_memory_size() -> std::size_t;
};

/**
 * @brief Setter节点创建器
 */
struct SetterCreator : public GetterSetterNodeCreator {
  using PinNameIndexMap = std::unordered_map<std::string, PlatoPinIndex>;
  PinNameIndexMap
      pin_name_input_index_map_; ///< Setter节点输入引脚，{引脚名, 引脚索引}
  PinNameIndexMap
      pin_name_output_index_map_; ///< Setter节点输出引脚，{引脚名, 引脚索引}
  PlatoNodeSyncType sync_type_{PlatoNodeSyncType::NONE}; ///< 节点同步类型

public:
  SetterCreator();
  virtual ~SetterCreator();
  virtual auto static_id() -> PlatoNodeID override;
  virtual auto get_pin_input_index(const std::string &pin_name)
      -> PlatoPinIndex override;
  virtual auto get_pin_output_index(const std::string &pin_name)
      -> PlatoPinIndex override;
  virtual auto get_node_memory_size() -> std::size_t override;
  virtual auto create(DomainPtr domain_ptr, PlatoNodeID id)
      -> PlatoNodePtr override;
  virtual auto add_var_pin(const std::string &name, VarID var_id)
      -> void override;
  /**
   * @brief 获取节点内存占用大小，方便建造器不建立实例也可以获取
   * @return 节点内存占用大小
   */
  static auto get_fixed_memory_size() -> std::size_t;
};

} // namespace plato
