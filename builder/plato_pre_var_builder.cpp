#include "plato_pre_var_builder.hh"
#include "plato_pre_builder.hh"

namespace plato {

/**
 * @brief 设置变量值
 * @tparam BasicTypeT 基础类型
 * @tparam VarTypeT 变量类型
 * @param var_ptr 变量指针
 * @param default_value 默认值
 * @return
 */
template <typename BasicTypeT, typename VarTypeT>
inline auto var_cast(plato::VariablePtr var_ptr, const std::any &default_value)
    -> void {
  return std::dynamic_pointer_cast<VarTypeT>(var_ptr)->set_default(
      std::any_cast<BasicTypeT>(default_value));
}

using VariableSizeMap = std::unordered_map<TypeID, std::size_t>;

/**
 * @brief 基础类型内存占用表
 */
static VariableSizeMap basic_var_size_map_ = {
    {TypeID(PlatoType::BOOL), sizeof(Bool)},
    {TypeID(PlatoType::STRING), sizeof(String)},
    {TypeID(PlatoType::FLOAT), sizeof(Float)},
    {TypeID(PlatoType::DOUBLE), sizeof(Double)},
    {TypeID(PlatoType::INT32), sizeof(Int32)},
    {TypeID(PlatoType::UINT32), sizeof(Uint32)},
    {TypeID(PlatoType::INT64), sizeof(Int64)},
    {TypeID(PlatoType::UINT64), sizeof(Uint64)},
};
/**
 * @brief Set类型内存占用表
 */
static VariableSizeMap basic_var_set_size_map_ = {
    {TypeID(PlatoType::BOOL), sizeof(Set<bool>)},
    {TypeID(PlatoType::STRING), sizeof(Set<std::string>)},
    {TypeID(PlatoType::FLOAT), sizeof(Set<float>)},
    {TypeID(PlatoType::DOUBLE), sizeof(Set<double>)},
    {TypeID(PlatoType::INT32), sizeof(Set<std::int32_t>)},
    {TypeID(PlatoType::UINT32), sizeof(Set<std::uint32_t>)},
    {TypeID(PlatoType::INT64), sizeof(Set<std::int64_t>)},
    {TypeID(PlatoType::UINT64), sizeof(Set<std::uint64_t>)},
};
/**
 * @brief Map类型内存占用表(基础类型)
 */
static VariableSizeMap basic_var_map_size_map_ = {
    {TypeID(PlatoType::BOOL), sizeof(Map<bool, Bool>)},
    {TypeID(PlatoType::STRING), sizeof(Map<std::string, Bool>)},
    {TypeID(PlatoType::FLOAT), sizeof(Map<float, Bool>)},
    {TypeID(PlatoType::DOUBLE), sizeof(Map<double, Bool>)},
    {TypeID(PlatoType::INT32), sizeof(Map<std::int32_t, Bool>)},
    {TypeID(PlatoType::INT64), sizeof(Map<std::int64_t, Bool>)},
    {TypeID(PlatoType::UINT32), sizeof(Map<std::uint32_t, Bool>)},
    {TypeID(PlatoType::UINT64), sizeof(Map<std::uint64_t, Bool>)},
};

PlatoVariablePreBuilderJson::PlatoVariablePreBuilderJson(
    PlatoStagePreBuilder *pre_builder) {
  pre_builder_ = pre_builder;
}

PlatoVariablePreBuilderJson::~PlatoVariablePreBuilderJson() {}

auto PlatoVariablePreBuilderJson::build(Json::Value &root)
    -> bool { // 注册变量相关基础设施
  register_variable_infrastructure();
  // 建立变量
  if (!pre_build_variables(root)) {
    return false;
  }
  return true;
}

auto PlatoVariablePreBuilderJson::get_max_var_reserved_id() -> VarID {
  return max_reserved_id_;
}

auto PlatoVariablePreBuilderJson::get_stage_var_id_vec() -> const VarIDVector & {
  return stage_var_id_vector_;
}

auto PlatoVariablePreBuilderJson::get_stage_var_creator_func() -> VarCreatorFunc {
  return stage_var_create_func_;
}

auto PlatoVariablePreBuilderJson::set_basic_default_value(const VarInfo &var_info,
                                                      VariablePtr var_ptr)
    -> void {
  switch (var_info.type) {
  case PlatoType::BOOL:
    var_cast<bool, Bool>(var_ptr, var_info.default_value);
    break;
  case PlatoType::STRING:
    var_cast<std::string, String>(var_ptr, var_info.default_value);
    break;
  case PlatoType::FLOAT:
    var_cast<float, Float>(var_ptr, var_info.default_value);
    break;
  case PlatoType::DOUBLE:
    var_cast<double, Double>(var_ptr, var_info.default_value);
    break;
  case PlatoType::INT32:
    var_cast<std::int32_t, Int32>(var_ptr, var_info.default_value);
    break;
  case PlatoType::INT64:
    var_cast<std::int64_t, Int64>(var_ptr, var_info.default_value);
    break;
  case PlatoType::UINT32:
    var_cast<std::uint32_t, Uint32>(var_ptr, var_info.default_value);
    break;
  case PlatoType::UINT64:
    var_cast<std::uint64_t, Uint64>(var_ptr, var_info.default_value);
    break;
  default:
    break;
  }
}

auto PlatoVariablePreBuilderJson::query_variable_size(plato::VarID var_id)
    -> std::size_t {
  auto it = var_size_map_.find(var_id);
  if (it == var_size_map_.end()) {
    return 0;
  }
  return it->second;
}

auto PlatoVariablePreBuilderJson::get_variable_name(plato::VarID var_id)
    -> const std::string & {
  static std::string NullString;
  auto it = var_name_map_.find(var_id);
  if (it == var_name_map_.end()) {
    return NullString;
  }
  return it->second;
}

/**
 * @brief {类型名，类型ID}
 */
static std::unordered_map<std::string, plato::PlatoType> query_type_map_ = {
    {"bool", plato::PlatoType::BOOL},     {"string", plato::PlatoType::STRING},
    {"float", plato::PlatoType::FLOAT},   {"double", plato::PlatoType::DOUBLE},
    {"int32", plato::PlatoType::INT32},   {"int64", plato::PlatoType::INT64},
    {"uint32", plato::PlatoType::UINT32}, {"uint64", plato::PlatoType::UINT64},
    {"dict", plato::PlatoType::MAP},      {"seq", plato::PlatoType::ARRAY},
    {"set", plato::PlatoType::SET},
};

auto PlatoVariablePreBuilderJson::get_variable_type(const std::string &type_name)
    -> PlatoType {
  auto it = query_type_map_.find(type_name);
  if (it == query_type_map_.end()) {
    return PlatoType::STRUCT;
  }
  return it->second;
}

auto PlatoVariablePreBuilderJson::get_variable_size(const Json::Value &var)
    -> std::size_t {
  if (!var.isMember("type")) {
    return 0;
  }
  auto var_type = var["type"].asString();
  auto type = get_variable_type(var_type);
  switch (type) {
  case PlatoType::SET: {
    auto key_type = get_variable_type(var["key"].asString());
    return basic_var_set_size_map_[TypeID(key_type)];
  }
  case PlatoType::MAP: {
    auto key_type = get_variable_type(var["key"].asString());
    return basic_var_map_size_map_[TypeID(key_type)];
  }
  case PlatoType::ARRAY: {
    // 数组内含智能指针，类实例的内存占用量相同
    return sizeof(Array<Bool>);
  }
  case PlatoType::STRUCT:
    return get_struct_size(var["type"].asString());
  default:
    return basic_var_size_map_[TypeID(type)];
  }
}

auto PlatoVariablePreBuilderJson::register_variable_infrastructure() -> void {
  // 添加变量创建器、变量大小查询函数
  add_var_creator(pre_builder_->get_stage_name().c_str(),
                  std::bind(&PlatoVariablePreBuilderJson::create_variable, this,
                            std::placeholders::_1, std::placeholders::_2,
                            std::placeholders::_3),
                  std::bind(&PlatoVariablePreBuilderJson::query_variable_size, this,
                            std::placeholders::_1));
  // 变量名字查询函数
  add_var_name_query(pre_builder_->get_stage_name().c_str(),
                     std::bind(&PlatoVariablePreBuilderJson::get_variable_name, this,
                               std::placeholders::_1));
}

auto PlatoVariablePreBuilderJson::get_variable_size(Json::Value &root)
    -> std::size_t {
  // 获取变量内存占用查询函数指针
  auto var_size_query_func =
      get_var_size_query(pre_builder_->get_stage_name().c_str());
  if (!var_size_query_func) {
    return 0;
  }
  std::size_t total_size = 0;
  if (root.isMember("variables")) {
    // 获取舞台所有变量内存占用
    for (auto &var : root["variables"]) {
      if (!var.isMember("id")) {
        throw std::runtime_error("Invalid JSON format");
      }
      auto var_id = VarID(var["id"].asUInt());
      // 获取变量内存占用量
      auto size = var_size_query_func(var_id);
      if (!size) {
        throw std::runtime_error("Unknown variable ID '" +
                                 std::to_string(var_id) + "'");
      }
      total_size += size;
    }
  }
  return total_size;
}

auto PlatoVariablePreBuilderJson::pre_build_variables(Json::Value &root) -> bool {
  // 获取变量创建器
  stage_var_create_func_ =
      get_var_creator(pre_builder_->get_stage_name().c_str());
  if (!stage_var_create_func_) {
    // 变量创建函数未找到
    pre_builder_->set_error("Variable creator function not found");
    return true;
  }
  // 建立所有变量信息
  return collect_var_infos(root);
}

auto PlatoVariablePreBuilderJson::collect_var_infos(Json::Value &root) -> bool {
  for (const auto &var : root["variables"]) {
    if (!var.isMember("id") || !var.isMember("title") ||
        !var.isMember("type")) {
      throw std::runtime_error("Invalid JSON format");
    }
    // 变量ID
    auto var_id = VarID(var["id"].asUInt());
    // 记录变量ID
    stage_var_id_vector_.emplace_back(var_id);
    // 变量信息
    VarInfo var_info;
    var_info.var_id = var_id;
    var_info.name = var["title"].asString();
    var_info.type = get_variable_type(var["type"].asString());
    switch (var_info.type) {
    case PlatoType::ARRAY:
      var_info.key_type = get_variable_type(var["key"].asString());
      if (var_info.key_type == PlatoType::STRUCT) {
        // struct记录下类型名
        var_info.type_name = var["key"].asString();
      }
      break;
    case PlatoType::SET:
      var_info.key_type = get_variable_type(var["key"].asString());
      break;
    case PlatoType::MAP:
      var_info.key_type = get_variable_type(var["key"].asString());
      var_info.value_type = get_variable_type(var["value"].asString());
      if (var_info.value_type == PlatoType::STRUCT) {
        var_info.type_name = var["value"].asString();
      }
      break;
    case PlatoType::STRUCT:
      var_info.type_name = var["type"].asString();
      break;
    //
    // 以下基础类型需要设置默认值
    //
    case PlatoType::BOOL:
      var_info.default_value = var["default"].asBool();
      break;
    case PlatoType::STRING:
      var_info.default_value = var["default"].asString();
      break;
    case PlatoType::FLOAT:
      var_info.default_value = var["default"].asFloat();
      break;
    case PlatoType::DOUBLE:
      var_info.default_value = var["default"].asDouble();
      break;
    case PlatoType::INT32:
      var_info.default_value = var["default"].asInt();
      break;
    case PlatoType::INT64:
      var_info.default_value = var["default"].asInt64();
      break;
    case PlatoType::UINT32:
      var_info.default_value = var["default"].asUInt();
      break;
    case PlatoType::UINT64:
      var_info.default_value = var["default"].asUInt64();
      break;
    }
    if (var_id > max_reserved_id_) {
      max_reserved_id_ = var_id;
    }
    auto var_size = get_variable_size(var);
    var_name_map_.emplace(var_id, var_info.name);
    var_size_map_.emplace(var_id, var_size);
    var_info_map_.emplace(var_id, var_info);
  }
  return true;
}

auto PlatoVariablePreBuilderJson::create_variable(
    plato::VarID var_id, DomainPtr domain_ptr,
    plato::PlatoVariableSyncType sync_type) -> VariablePtr {
  // 建立变量
  auto it = var_info_map_.find(var_id);
  if (it == var_info_map_.end()) {
    return nullptr;
  }
  auto &var_info = it->second;
  VariablePtr var_ptr;
  switch (var_info.type) {
  case PlatoType::SET: // Set，只支持基础类型
    var_ptr = domain_ptr->NewNoIdentifiedSet(var_info.key_type, sync_type);
    break;
  case PlatoType::MAP: // Map,
                       // key只支持基础类型，value不支持容器嵌套，但支持struct
    if (var_info.value_type != PlatoType::STRUCT) {
      var_ptr = domain_ptr->NewNoIdentifiedMap(var_info.key_type,
                                               var_info.value_type, sync_type);
    } else {
      var_ptr = domain_ptr->NewNoIdentifiedMapStruct(
          var_info.key_type, var_info.type_name.c_str(), sync_type);
    }
    break;
  case PlatoType::ARRAY: ///< Array, 支持基础类型，不支持容器嵌套，但支持struct
    if (var_info.key_type == PlatoType::STRUCT) {
      var_ptr = domain_ptr->NewNoIdentifiedArrayStruct(
          var_info.type_name.c_str(), sync_type);
    } else {
      var_ptr = domain_ptr->NewNoIdentifiedArray(var_info.key_type, sync_type);
    }
    break;
  case PlatoType::STRUCT: { ///< struct
    var_ptr = create_struct(var_info.type_name, domain_ptr.get(),
                            INVALID_VAR_ID, sync_type);
    break;
  }
  default: { ///< 基础类型
    var_ptr = domain_ptr->NewNoIdentifiedBasic(var_info.type, sync_type);
    // 设置默认值，只有基础类型支持默认值
    if (var_info.default_value.has_value()) {
      set_basic_default_value(var_info, var_ptr);
    }
    break;
  }
  }
  auto impl_ptr = std::dynamic_pointer_cast<VariableImpl>(var_ptr);
  // 设置配置ID
  impl_ptr->set_id(var_id);
  // 加入到变量域，使用配置ID
  domain_ptr->identify(var_ptr);
  return var_ptr;
}

} // namespace plato
