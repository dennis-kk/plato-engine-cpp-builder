import json
import os
import sys
import getopt
import platform

global cwd
cwd = os.getcwd()
global abs_path
abs_path = os.path.abspath(os.path.dirname(__file__)) 
if cwd != abs_path:
    os.chdir(abs_path)

class Builder:
    def __init__(self):
        self.gen()

    def gen(self):
        os.chdir("tools")
        if platform.system() == 'Linux':
            os.system("python gen_node.py -f ../test/plato/plato.json -o ../generated/base")
        elif platform.system() == "Windows":
            os.system("python gen_node.py -f ..\\test\\plato\\plato.json -o ..\\generated\\base")
        else:
            os._exit(1)
        os.chdir("..")
        if platform.system() == "Linux":
            os.system('cmake .')
            os.system('make')
        elif platform.system() == "Windows":
            os.system('cmake -G "Visual Studio 16 2019" -A x64')
            os.system('cmake --build . --config "Release"')
            os.system('cmake --build . --config "Debug"')
        else:
            os._exit(1)
        os.chdir('tools')
        if platform.system() == 'Linux':
            os.system("python gen_logic.py -f ../test/plato/plato.json -o ../logic")
        elif platform.system() == "Windows":
            os.system("python gen_logic.py -f ..\\test\\plato\\plato.json -o ..\\logic")
        else:
            os._exit(1)
        os.chdir('../logic')
        if platform.system() == "Linux":
            os.system('cmake .')
            os.system('make')
        elif platform.system() == "Windows":
            os.system('cmake -G "Visual Studio 16 2019" -A x64')
            os.system('cmake --build . --config "Release"')
            os.system('cmake --build . --config "Debug"')
        else:
            os._exit(1)
        os.chdir('..')

if __name__ == "__main__":
    Builder()