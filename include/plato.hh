#pragma once

#include <cstdint>
#include <memory>
#include <string>

namespace plato {

class PlatoStage;
class PlatoLogger;

/**
 * @brief Plato日志接口
 */
class PlatoLogger {
public:
  virtual ~PlatoLogger() {}
  /**
   * @brief 写入一条日志
   * @param log 日志
   * @return
   */
  virtual auto write(const std::string &log) -> void = 0;
};

/**
 * @brief 流运行模式
 */
enum class FlowRunMode {
  PASSIVE = 1, ///< 等待被远程调用节点逻辑
  ACTIVE,      ///< 主动运行
};
/**
 * @brief 加载节点逻辑
 * @param logic_bundle_path 节点逻辑bundle(dll/so)路径
 * @param error 错误描述
 * @return true成功, false失败
 */
extern auto load_logic_bundle(const std::string &logic_bundle_path,
                              std::string &error) -> bool;
/**
 * @brief 卸载节点逻辑bundle并重新加载
 * @param logic_bundle_path 节点逻辑bundle(dll/so)路径
 * @param error 错误描述
 * @return true成功, false失败
 */
extern auto reload_logic_bundle(const std::string &logic_bundle_path,
                                std::string &error) -> bool;
/**
 * @brief 卸载节点逻辑bundle
 * @return
 */
extern auto unload_logic_bundle() -> void;
/**
 * @brief 预加载plato配置
 * @param stage_conf_file_path plato配置文件路径
 * @param error 错误描述
 * @return true成功, false失败
 */
extern auto pre_build_plato_stage(const std::string &stage_conf_file_path,
                                  std::string &error) -> bool;
/**
 * @brief 建立一个plato舞台
 * @param stage_id 舞台ID
 * @param stage_conf_file_path 舞台配置文件
 * @param run_mode 运行模式
 * @param logger 日志
 * @return 舞台智能指针
 */
extern auto new_plato_stage(std::uint64_t stage_id,
                            const std::string &stage_conf_file_path,
                            FlowRunMode run_mode, PlatoLogger *logger)
    -> std::shared_ptr<PlatoStage>;

} // namespace plato

#include "../core/plato_node.hh"
#include "../core/plato_stage.hh"
#include "../core/plato_stream.hh"
#include "../core/plato_variable.hh"
