#pragma once

#include "plato_variable.hh"

namespace plato {

class Vector3;
using Vector3Ptr = std::shared_ptr<Vector3>;

class Player : public StructVariable {
  Player() = delete;
  Player(const Player &) = delete;
  Player(Player &&) = delete;

public:
  Uint32Ptr health;
  Vector3Ptr pos;

  Player(Domain *domain, VarID parent, PlatoVariableSyncType sync_type);
  virtual ~Player();
  auto static New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type) -> std::shared_ptr<Player>;
  virtual auto serialize(PlatoStream &stream) -> bool override;
  virtual auto deserialize(PlatoStream &stream) -> bool override;
  virtual auto copy(Variable *other) -> void override;
  virtual auto object_size() -> std::size_t override;
  virtual auto copy_default() -> void override;
  virtual auto complete_prototype() -> void override;
  static auto get_fixed_mem_size() -> std::size_t;
};

using ArrayPlayer = ArrayVariable<Player>;
template <typename T> using MapPlayer = MapVariable<T, Player>;
}

