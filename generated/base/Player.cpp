#include "Player.hh"
#include "Vector3.hh"

namespace plato {

Player::Player(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
  : StructVariable(domain, parent, sync_type),
    health(domain->New<Uint32>(sync_type)),
    pos(domain->New<Vector3>(sync_type)) {
  }

Player::~Player() {}

auto Player::New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type) -> std::shared_ptr<Player> {
  return plato::make_shared<Player>(domain->mem_block(), domain, parent, sync_type);
}

auto Player::serialize(PlatoStream &stream) -> bool {
  stream << id();
  health->serialize(stream);
  pos->serialize(stream);
  return true;
}

auto Player::deserialize(PlatoStream &stream) -> bool {
  stream.skip(sizeof(VarID));
  health->deserialize(stream);
  pos->deserialize(stream);
  return true;
}

auto Player::copy(Variable *other) -> void {
  auto *ptr = dynamic_cast<Player *>(other);
  if (!ptr) { return; }
  health->copy(ptr->health.get());
  pos->copy(ptr->pos.get());
}

auto Player::object_size() -> std::size_t {
  return sizeof(Player);
}

auto Player::copy_default() -> void {
  health->copy_default();
  pos->copy_default();
}

auto Player::complete_prototype() -> void { copy_default(); }

auto Player::get_fixed_mem_size() -> std::size_t {
  std::size_t total_size = sizeof(Player);
  total_size += sizeof(Uint32);
  total_size += Vector3::get_fixed_mem_size();
  return total_size;
}

}

using StructCreator = plato::VariablePtr (*)(plato::Domain *domain, plato::VarID, plato::PlatoVariableSyncType);
extern auto add_struct_creator(const std::string &name, StructCreator creator) -> bool;

static auto create_Player(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::Player::New(domain, parent, sync_type);
}

bool res_struct_Player = add_struct_creator("Player", create_Player);


static auto create_ArrayPlayer(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::ArrayPlayer::New(domain, parent, sync_type);
}

bool res_struct_ArrayPlayer = add_struct_creator("ArrayPlayer", create_ArrayPlayer);

static auto create_MapInt32Player(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<std::int32_t>::New(domain, parent, sync_type);
}

static auto create_MapInt64Player(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<std::int64_t>::New(domain, parent, sync_type);
}

static auto create_MapUint32Player(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<std::uint32_t>::New(domain, parent, sync_type);
}

static auto create_MapUint64Player(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<std::uint64_t>::New(domain, parent, sync_type);
}

static auto create_MapStringPlayer(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<std::string>::New(domain, parent, sync_type);
}

static auto create_MapFloatPlayer(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<float>::New(domain, parent, sync_type);
}

static auto create_MapDoublePlayer(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<double>::New(domain, parent, sync_type);
}

static auto create_MapBoolPlayer(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapPlayer<bool>::New(domain, parent, sync_type);
}

bool res_struct_MapInt32Player = add_struct_creator("MapInt32Player", create_MapInt32Player);
bool res_struct_MapInt64Player = add_struct_creator("MapInt64Player", create_MapInt64Player);
bool res_struct_MapUint32Player = add_struct_creator("MapUint32Player", create_MapUint32Player);
bool res_struct_MapUint64Player = add_struct_creator("MapUint64Player", create_MapUint64Player);
bool res_struct_MapStringPlayer = add_struct_creator("MapStringPlayer", create_MapStringPlayer);
bool res_struct_MapFloatPlayer = add_struct_creator("MapFloatPlayer", create_MapFloatPlayer);
bool res_struct_MapDoublePlayer = add_struct_creator("MapDoublePlayer", create_MapDoublePlayer);
bool res_struct_MapBoolPlayer = add_struct_creator("MapBoolPlayer", create_MapBoolPlayer);

extern auto add_struct_size(const std::string &name, std::size_t size) -> bool;
bool res_struct_size_Player = add_struct_size("Player", plato::Player::get_fixed_mem_size());

