#include "Vector3.hh"

namespace plato {

Vector3::Vector3(Domain *domain, VarID parent, PlatoVariableSyncType sync_type)
  : StructVariable(domain, parent, sync_type),
    x(domain->New<Float>(sync_type)),
    y(domain->New<Float>(sync_type)),
    z(domain->New<Float>(sync_type)) {
  }

Vector3::~Vector3() {}

auto Vector3::New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type) -> std::shared_ptr<Vector3> {
  return plato::make_shared<Vector3>(domain->mem_block(), domain, parent, sync_type);
}

auto Vector3::serialize(PlatoStream &stream) -> bool {
  stream << id();
  x->serialize(stream);
  y->serialize(stream);
  z->serialize(stream);
  return true;
}

auto Vector3::deserialize(PlatoStream &stream) -> bool {
  stream.skip(sizeof(VarID));
  x->deserialize(stream);
  y->deserialize(stream);
  z->deserialize(stream);
  return true;
}

auto Vector3::copy(Variable *other) -> void {
  auto *ptr = dynamic_cast<Vector3 *>(other);
  if (!ptr) { return; }
  x->copy(ptr->x.get());
  y->copy(ptr->y.get());
  z->copy(ptr->z.get());
}

auto Vector3::object_size() -> std::size_t {
  return sizeof(Vector3);
}

auto Vector3::copy_default() -> void {
  x->copy_default();
  y->copy_default();
  z->copy_default();
}

auto Vector3::complete_prototype() -> void { copy_default(); }

auto Vector3::get_fixed_mem_size() -> std::size_t {
  std::size_t total_size = sizeof(Vector3);
  total_size += sizeof(Float);
  total_size += sizeof(Float);
  total_size += sizeof(Float);
  return total_size;
}

}

using StructCreator = plato::VariablePtr (*)(plato::Domain *domain, plato::VarID, plato::PlatoVariableSyncType);
extern auto add_struct_creator(const std::string &name, StructCreator creator) -> bool;

static auto create_Vector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::Vector3::New(domain, parent, sync_type);
}

bool res_struct_Vector3 = add_struct_creator("Vector3", create_Vector3);


static auto create_ArrayVector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::ArrayVector3::New(domain, parent, sync_type);
}

bool res_struct_ArrayVector3 = add_struct_creator("ArrayVector3", create_ArrayVector3);

static auto create_MapInt32Vector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<std::int32_t>::New(domain, parent, sync_type);
}

static auto create_MapInt64Vector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<std::int64_t>::New(domain, parent, sync_type);
}

static auto create_MapUint32Vector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<std::uint32_t>::New(domain, parent, sync_type);
}

static auto create_MapUint64Vector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<std::uint64_t>::New(domain, parent, sync_type);
}

static auto create_MapStringVector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<std::string>::New(domain, parent, sync_type);
}

static auto create_MapFloatVector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<float>::New(domain, parent, sync_type);
}

static auto create_MapDoubleVector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<double>::New(domain, parent, sync_type);
}

static auto create_MapBoolVector3(plato::Domain *domain, plato::VarID parent, plato::PlatoVariableSyncType sync_type) -> plato::VariablePtr {
  return plato::MapVector3<bool>::New(domain, parent, sync_type);
}

bool res_struct_MapInt32Vector3 = add_struct_creator("MapInt32Vector3", create_MapInt32Vector3);
bool res_struct_MapInt64Vector3 = add_struct_creator("MapInt64Vector3", create_MapInt64Vector3);
bool res_struct_MapUint32Vector3 = add_struct_creator("MapUint32Vector3", create_MapUint32Vector3);
bool res_struct_MapUint64Vector3 = add_struct_creator("MapUint64Vector3", create_MapUint64Vector3);
bool res_struct_MapStringVector3 = add_struct_creator("MapStringVector3", create_MapStringVector3);
bool res_struct_MapFloatVector3 = add_struct_creator("MapFloatVector3", create_MapFloatVector3);
bool res_struct_MapDoubleVector3 = add_struct_creator("MapDoubleVector3", create_MapDoubleVector3);
bool res_struct_MapBoolVector3 = add_struct_creator("MapBoolVector3", create_MapBoolVector3);

extern auto add_struct_size(const std::string &name, std::size_t size) -> bool;
bool res_struct_size_Vector3 = add_struct_size("Vector3", plato::Vector3::get_fixed_mem_size());

