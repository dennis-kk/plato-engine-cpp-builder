#pragma once

#include "plato_variable.hh"

namespace plato {

class Vector3 : public StructVariable {
  Vector3() = delete;
  Vector3(const Vector3 &) = delete;
  Vector3(Vector3 &&) = delete;

public:
  FloatPtr x;
  FloatPtr y;
  FloatPtr z;

  Vector3(Domain *domain, VarID parent, PlatoVariableSyncType sync_type);
  virtual ~Vector3();
  auto static New(Domain *domain, VarID parent, PlatoVariableSyncType sync_type) -> std::shared_ptr<Vector3>;
  virtual auto serialize(PlatoStream &stream) -> bool override;
  virtual auto deserialize(PlatoStream &stream) -> bool override;
  virtual auto copy(Variable *other) -> void override;
  virtual auto object_size() -> std::size_t override;
  virtual auto copy_default() -> void override;
  virtual auto complete_prototype() -> void override;
  static auto get_fixed_mem_size() -> std::size_t;
};

using ArrayVector3 = ArrayVariable<Vector3>;
template <typename T> using MapVector3 = MapVariable<T, Vector3>;
}

